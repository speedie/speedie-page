<!DOCTYPE html>
<html>
<head>
<?php include 'php/header.php';?>
<meta charset="UTF-8">
<meta name="description" content="Privacy notice">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Privacy notice</title>
</head>
<body>
		<div class="content">
				<h2>Privacy notice and licensing</h2>
                    <p>I am just as concerned about privacy, security and free software as you. Because of my philosophy, this website does not use a single line of JavaScript as I believe it is unnecessary for 90% of what a website needs to do for the average person! This also means, the website is <strong>free/libre (as in freedom)</strong> by design, and therefore also LibreJS compliant. It should be noted that I <em>do</em> use PHP server-side in order to make maintaining this site easier.</p>
                    <p>However my entire website, including HTML. CSS, and PHP is available for free and distributed to you under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International</a> license. <strong>All contributions to the site are licensed under the same license.</strong> Nonfree contributions of any kind are <strong>not accepted</strong>.</p>
                    <p>This site is running on my Debian GNU/Linux VPS on the Apache 2 web server, along with the other services I run, such as <a href="https://ls.speedie.site">ls.speedie.site</a>, <a href="https://git.speedie.site">git.speedie.site</a>, <a href="https://wiki.speedie.site">wiki.speedie.site</a>, among others.</p>
		</div>
</body>
<footer>
		<?php include 'php/footer.php';?>
</footer>
</html>
