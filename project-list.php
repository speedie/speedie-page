<!DOCTYPE html>
<html>
<head>
<?php include 'php/header.php';?>
<title>Projects</title>
<meta charset="UTF-8">
<meta name="description" content="List of all projects I maintain/used to maintain.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
			<h1>Projects</h1>
                <p>Here's a list of most software projects I've worked on/currently work on or people still use.</p>
                <p>Note that it is not a full list, for a full list see <a href="https://github.com/speediegq">my GitHub</a> and <a href="https://codeberg.org/speedie">my Codeberg</a>.</p>
                <ul>
                <li>speedwm</li>
                    <p>speedwm is a window manager forked from suckless.org's dwm or dynamic window manager. Unlike dwm, speedwm tries to be minimal just like dwm but also has functionality and aesthetics as a goal. It is also much more minimal than other window managers like i3-gaps while offering many more features.</p>
	                <p><a href="https://speedwm.speedie.site">More information</a></p>
                <li>libspeedwm</li>
                    <p>libspeedwm is a minimal program for interacting with speedwm through the shell. It allows you to run signals but also perform actions. It is written in C because speedwm itself is written in C. It only has one dependency; libX11 used to set the root window name.</p>
	                <p><a href="projects/libspeedwm.php">More information</a></p>
                <li>speedwm-extras</li>
                    <p>speedwm-extras is a package of scripts that were originally part of speedwm. In an attempt to follow the unix philosophy further, these scripts eventually got their own repository. speedwm-extras contains scripts for volume, wifi, bluetooth, alt+tab, and more.</p>
	                <p><a href="https://codeberg.org/speedie/speedwm-extras">More information</a></p>
                <li>spmenu</li>
                    <p>spmenu is an X11 menu application based on dmenu which takes standard input, parses it, and lets the user choose an option and sends the selected option to standard output. Unique to this build is 256 color support (including SGR 16 color sequences), option to block typing, dwm-like keybind array in keybinds.h and more.</p>
	                <p><a href="https://spmenu.speedie.site">More information</a></p>
                <li>patches</li>
                    <p>Some patches I've made.</p>
                    <p><a href="projects/patches.php">More information</a></p>
                <li>startpage</li>
                    <p>A minimal start page for web browsers without JavaScript. It has support for overriding the CSS with Pywal colors and uses your desktop background as background for the page.</p>
	                <p><a href="https://codeberg.org/speedie/startpage">More information</a></p>
                <li>iron</li>
                    <p>iron is a minimal, customizable, hackable rautafarmi textboard client for GNU/Linux written in Bash. iron is the successor to rchat, and is the first client to support the new, faster, JSON rautafarmi API.</p>
	                <p><a href="projects/iron.php">More information</a></p>
                <li>rchat</li>
                    <p>rchat is a discontinued, minimal, customizable, hackable rautafarmi textboard client for GNU/Linux written in Bash. Unlike iron, rchat only supports the old messages.txt method.</p>
	                <p><a href="projects/rchat.php">More information</a></p>
                </ul>
                <ul>
                <li>fontctrl</li>
                    <p>fontctrl is a minimal, symlink based font manager for GNU/Linux. It handles installation, removal, and management of fonts, keeping your custom fonts easy to install, remove and manage.</p>
	                <p><a href="projects/fontctrl.php">More information</a></p>
                </ul>
                <ul>
                <li>elevendebloater</li>
                    <p>Elevendebloater is a minimal, hackable free software Windows 10 and 11 debloater. It removes most bloatware Microsoft added to the new Windows 11 operating system through winget.</p>
	                <p><a href="projects/elevendebloater.php">More information</a></p>
                </ul>
                <ul>
                <li>Project 081</li>
                    <p>Project 081 is a modification of Apple's Mac OS X 10.4 Tiger operating system adding better hardware support for the unofficially supported but mostly functional Late 2007 and Early 2008 Apple Macs. </p>
	                <p><a href="projects/project081.php">More information</a></p>
                </ul>
		</div>
</body>
<footer>
		<?php include 'php/footer.php';?>
</footer>
</html>
