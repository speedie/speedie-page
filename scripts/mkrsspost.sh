#!/bin/sh
# mkrsspost
# it just copies a html document to rss because im lazy as fuck

print() { printf "%s\n" "$*"; }
die() { clean; print "$*" >&2; exit 1; }

setvars() {
    [ -z "$rssdir" ] && rssdir="../templates"
    [ -z "$templatename" ] && templatename="mkrsspost_post.xml"
    [ -z "$realrssdir" ] && realrssdir=".."
    [ -z "$realrss" ] && realrss="rss.xml"
    return 0
}

clean() {
    rm -f /tmp/mkrsspost*
    return 0
}

readname() {
    printf "What's the name of the blog post? Example: 'My new blog post!'\n> "
    read -r NAME
    [ -z "$NAME" ] && printf "You must specify a name for the blog post.\n" && readname "$@" || return 0
}

readdate() {
    printf "What's the date of the blog post? Example: 'Tue, 03 Jan 2023 00:00:00 +0000'. Enter 'now' to use the current date.\n> "
    read -r DATE
    [ -z "$DATE" ] && printf "You must specify a date for the blog post.\n" && readdate "$@"

    [ "$DATE" = "now" ] && DATE="$(date "+%A, %d %b %Y %T %z")"

    return 0
}

readlink() {
    printf "What's the link to the blog post? This needs to be a path from the path to the RSS feed to the HTML post. Example: '/articles/my-post.php'.\n> "
    read -r LINK
    [ -z "$LINK" ] && printf "You must specify a link to the blog post.\n" && readlink "$@" || return 0
}

readguid() {
    printf "What's the GUID (unique identifier) of the blog post? Example: '%s'. Enter 'link' to use '%s'.\n> " "$LINK" "$LINK"
    read -r GUID
    [ -z "$GUID" ] && printf "You must specify a GUID for the blog post.\n" && readguid "$@"

    [ "$GUID" = "link" ] && GUID="$LINK"

    return 0
}

readdesc() {
    printf "What's the description (text) for the blog post?\n> "
    read -r DESC
    [ -z "$DESC" ] && printf "You must specify a description for the blog post.\n" && readdesc "$@"

    [ -e "$DESC" ] && PT=false || PT=true

    return 0
}

readq() {
    [ -z "$NAME" ] && readname "$@"
    [ -z "$DATE" ] && readdate "$@"
    [ -z "$LINK" ] && readlink "$@"
    [ -z "$GUID" ] && readguid "$@"
    [ -z "$DESC" ] && readdesc "$@"
}

mk() {
    [ "$PT" != "true" ] && DESC="$(cat "$DESC")"
    sed "s|mkrsspost_title|$NAME|g; \
         s|mkrsspost_link|$LINK|g; \
         s|mkrsspost_guid|$GUID|g; \
         s|mkrsspost_date|$DATE|g; \
         s|mkrsspost_description|$DESC|g" \
         /tmp/mkrsspost_template > $NAME-generated.rss.xml && print "Generated '$NAME-generated.rss.xml'." || die "Failed to generate item."

    return 0
}

init() {
    setvars

    [ -e "$rssdir/$templatename" ] && cp "$rssdir/$templatename" "/tmp/mkrsspost_template" || die "Template doesn't exist. Edit the script to fix this or export \$rssdir, \$realrssdir, \$realrss, \$templatename."

    readq "$@"
    mk "$@"

    # exiting
    clean "$@"
    return 0
}

init "$@" && exit 0 || exit 1
