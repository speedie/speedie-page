<!DOCTYPE html>
<html>
<head>
<?php include 'php/header.php';?>
<title>Frequently asked questions</title>
<meta charset="UTF-8">
<meta name="description" content="People often ask me stuff, what people keep asking is probably here somewhere.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Frequently asked questions</h2>
                <p>People often ask me stuff, what people keep asking is probably here somewhere.</p>
                <h3>Q: What operating system do I use?</h3>
                    <p>A: I used to use Gentoo on my desktop and Arch on my laptop, but I have since switched away from Gentoo completely. I now run Arch on all my systems. On my desktop, I have a host which only has to run a VM where I do my actual work. This is great for security!</p>
                <h3>Q: Why were some blog posts archived?</h3>
                    <p>A: I initially started archiving blog posts a few months ago, and at that point I had archived blog post 1 through 10. This is because they were either misinformation or just plain bad, outdated or even all of the above. I later on decided to archive a lot more, as well as remove them from the RSS feed. For those of you who want to read my old blog posts through RSS, you can add <a href="rss-archived-01.xml">this feed</a> to your reader which contains everything I removed.</p>
                <h3>Q: What about Discord?</h3>
                    <p>Back in 2021 through some time late 2022, I had a Discord server. I have since deleted it. While I do still have my Discord account, I rarely actually use it anymore. I don't want to use Discord because it is nonfree spyware, but as if that wasn't enough already the community is pretty much a combination of the worst people you could imagine. Matrix seems to keep out all these weirdo normies relatively well, so if you want to reach me, feel free to join the Matrix. Link is at home.</p>
                <h3>Q: Insert any question about politics here</h3>
                    <p>I want to keep this site politics free (outside of free software of course). While this may change in the future, I have decided to do this because politics really do not matter much anyway. That said though, I absolutely despise SJWs, and if you are one of those then I kindly ask you to leave, because I do not want your nonsense here.</p>
                <h3>Q: Can I still read this blog if I use Microsoft Windows or some other spyware?</h3>
                    <p>This is not really a question, however I am tired of people "apologizing" to me for using nonfree software. If you wish to use Windows, and play Candy Crush while reading my blog then you can do so. I am not going to tell you what you can use, however I will definitely make software recommendations. Please do <strong>not</strong> consider this me forcing you to use the software. If you don't want to use it, don't.</p>
                <h3>Q: Can I fork your software?</h3>
                    <p>I find this question kind of stupid, but I've been asked it a few times now, so I will answer it. You can <strong>always</strong> fork my software, with or without my permission because it is free software. This is why we fight for user freedom. I should not be able to hold you back from forking my software if you really want to. Do however note that you must follow the license.</p>
                <h3>Suggest questions</h3>
                    <p>If you have any suggestions for this page, feel free to <a href="mailto:speedie@speedie.site">email me</a>. If I get asked a question many times, I may put up the answer here.</p>
		</div>
</body>
<footer>
		<?php include 'php/footer.php';?>
</footer>
</html>
