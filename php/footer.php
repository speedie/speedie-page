<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="/css/footer.css">
<head>
<body>
<div>
<div class="column">
    <a class="links" href="/privacy.php">Licensing</a>
    <a class="links" href="/credits.php">Credits</a>
    <a class="links" href="https://git.speedie.site">Git</a>
    <a class="links" href="https://ls.speedie.site">Downloads</a>
</div>
</head>
</html>
