<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="/css/header.css">
<link rel="stylesheet" href="/css/main.css">
<link rel="icon" type="image/x-icon" href="/img/favicon.svg">
</head>
<body>
	<div class="navbar">
		<span>speedie's page <a href="https://speedie.site">Home</a> <a href="/project-list.php">Projects</a> <a href="/blog.php">Blog</a> <a href="https://git.speedie.site">Git</a> <a href="https://wiki.speedie.site">Wiki</a> <a href="/donate.php">Donate</a> <a href="/dir.php">Directory Listing</a></span>
	</div>
<div class="content">
