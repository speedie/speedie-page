<!DOCTYPE html>
<html>
<head>
<?php include 'php/header.php';?>
<title>Donate</title>
<meta charset="UTF-8">
<meta name="description" content="If you like my efforts and would like to support me, consider donating some Monero to me. You can easily do it anonymously.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h1>Donate</h1>
                <p>If you'd like to donate to me then you can do so anonymously using Monero. Any support is greatly appreciated! I am never going to paywall any of my content though and I'm not going to shill a product for some free Monero.</p>
                <img src="img/monero-qr.png" alt="image">
                <p><code>888ncoFDtpQecZvRgZf5ZCNXSmLVS3st1Yjf4k8SD4Jt4pPUpHkqzKE8UWiKFw9V5W6P946PUpmnS4YGuPkyq997LKQ3HzU</code></p>
            <h1>Vultr VPS</h3>
                <p>If you're going to get a VPS on Vultr, you can use my referral link and make me $10 (a little over a month of credit) when you purchase a VPS. <a href="https://www.vultr.com/?ref=9327892">Referral Link</a>.</p>
		</div>
</body>
<footer>
		<?php include 'php/footer.php';?>
</footer>
</html>
