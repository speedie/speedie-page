<!DOCTYPE html>
<html>
<head>
<?php include '../php/projects_header.php';?>
<title>speedwm</title>
<meta charset="UTF-8">
<meta name="description" content="speedie's fork of suckless.org's dwm">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="speedwm.css">
<title>Welcome to speedie.site</title>
</head>
<body>
		<div class="content">
            <img src="preview.png" alt="speedwm screenshot" width="75%">
<h1 id="speedwm">speedwm</h1>
<h2 id="what-is-speedwm">What is speedwm?</h2>
<p>speedwm is a window manager forked from dwm or dynamic window
manager. It manages the user’s open windows and tiles them according to
a set layout (dynamic).</p>
<p>Just like dwm, speedwm also tries to be minimal but also has
functionality and aesthetics as a goal. Tiling window managers (unlike
floating window managers that you may be used to) tile windows based on
a set layout making them easy to get productive on. They also encourage
the user to use their keyboard instead of the mouse so that the user
doesn’t have to move his hands much but there are also mouse binds and
more can be added by the user if desired. ## Installation</p>
<p>In order to install this build of speedwm, all dependencies must be
installed. You can see ‘Dependencies’ for a list of all dependencies
required and optionally recommended to use speedwm.</p>
<ul>
<li>git clone https://codeberg.org/speedie/speedwm</li>
<li>cd speedwm</li>
<li>make clean install
<ul>
<li>If any warnings/errors show up, fix it by installing the missing
dependency.</li>
</ul></li>
<li>If a .xinitrc is used, add <code>speedwm</code> to the end. If
you’re using .xinit you can also just
<code>startx /usr/bin/speedwm</code>.
<ul>
<li>If you do not have a .xinitrc, you can add autostart commands to the
file ~/.config/speedwm/autostart.sh.</li>
</ul></li>
<li>If a display manager is used, make sure it supports .desktop
entries.</li>
<li>NOTE: <code>ly</code> is known to have issues with dwm and
speedwm.</li>
</ul>
<p>If you see a bar or at least a cursor, your X server is likely
working. If you do <strong>not</strong> see a cursor or a bar, and no
keys seem to do anything, your X server is most likely not configured
properly. If you get sent back to the TTY, your X server is most likely
not configured properly. In this case, see your distro’s wiki page on
how to set up X11.</p>
<p>Note that speedwm is not and will <strong>never</strong> be
compatible with Wayland. I have no interest in ever supporting or
developing for it. Please don’t create any issues regarding Wayland
support.</p>
<p>If you’re having any issues on operating systems with the BSD kernel,
or something like NixOS, please file a bug report here.</p>
<h2 id="layouts">Layouts</h2>
<p>speedwm comes with the following layouts:</p>
<ul>
<li>Tile</li>
<li>Monocle</li>
<li>Grid</li>
<li>Deck</li>
<li>Centered Master</li>
<li>Tatami</li>
<li>Spiral</li>
<li>Dwindle</li>
<li>Bottom Stack</li>
<li>Horizontal Bottom Stack</li>
<li>Horizonal Grid</li>
<li>Dynamic Grid</li>
<li>Custom</li>
<li>Empty</li>
</ul>
<p>They can be switched between using a little menu (See Keybinds for
more information) or by right clicking the Layout indicator. The more
commonly used layouts can be switched between using a quick keybind.</p>
<h2 id="keybinds">Keybinds</h2>
<p>This is a full list of keybinds. Please let me know if any keybinds
are missing as these have been manually added.</p>
<p>Keybinds for regular applications</p>
<ul>
<li>Super+Shift+Enter - Opens the defined terminal (default is st)</li>
<li>Super+Shift+Colon - Opens an spmenu prompt</li>
<li>Super+Shift+s - Opens ‘maim’ to take a screenshot and copies it to
the clipboard using ‘xclip’ (requires speedwm-extras)</li>
<li>Super+Shift+f - Opens the defined file manager</li>
<li>Super+Shift+w - Opens the defined web browser</li>
<li>Super+Shift+o - Opens the speedwm-dfmpeg spmenu script to record
your screen. (requires speedwm-extras)</li>
<li>Super+Shift+t - Opens the defined editor in your terminal</li>
<li>Super+Shift+m - Kills the defined music player</li>
<li>Super+Shift+a - Opens the defined mixer in your terminal</li>
<li>Super+Shift+m - Opens the defined music player</li>
<li>Super+Shift+x - Opens the defined system process viewer in your
terminal</li>
<li>Super+Shift+c - Opens the defined chat client</li>
<li>Super+Shift+d - Opens iron in the terminal if it is installed</li>
<li>Super+Shift+u - Opens the defined RSS reader</li>
<li>Super+Shift+r - Opens the defined email client</li>
</ul>
<p>These keybinds are for navigating speedwm</p>
<ul>
<li>Super+f - Fullscreen the selected window</li>
<li>Super+b - Show/hide the speedwm bar</li>
<li>Super+s - Show/hide the systray</li>
<li>Super+j/k - Move focus between visible windows</li>
<li>Super+m - Moves focus to the Master window (or back if Master is
already focused)</li>
<li>Super+a/d - Increase/decrease size of each window</li>
<li>Super+w/e - Resize the window keeping the aspect ratio of it.</li>
<li>Super+o - Hide a window</li>
<li>Super+Minus - Show the scratchpad</li>
<li>Super+Equal - Remove the scratchpad</li>
<li>Super+Enter - Switch order of windows</li>
<li>Super+Colon - Open a list of desktop entries in spmenu</li>
<li>Super+Tab - Switch between windows on the same tag. (Microsoft
Windows-like Alt+Tab)</li>
<li>Super+v - Center the focused window.</li>
<li>Super+0 - Reset mfact</li>
<li>Super+r - Reset number of masters</li>
<li>Super+y - Make the current selected window sticky</li>
<li>Super+d - Focus the previous monitor.</li>
<li>Super+/ - Focus the next monitor.</li>
<li>Super+. - Increase bar padding by 1</li>
<li>Super+, - Decrease bar padding by 1</li>
<li>Super+  - Toggle mark on a client.</li>
<li>Super+] - Swap focused client with the marked client.</li>
<li>Super+[ - Swap focus with the marked client.</li>
<li>Super+1 - Move to tag 1</li>
<li>Super+2 - Move to tag 2</li>
<li>Super+3 - Move to tag 3</li>
<li>Super+4 - Move to tag 4</li>
<li>Super+5 - Move to tag 5</li>
<li>Super+6 - Move to tag 6</li>
<li>Super+7 - Move to tag 7</li>
<li>Super+8 - Move to tag 8</li>
<li>Super+9 - Move to tag 9</li>
<li>Super+Shift+1 - Preview tag 1</li>
<li>Super+Shift+2 - Preview tag 2</li>
<li>Super+Shift+3 - Preview tag 3</li>
<li>Super+Shift+4 - Preview tag 4</li>
<li>Super+Shift+5 - Preview tag 5</li>
<li>Super+Shift+6 - Preview tag 6</li>
<li>Super+Shift+7 - Preview tag 7</li>
<li>Super+Shift+8 - Preview tag 8</li>
<li>Super+Shift+9 - Preview tag 9</li>
<li>Super+Shift+q - Close the current window</li>
<li>Super+Shift+  - Kill all clients except focused.</li>
<li>Super+Shift+. - Increase bar padding and gaps by 1</li>
<li>Super+Shift+, - Decrease bar padding and gaps by 1</li>
<li>Super+Shift+/ - Reset bar padding and gaps</li>
<li>Super+Shift+h/j/k/l - Rotates a stack.</li>
<li>Super+Shift+Arrow - Resizes a window in floating mode</li>
<li>Super+Shift+Escape - Ask the user if they want to shutdown or reboot
or nothing</li>
<li>Super+Shift+Tab - Switch between windows and tags in detail.</li>
<li>Super+Shift+Equal - Toggle scratchpads</li>
<li>Super+Shift+Minus - Hide the scratchpad</li>
<li>Super+Shift+Space - Toggle floating windows</li>
<li>Super+Control+1 - Combine the current tag with tag 1</li>
<li>Super+Control+2 - Combine the current tag with tag 2</li>
<li>Super+Control+3 - Combine the current tag with tag 3</li>
<li>Super+Control+4 - Combine the current tag with tag 4</li>
<li>Super+Control+5 - Combine the current tag with tag 5</li>
<li>Super+Control+6 - Combine the current tag with tag 6</li>
<li>Super+Control+7 - Combine the current tag with tag 7</li>
<li>Super+Control+8 - Combine the current tag with tag 8</li>
<li>Super+Control+9 - Combine the current tag with tag 9</li>
<li>Super+Control+q - Mutes your audio</li>
<li>Super+Control+w - Increases your volume</li>
<li>Super+Control+e - Decreases your volume</li>
<li>Super+Control+0 - Tag all tags at once.</li>
<li>Super+Control+Enter - Mirror the layout</li>
<li>Super+Control+i - Increase stackcount by 1</li>
<li>Super+Control+u - Decrease stack count by 1</li>
<li>Super+Control+z/x - Increase/decrease gaps between windows by 5</li>
<li>Super+Control+j/k - Move focus between hidden windows (Can then
‘Show’)</li>
<li>Super+Control+Arrow - Moves a window to any corner of your screen
(Arrow key)</li>
<li>Super+Control+Tab - Open a spmenu prompt asking the user what layout
to switch to</li>
<li>Super+Control+o - Show a hidden focused window</li>
<li>Super+Control+a/d - Move to the next/previous tag</li>
<li>Super+Control+Shift+o - Show all hidden windows</li>
<li>Super+Control+Shift+p - Hide all windows</li>
<li>Super+Control+Shift+z/c - Move to the next/previous tag skipping any
without windows open</li>
<li>Super+Control+Shift+a/d - Move between available layouts</li>
<li>Super+Control+Shift+1 - Move the focused window to tag 1</li>
<li>Super+Control+Shift+2 - Move the focused window to tag 2</li>
<li>Super+Control+Shift+3 - Move the focused window to tag 3</li>
<li>Super+Control+Shift+4 - Move the focused window to tag 4</li>
<li>Super+Control+Shift+5 - Move the focused window to tag 5</li>
<li>Super+Control+Shift+6 - Move the focused window to tag 6</li>
<li>Super+Control+Shift+7 - Move the focused window to tag 7</li>
<li>Super+Control+Shift+8 - Move the focused window to tag 8</li>
<li>Super+Control+Shift+9 - Move the focused window to tag 9</li>
<li>Super+Control+Shift+Esc - Open speedwm-utils (requires
speedwm-extras)</li>
<li>Super+Control+Shift+Arrow - Resize the window to the screen
size.</li>
<li>Super+Control+Shift+s - Set a wallpaper (requires
speedwm-extras)</li>
<li>Super+Control+Shift+n - Connect to WLAN using iwd (requires
speedwm-extras)</li>
<li>Super+Control+Shift+b - Connect to a Bluetooth device using bluez
(requires speedwm-extras)</li>
<li>Super+Control+Shift+q - Pauses your music</li>
<li>Super+Control+Shift+w - Decreases your music volume</li>
<li>Super+Control+Shift+e - Increase your music volume</li>
<li>Super+Control+Shift+r - Restart speedwm</li>
<li>Super+Control+Shift+j/k - Change window size vertically (cfact)</li>
<li>Super+Control+Shift+Equal - Increase bar height by 1</li>
<li>Super+Control+Shift+Minus - Decrease bar height by 1</li>
</ul>
<p>Chained keybinds</p>
<ul>
<li>Super+r &amp; s - Run screenkey if its not running, otherwise kill
it.</li>
<li>Super+t &amp; r - Reorganize tags and move windows</li>
<li>Super+t &amp; t - Toggle tag area in the bar</li>
<li>Super+t &amp; y - Toggle empty tag area in the bar</li>
<li>Super+t &amp; w - Toggle title area in the bar</li>
<li>Super+t &amp; u - Toggle unselected title area in the bar</li>
<li>Super+t &amp; s - Toggle status area in the bar</li>
<li>Super+t &amp; l - Toggle layout area in the bar</li>
<li>Super+t &amp; o - Toggle inactive fade</li>
<li>Super+t &amp; r - Reset all bar modules</li>
<li>Super+t &amp; s - Make the current selected window sticky</li>
<li>Super+g &amp; t - Toggle gaps</li>
<li>Super+g &amp; 0 - Reset gaps</li>
<li>Super+g &amp; i - Increase inner gaps by 1</li>
<li>Super+g &amp; o - Increase outer gaps by 1</li>
<li>Super+g &amp; j/k - Increase/decrease gaps between windows by 1</li>
<li>Super+p &amp; j/k - Increase/decrease bar padding by 1</li>
<li>Super+p &amp; u/d - Increase/decrease bar padding by 5</li>
<li>Super+p &amp; r - Reset bar padding to default</li>
<li>Super+p &amp; t - Toggle bar padding on/off</li>
<li>Super+, &amp; r - Reset powerline options</li>
<li>Super+l &amp; q - Rotate forward in the layout axis</li>
<li>Super+l &amp; w - Rotate forward in the master axis</li>
<li>Super+l &amp; e - Rotate forward in the stack axis</li>
<li>Super+l &amp; r - Rotate forward in the secondary stack axis</li>
<li>Super+l &amp; q - Rotate backwards in the layout axis</li>
<li>Super+l &amp; w - Rotate backwards in the master axis</li>
<li>Super+l &amp; e - Rotate backwards in the stack axis</li>
<li>Super+l &amp; r - Rotate backwards in the secondary stack axis</li>
<li>Super+l &amp; 1 - Set layout to layout 0</li>
<li>Super+l &amp; 2 - Set layout to layout 1</li>
<li>Super+l &amp; 3 - Set layout to layout 2</li>
<li>Super+l &amp; 4 - Set layout to layout 3</li>
<li>Super+l &amp; 5 - Set layout to layout 4</li>
<li>Super+l &amp; 6 - Set layout to layout 5</li>
<li>Super+l &amp; 7 - Set layout to layout 6</li>
<li>Super+l &amp; 8 - Set layout to layout 7</li>
<li>Super+l &amp; 9 - Set layout to layout 8</li>
<li>Super+l &amp; 0 - Set layout to layout 9</li>
<li>Super+Shift+g &amp; i - Decrease inner gaps by 1</li>
<li>Super+Shift+g &amp; o - Decrease outer gaps by 1</li>
<li>Super+Shift+e &amp; a - Open the virtual keyboard</li>
<li>Super+Shift+e &amp; e - Open a list of all emojis and copy the
selection</li>
<li>Super+Shift+e &amp; r - Randomize wallpaper</li>
<li>Super+Shift+e &amp; p - Set wallpaper to the previous</li>
</ul>
<p>These will only work if your keyboard has special multimedia
buttons.</p>
<ul>
<li>Mute button - Mutes your audio</li>
<li>Up Volume button - Increases your volume</li>
<li>Next/Previous song button - Switch to the next/previous track</li>
<li>Down Volume button - Decreases your volume</li>
<li>Pause button - Pauses the current track</li>
<li>Stop button - Stops your defined music player</li>
<li>Browser button - Opens your defined web browser</li>
<li>Power button - Ask if you wanna shut down, restart or lock your
computer. (requires speedwm-extras)</li>
<li>Email button - Open your defined email client</li>
<li>System button - Open your defined status viewer in a terminal</li>
<li>Music button - Open your defined music player</li>
<li>WLAN button - Disconnect from WLAN (requires speedwm-extras)</li>
</ul>
<p>These binds can be activated using your mouse</p>
<ul>
<li>Tag <num> (Left click) - Switch to tag <num></li>
<li>Tag (Scrolling up/down) - Switch to the next/previous tag</li>
<li>Layout indicator (Left click) - Switch to the next layout</li>
<li>Layout indicator (Right click) - Switch to the previous layout</li>
<li>Layout indicator (Middle click) - Open an spmenu list of all layouts
(requires speedwm-extras)</li>
<li>Layout indicator (Scrolling up/down) - Switch to the next/previous
layout</li>
<li>Window title (Left click) - Show/hide the window</li>
<li>Window title (Right click) - Open speedwm-utils (requires
speedwm-extras)</li>
<li>Focused window (Super+Alt+Left click) - Move the focused window
around</li>
<li>Focused window (Super+Alt+Middle click) - Make the focused window
floating</li>
<li>Focused window title (Middle click) - Rotate stack</li>
<li>Dragging (Super+Right click) - Increase/decrease size of each window
(mfact)</li>
<li>Dragging (SuperControl+Right click) - Increase/decrease cfact</li>
<li>Root window (Right click) - List .desktop entries and open them
(requires j4-dmenu-desktop)</li>
</ul>
<h2 id="dependencies">Dependencies</h2>
<h3 id="required">Required</h3>
<p>These are absolutely necessary, speedwm will NOT compile without them
- libxft - Used for rendering text - pango - Used to markup text -
libXinerama - Used for multi-monitor support. - Can be disabled through
editing toggle.mk if you’re not interested in multiple monitors. -
imlib2 - Used for tag previews, window icons. - Can be disabled through
editing toggle.mk and toggle.h if you don’t want these features. - yajl
- Used to interact with speedwm through a sock. - Required for the IPC
patch. If the IPC patch is disabled through toggle.mk and toggle.h, you
do not need this. - tcc - Very minimal C compiler that speedwm uses to
greatly speed up compile times. If you do not want this dependency, edit
host.mk and set CC to ‘cc’ (or what C99 compiler you prefer).</p>
<h3 id="features">Features</h3>
<p>These are necessary for certain features and keybinds. If you want to
use an alternative, change it in options.h and/or keybinds.h and
mouse.h.</p>
<ul>
<li><a href="https://codeberg.org/speedie/spmenu">spmenu</a>
<ul>
<li>As of speedwm 1.9, speedwm now expects spmenu (fork of dmenu) to be
on the system instead of dmenu. While you can revert this change I
cannot provide support due to missing arguments.d</li>
</ul></li>
<li>xrdb
<ul>
<li>xrdb is used for external speedwm configuration (ie. after
compiling). It is <strong>not</strong> mandatory if you don’t need
this.</li>
</ul></li>
</ul>
<h3 id="software">Software</h3>
<p>This build of speedwm comes with keybinds for software. You can add,
change and remove keybinds by editing <code>keybinds.h</code> and
<code>mouse.h</code> and running <code>make clean install</code>.</p>
<ul>
<li>libspeedwm
<ul>
<li>Dependency for speedwm-extras allowing you to perform actions
externally.</li>
</ul></li>
<li>speedwm-extras
<ul>
<li>Important metapackage containing extra scripts that speedwm makes
use of. (https://codeberg.org/speedie/speedwm-extras)</li>
</ul></li>
<li>st - Default terminal</li>
<li>Chromium - Default web browser</li>
<li>htop - Status monitor</li>
<li>sfeed - RSS reader</li>
<li>rssread - RSS frontend</li>
<li>cmus - Music player
<ul>
<li>Default status bar also has support for moc/mocp (Music On
Console)</li>
</ul></li>
<li>neovim - Text editor</li>
<li>neomutt - Email client</li>
<li>lf - File manager</li>
<li>slock - Lock screen</li>
<li>weechat - IRC client</li>
<li>tmux - Used for the music player and IRC client</li>
<li>j4-dmenu-desktop - Listing .desktop entries</li>
</ul>
<p>And everything under <code>Features</code>.</p>
<h2 id="important">Important</h2>
<p>If you’re used to dwm, speedwm might be a little unfamiliar to you at
first. This is because speedwm doesn’t use config.h (or config.def.h).
Instead, config.h is split into different parts to make it easier to
edit. Instead of editing config.h you’ll want to edit:</p>
<ul>
<li>autostart.h for starting stuff right before speedwm (For example
xclip, pywal, etc.)</li>
<li>options.h for changing colors and applications to use with
keybinds.</li>
<li>signal.h for adding fake signals</li>
<li>colors.h for changing alpha options and color options, most users
won’t need to edit it.</li>
<li>xresources.h for adding/removing .Xresources options</li>
<li>rules.h for adding/removing rules</li>
<li>keybinds.h for adding/removing keybinds.</li>
<li>mouse.h for adding/removing mouse binds.</li>
<li>status.h for adding/removing status modules and aadding/removing
statuscmd clicks.</li>
<li>ipc.h for adding/removing IPC commands. (If support is compiled
in)</li>
<li>toggle.h for adding/removing features from getting compiled in.</li>
</ul>
<p>After you’ve edited one of the files, you need to run ‘make clean
install’ to reinstall speedwm. Remember that you can change colors
through your .Xresources file (see .Xresources and Pywal) meaning you do
not need to recompile speedwm.</p>
<p>Another important detail you must keep in mind is that this build
comes with a status bar simply named ‘speedwm_status’. It can be found
in the speedwm source code directory. It is just a shell script which
adds stuff to your status bar. It will automatically be started when
speedwm starts.</p>
<p>You can edit the status bar simply by editing ‘speedwm_stellar’ or
its modules (modules_*) and running ‘make clean install’. You can also
configure it by editing ‘~/.config/speedwm/statusrc’.</p>
<p>If you want to change status bar, edit options.h and set ‘static char
status’ to your status bar binary (must be in $PATH). Alternatively, you
can also set it in .Xresources (See .Xresources and Pywal).</p>
<h2 id="configuration-and-.xresources">Configuration and
.Xresources</h2>
<p>speedwm has .Xresources support thanks to the .Xresources patch. It
also has pywal support (tool which grabs colors based on your
wallpaper).</p>
<p>To configure speedwm, you may /usr/share/speedwm/example.Xresources
to either ~/.speedwmrc or ~/.config/speedwm/speedwmrc. Alternatively,
you can also copy the values to your .Xresources file.</p>
<p>.speedwmrc or speedwm/speedwmrc will be loaded when speedwm restarts.
If you want to load a .Xresources file you’ll need to add that to
autostart.sh.</p>
<p>Colors do not reload automatically though, you must reload them
manually. Use a signal for this (See list of signals above) or simply
‘libspeedwm –perform core_wm_reload’. This won’t restart speedwm, but it
will reload colors.</p>
<p>To use .Xresources, make sure ‘xrdb’ is installed. If a .xinitrc is
used, add ‘xrdb /path/to/.Xresources/file’ before ‘speedwm’. If a
.Xresources file is not used, add it to ~/.config/speedwm/autostart.sh
instead.</p>
<p>If you don’t want to define the options manually, there is an example
.Xresources file containing speedwm default settings in
docs/example.Xresources. You can copy this somewhere or you can simply
‘&lt; docs/example.Xresources &gt;&gt; ~/.Xresources’ to append the
speedwm options to your .Xresources file.</p>
<p>The magic of .Xresources is that it is a universal configuration
file. While you <em>can</em> use the col.value values, you can also use
traditional colors 0 through 15 as well. These colors take priority over
regular speedwm colors. This is so that speedwm is compatible with Pywal
and more general/mainstream .Xresources configurations.</p>
<p>Below is a list of all .Xresources values you can define.</p>
<ul>
<li>speedwm.bar.alpha: 1</li>
<li>speedwm.bar.height: 3</li>
<li>speedwm.bar.position: 1</li>
<li>speedwm.bar.paddingoh: 0</li>
<li>speedwm.bar.paddingov: 0</li>
<li>speedwm.bar.paddingih: 0</li>
<li>speedwm.bar.paddingiv: 0</li>
<li>speedwm.bar.hide: 0</li>
<li>speedwm.bar.hide.tags: 0</li>
<li>speedwm.bar.hide.emptytags: 1</li>
<li>speedwm.bar.hide.floating: 0</li>
<li>speedwm.bar.hide.layout: 0</li>
<li>speedwm.bar.hide.sticky: 0</li>
<li>speedwm.bar.hide.status: 0</li>
<li>speedwm.bar.hide.systray: 1</li>
<li>speedwm.bar.hide.unselected.title: 0</li>
<li>speedwm.bar.hide.title: 0</li>
<li>speedwm.bar.hide.icon: 0</li>
<li>speedwm.bar.hide.clientindicator: 0</li>
<li>speedwm.inset.x: 0</li>
<li>speedwm.inset.y: 0</li>
<li>speedwm.inset.w: 0</li>
<li>speedwm.inset.h: 0</li>
<li>speedwm.bar.titleposition: 1</li>
<li>speedwm.border.size: 1</li>
<li>speedwm.client.map: 1</li>
<li>speedwm.client.allowurgent: 1</li>
<li>speedwm.client.automove: 1</li>
<li>speedwm.client.autofocus: 1</li>
<li>speedwm.client.autoresize: 1</li>
<li>speedwm.client.decorhints: 1</li>
<li>speedwm.client.hide.border: 0</li>
<li>speedwm.client.hide.unselected.border: 1</li>
<li>speedwm.client.fade.inactive: 1</li>
<li>speedwm.client.fade.windows: 1</li>
<li>speedwm.client.floatscratchpad: 0</li>
<li>speedwm.client.savefloat: 1</li>
<li>speedwm.client.swallow: 1</li>
<li>speedwm.client.swallowfloating: 1</li>
<li>speedwm.client.wmclass: 1</li>
<li>speedwm.fonts.font: NotoSans Regular 9</li>
<li>speedwm.col.layout: #99b3ff</li>
<li>speedwm.col.layouttext: #000000</li>
<li>speedwm.col.status0: #131210</li>
<li>speedwm.col.status1: #bf616a</li>
<li>speedwm.col.status2: #A16F9D</li>
<li>speedwm.col.status3: #68ABAA</li>
<li>speedwm.col.status4: #A89F93</li>
<li>speedwm.col.status5: #D3A99B</li>
<li>speedwm.col.status6: #AFC9AC</li>
<li>speedwm.col.status7: #eae1cb</li>
<li>speedwm.col.status8: #a39d8e</li>
<li>speedwm.col.status9: #6D5E8E</li>
<li>speedwm.col.status10: #a16f9d</li>
<li>speedwm.col.status11: #d3a99b</li>
<li>speedwm.col.status12: #afc9ac</li>
<li>speedwm.col.status13: #eae1cb</li>
<li>speedwm.col.status14: #6d5e8e</li>
<li>speedwm.col.status15: #ffffff</li>
<li>speedwm.col.powerline0: #131210</li>
<li>speedwm.col.powerline1: #bf616a</li>
<li>speedwm.col.powerline2: #A16F9D</li>
<li>speedwm.col.powerline3: #68ABAA</li>
<li>speedwm.col.powerline4: #A89F93</li>
<li>speedwm.col.powerline5: #D3A99B</li>
<li>speedwm.col.powerline6: #AFC9AC</li>
<li>speedwm.col.powerline7: #eae1cb</li>
<li>speedwm.col.powerline0_text: #eeeeee</li>
<li>speedwm.col.powerline1_text: #131210</li>
<li>speedwm.col.powerline2_text: #131210</li>
<li>speedwm.col.powerline3_text: #131210</li>
<li>speedwm.col.powerline4_text: #131210</li>
<li>speedwm.col.powerline5_text: #131210</li>
<li>speedwm.col.powerline6_text: #131210</li>
<li>speedwm.col.powerline7_text: #131210</li>
<li>speedwm.col.systray: #222222</li>
<li>speedwm.col.tag1: #99b3ff</li>
<li>speedwm.col.tag2: #99b3ff</li>
<li>speedwm.col.tag3: #99b3ff</li>
<li>speedwm.col.tag4: #99b3ff</li>
<li>speedwm.col.tag5: #99b3ff</li>
<li>speedwm.col.tag6: #99b3ff</li>
<li>speedwm.col.tag7: #99b3ff</li>
<li>speedwm.col.tag8: #99b3ff</li>
<li>speedwm.col.tag9: #99b3ff</li>
<li>speedwm.col.tag1.text: #eeeeee</li>
<li>speedwm.col.tag2.text: #eeeeee</li>
<li>speedwm.col.tag3.text: #eeeeee</li>
<li>speedwm.col.tag4.text: #eeeeee</li>
<li>speedwm.col.tag5.text: #eeeeee</li>
<li>speedwm.col.tag6.text: #eeeeee</li>
<li>speedwm.col.tag7.text: #eeeeee</li>
<li>speedwm.col.tag8.text: #eeeeee</li>
<li>speedwm.col.tag9.text: #eeeeee</li>
<li>speedwm.col.tagurgent: #f0e68c</li>
<li>speedwm.col.background: #222222</li>
<li>speedwm.col.textnorm: #bbbbbb</li>
<li>speedwm.col.textsel: #222222</li>
<li>speedwm.col.titlenorm: #222222</li>
<li>speedwm.col.titlesel: #99b3ff</li>
<li>speedwm.col.titlehid: #222222</li>
<li>speedwm.col.windowbordernorm: #000000</li>
<li>speedwm.col.windowbordersel: #eeeeee</li>
<li>speedwm.col.windowborderurg: #f0e68c</li>
<li>speedwm.text.tag1.empty: </li>
<li>speedwm.text.tag2.empty: </li>
<li>speedwm.text.tag3.empty: </li>
<li>speedwm.text.tag4.empty: </li>
<li>speedwm.text.tag5.empty: </li>
<li>speedwm.text.tag6.empty: </li>
<li>speedwm.text.tag7.empty: </li>
<li>speedwm.text.tag8.empty: </li>
<li>speedwm.text.tag9.empty: </li>
<li>speedwm.text.tag1.used: </li>
<li>speedwm.text.tag2.used: </li>
<li>speedwm.text.tag3.used: </li>
<li>speedwm.text.tag4.used: </li>
<li>speedwm.text.tag5.used: </li>
<li>speedwm.text.tag6.used: </li>
<li>speedwm.text.tag7.used: </li>
<li>speedwm.text.tag8.used: </li>
<li>speedwm.text.tag9.used: </li>
<li>speedwm.text.layout1: </li>
<li>speedwm.text.layout2: </li>
<li>speedwm.text.layout3: </li>
<li>speedwm.text.layout4: </li>
<li>speedwm.text.layout5: </li>
<li>speedwm.text.layout6: </li>
<li>speedwm.text.layout7: </li>
<li>speedwm.text.layout8: </li>
<li>speedwm.text.layout9: </li>
<li>speedwm.text.layout10: </li>
<li>speedwm.text.layout11: </li>
<li>speedwm.text.layout12: </li>
<li>speedwm.text.layout13: </li>
<li>speedwm.text.layout14: </li>
<li>speedwm.text.layout15: </li>
<li>speedwm.color.hiddentitle: 1</li>
<li>speedwm.color.layout: 0</li>
<li>speedwm.color.selectedtitle: 0</li>
<li>speedwm.bar.powerline.tag.shape: 0</li>
<li>speedwm.fullscreen.hidebar: 1</li>
<li>speedwm.fullscreen.lockfullscreen: 1</li>
<li>speedwm.fullscreen.movefullscreenmon: 0</li>
<li>speedwm.gaps.enable: 1</li>
<li>speedwm.gaps.sizeih: 10</li>
<li>speedwm.gaps.sizeiv: 10</li>
<li>speedwm.gaps.sizeoh: 10</li>
<li>speedwm.gaps.sizeov: 10</li>
<li>speedwm.gaps.smartgaps: 0</li>
<li>speedwm.gaps.smartgapsize: 0</li>
<li>speedwm.icon.size: 15</li>
<li>speedwm.icon.spacing: 5</li>
<li>speedwm.layout.deck.count: 0</li>
<li>speedwm.layout.deck.format: [%d]</li>
<li>speedwm.layout.monocle.clientcount: 0</li>
<li>speedwm.layout.monocle.count: 0</li>
<li>speedwm.layout.monocle.format: [%d/%d]</li>
<li>speedwm.mfact: 0.50</li>
<li>speedwm.mfact.lowest: 0.05</li>
<li>speedwm.mouse.clicktofocus: 0</li>
<li>speedwm.mouse.mfact: 1</li>
<li>speedwm.mouse.cfact: 1</li>
<li>speedwm.rule.refresh: 0</li>
<li>speedwm.stack.attachdirection: 3</li>
<li>speedwm.stack.centerfloating: 1</li>
<li>speedwm.stack.i3mcount: 0</li>
<li>speedwm.stack.mastercount: 1</li>
<li>speedwm.stack.snap: 20</li>
<li>speedwm.status.defaultstatus:</li>
<li>speedwm.switcher.maxheight: 200</li>
<li>speedwm.switcher.maxwidth: 600</li>
<li>speedwm.switcher.menupositionv: 1</li>
<li>speedwm.switcher.menupositionh: 1</li>
<li>speedwm.systray.padding: 2</li>
<li>speedwm.systray.pinning: 0</li>
<li>speedwm.systray.position: 0</li>
<li>speedwm.tag.pertag: 1</li>
<li>speedwm.tag.preview: 1</li>
<li>speedwm.tag.preview.bar: 1</li>
<li>speedwm.tag.preview.paddingh: 0</li>
<li>speedwm.tag.preview.paddingv: 0</li>
<li>speedwm.tag.preview.scale: 4</li>
<li>speedwm.tag.resetgaps: 0</li>
<li>speedwm.tag.resetlayout: 0</li>
<li>speedwm.tag.resetmfact: 0</li>
<li>speedwm.tag.start: 1</li>
<li>speedwm.tag.underline: 0</li>
<li>speedwm.tag.underlineall: 0</li>
<li>speedwm.tag.underlinepad: 5</li>
<li>speedwm.tag.underlinestroke: 2</li>
<li>speedwm.tag.underlinevoffset: 0</li>
<li>speedwm.tag.urgentwindows: 1</li>
<li>speedwm.tiling.resizehints: 0</li>
<li>speedwm.run.shell: /bin/sh</li>
<li>speedwm.status.hideemptymodule: 1</li>
<li>speedwm.status.leftpadding:</li>
<li>speedwm.status.rightpadding:</li>
<li>speedwm.status.separator:</li>
</ul>
<h2 id="signals">Signals</h2>
<p>Thanks to the ‘fsignal’ patch available on suckless.org’s website, we
can easily write shell scripts to interact with dwm and therefore
speedwm. I made some changes to this patch, because it has some..
questionable behaviour in my opinion.</p>
<p>To use signals, you can use libspeedwm. Previously, speedwm-utils
(part of speedwm-extras) would be used but that now depends on
libspeedwm anyway. Using libspeedwm directly is the easiest option.</p>
<p>If you do not have speedwm-extras or libspeedwm, you can use the
speedwm binary itself. The syntax is speedwm -s “#cmd:<signum>” This
option is not as solid though as signums can and will likely be moved
around breaking your scripts. Therefore I highly recommend you use
libspeedwm when writing scripts.</p>
<p>Below is a list of all signums and what they do.</p>
<ul>
<li>1 - Switch to the Tiling layout</li>
<li>2 - Switch to the Floating layout</li>
<li>3 - Switch to the Monocle layout</li>
<li>4 - Switch to the Grid layout</li>
<li>5 - Switch to the Deck layout</li>
<li>6 - Switch to the Centered Master layout</li>
<li>7 - Switch to the Tatami layout</li>
<li>8 - Switch to the Fibonacci Spiral layout</li>
<li>9 - Switch to the Fibonacci Dwindle layout</li>
<li>10 - Switch to the Bottom Stack Vertical layout</li>
<li>11 - Switch to the Bottom Stack Horizontal layout</li>
<li>12 - Switch to the Horizontal Grid layout</li>
<li>13 - Switch to the Dynamic Grid layout</li>
<li>14 - Switch to the Custom layout</li>
<li>15 - Custom layout options</li>
<li>16 - Switch to the next layout</li>
<li>17 - Switch to the previous layout</li>
<li>18 - Increase mfact by 0.05</li>
<li>19 - Decrease mfact by 0.05</li>
<li>20 - Toggle sticky</li>
<li>21 - Toggle the bar</li>
<li>22 - Toggle fullscreen</li>
<li>23 - Toggle floating</li>
<li>24 - Swap the current window with the next</li>
<li>25 - Reorganize tags (Reorder them)</li>
<li>26 - Shutdown speedwm</li>
<li>27 - Restart speedwm</li>
<li>28 - Show the focused window</li>
<li>29 - Hide the focused window</li>
<li>30 - Kill the focused window</li>
<li>31 - Rotate the stack up</li>
<li>32 - Rotate the stack down</li>
<li>33 - Increase number of windows in the master stack</li>
<li>34 - Decrease number of windows in the master stack</li>
<li>35 - Focus the Master window</li>
<li>36 - Switch focus between windows +1</li>
<li>37 - Switch focus between windows -1</li>
<li>38 - Switch focus between hidden windows +1</li>
<li>39 - Switch focus between hidden windows -1</li>
<li>40 - Toggle opacity for windows</li>
<li>41 - Increase cfact by 0.05</li>
<li>42 - Decrease cfact by 0.05</li>
<li>43 - Switch to the previous tag</li>
<li>44 - Switch to the next tag</li>
<li>45 - Reset gaps to the default size</li>
<li>46 - Toggle gaps</li>
<li>47 - Increase gaps by 1</li>
<li>48 - Decrease gaps by 1</li>
<li>49 - Increase inner gaps by 1</li>
<li>50 - Decrease inner gaps by 1</li>
<li>51 - Increase outer gaps by 1</li>
<li>52 - Decrease outer gaps by 1</li>
<li>53 - Kill all windows except focused</li>
<li>54 - Focus the next monitor</li>
<li>55 - Focus the previous monitor</li>
<li>56 - Show the scratchpad</li>
<li>57 - Hide the scratchpad</li>
<li>58 - Remove the scratchpad</li>
<li>59 - Reset layout/mfact</li>
<li>60 - Reset mastercount</li>
<li>61 - Toggle systray</li>
<li>62 - Hide all windows</li>
<li>63 - Show all windows</li>
<li>64 - Reset mfact</li>
<li>65 - Reload .Xresources on the fly</li>
<li>66 - Switch to the previous tag, skipping empty tags</li>
<li>67 - Switch to the next tag, skipping empty tags</li>
<li>68 - Toggle the tag area in the bar</li>
<li>69 - Toggle the empty tags in the bar</li>
<li>70 - Unused</li>
<li>71 - Unused</li>
<li>72 - Unused</li>
<li>73 - Unused</li>
<li>74 - Unused</li>
<li>75 - Unused</li>
<li>76 - Unused</li>
<li>77 - Toggle the title area in the bar</li>
<li>78 - Toggle the unselected title area in the bar</li>
<li>79 - Toggle the layout area in the bar</li>
<li>80 - Toggle the status area in the bar</li>
<li>81 - Toggle the floating indicator area in the bar</li>
<li>82 - Toggle the sticky indicator area in the bar</li>
<li>83 - Toggle the icon in the window title</li>
<li>84 - Unused</li>
<li>85 - Unused</li>
<li>86 - Unused</li>
<li>87 - Increase bar height by 1</li>
<li>88 - Decrease bar height by 1</li>
<li>89 - Reset bar height</li>
<li>90 - Increase vertical barpadding by 1</li>
<li>91 - Decrease vertical barpadding by 1</li>
<li>92 - Increase horizontal barpadding by 1</li>
<li>93 - Decrease horizontal barpadding by 1</li>
<li>94 - Increase vertical and horizontal barpadding by 1</li>
<li>95 - Decrease vertical and horizontal barpadding by 1</li>
<li>96 - Toggle vertical barpadding</li>
<li>97 - Toggle horizontal barpadding</li>
<li>98 - Toggle vertical and horizontal barpadding</li>
<li>99 - Reset vertical barpadding</li>
<li>100 - Reset horizontal barpadding</li>
<li>101 - Reset vertical and horizontal barpadding</li>
<li>102 - Increase stack count by 1</li>
<li>103 - Decrease stack count by 1</li>
<li>104 - Rotate forward in the layout axis</li>
<li>105 - Rotate forward in the master axis</li>
<li>106 - Rotate forward in the stack axis</li>
<li>107 - Rotate forward in the secondary stack axis</li>
<li>108 - Rotate backwards in the layout axis</li>
<li>109 - Rotate backwards in the master axis</li>
<li>110 - Rotate backwards in the stack axis</li>
<li>111 - Rotate backwards in the secondary stack axis</li>
<li>112 - Mirror the layout</li>
<li>113 - Enter an empty layout where all windows are hidden</li>
<li>114 - Increase barpadding and gaps by 1</li>
<li>115 - Decrease barpadding and gaps by 1</li>
<li>116 - Toggle mark on a window.</li>
<li>117 - Swap focus with the marked window.</li>
<li>118 - Swap the focused window with the marked window.</li>
<li>119 - Center the focused window</li>
<li>120 - Toggle border for the focused window</li>
<li>121 - Reset bar padding and gaps</li>
</ul>
<h2 id="status-bar">Status bar</h2>
<p>speedwm has a status bar. It’s the (by default) right part of the
bar. It supports:</p>
<ul>
<li>Pango markup</li>
<li>Colored glyphs</li>
</ul>
<p>The regular (non-powerline) bar also supports:</p>
<ul>
<li>status2d markup
<ul>
<li>This allows you to color the status bar text at any time.</li>
</ul></li>
<li>statuscmd markup
<ul>
<li>This allows the status bar to have clickable modules.</li>
</ul></li>
</ul>
<p>as well as regular plain text and colored emojis or glyphs. To
override this status, you can use the ‘speedwm -s “status text”’
command. If you prefer, you can also use <code>xsetroot -name</code>
which does the same thing.</p>
<p>Bundled with speedwm is a fork of dwmblocks. dwmblocks is a dwm
status bar that handles this all for you through a block system. This
fork has been integrated into the Makefile and is (by default) installed
when speedwm is compiled. The status bar can be configured in the
status.c and status.h.</p>
<p>By default the status bar runs modules that are also bundled with
speedwm (see modules/ directory). To configure these modules, you can
edit ~/.config/speedwm/statusrc which should be created when a module
runs.</p>
<p>The bundled status bar is autostarted by speedwm if it is installed.
If you want to use your own status bar, comment out ‘USESTATUS’ in
toggle.mk and remove /usr/bin/status if speedwm has been installed
previously. Then simply start the status bar through autostart.h,
~/.config/speedwm/autostart.sh, .xinitrc or some other means of running
a program. ## Additional note on autostart</p>
<p>If you wish to add autostart entries without recompiling, consider
using $HOME/.config/speedwm/autostart.sh. This is a path added to
autostart.h and you can fill it with anything you want. Make sure your
user has permission to execute the script.</p>
<p>Note that this script or any other commands in autostart.h will
<strong>not</strong> run when speedwm is restarted, only when speedwm is
first started. ## Credits</p>
<p>I far from wrote this entire project myself. Below are people who
made this project what it is through submitting patches to suckless or
otherwise contributing code in some way in alphabetical order.</p>
<ul>
<li>Adam Yuan</li>
<li>Alex Cole</li>
<li>Anukul Adhikari</li>
<li>Ayoub Khater</li>
<li>bakkeby</li>
<li>bit6tream</li>
<li>cd</li>
<li>Chris Down</li>
<li>Chris Noxz</li>
<li>Daniel Bylinka</li>
<li>Dhaval Patel</li>
<li>Eon S. Jeon</li>
<li>explosion-mental</li>
<li>Fabian Blatz</li>
<li>Finn Rayment</li>
<li>Georgios Oxinos</li>
<li>Ivan J.</li>
<li>Jan Christoph Ebersbach</li>
<li>Jared</li>
<li>kleinbottle4</li>
<li>Luigi Foscari</li>
<li>Luke Smith</li>
<li>Marius Iacob</li>
<li>Markus Teich</li>
<li>Mihir Lad</li>
<li>MLquest8</li>
<li>Ondřej Grover</li>
<li>ornx</li>
<li>Patrick Steinhardt</li>
<li>phi</li>
<li>prx</li>
<li>Rob King</li>
<li>Ryan Roden-Corrent</li>
<li>sipi</li>
<li>Sönke Lambert</li>
<li>speedie</li>
<li>Stefan Mark</li>
<li>Stefan Matz</li>
<li>suckless.org</li>
<li>Timmy Keller</li>
<li>Viliam Kováč</li>
</ul>
<p>See Codeberg contributions for more information.</p>
<h2>Auto generated.</h2>
<p>This page was auto generated by the speedwm-help script bundled with speedwm. It acts as the help script and it writes documentation to HTML, Markdown and plain text from documentation in the docs folder and data grabbed from your current system.</p>
</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>

