<!DOCTYPE html>
<html>
<head>
<?php include '../php/projects_header.php';?>
<title>Download Project 081 </title>
<meta charset="UTF-8">
<meta name="description" content="This page has download links for Project 081. All of them are official.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
	<div class="content">
            <h1>Download Project 081</h1>
                <p>This page has all <em>official</em> Project 081 mirrors. Feel free to use any of them. Archive.org download speeds are very slow so I recommend the other mirrors.</p>
                <p>To write a Project 081 image, simply run <code>dd if=X of=Y</code> where X is your Project 081 image and Y is the USB drive you have. To see your drives, run <code>lsblk</code>.</p>
                <p>For Windows, you can <a href="https://gentoo.org">install gentoo</a>. (or view the installation guide)</p>
                <h2>Project 081 0.5</h2>
                <h3>Regular</h3>
                <p>This image is for all Macs except iMac8,1 and MacBookPro4.1.</p>
                <p>GitHub (Regular) <a href="https://github.com/p081/Project081/releases/download/beta3-0.5/Red.Project_081-0.5B3-Installer.dmg">[Download]</a></p>
                <p>Archive.org (Regular) <a href="https://archive.org/download/the81project/Else-Project_081-0.5B3-Installer.dmg">[Download]</a></p>
                <h3>Special</h3>
                <p>This image is for all Macs that require patched sound. This means if you're on an iMac8,1 or MacBookPro4,1 pick this version.</p>
                <p>GitHub (iMac8,1/MacBookPro4,1) <a href="https://github.com/p081/Project081/releases/download/beta3-0.5/Blue.Project_081-0.5B3-Installer.dmg">[Download]</a></p>
                <p>Archive.org (iMac8,1/MacBookPro4,1) <a href="https://archive.org/download/the81project/iMac8%2C1-MacBookPro4%2C1-Project_081-0.5B3-Installer.dmg">[Download]</a></p>
                <h2>Older versions of Project 081</h2>
                <p>Older versions can be found on <a href="https://github.com/p081/project081">GitHub</a> or <a href="https://archive.org/details/the81project">Archive.org</a>.</p>
                <h2>Mirrors down?</h2>
                <p>If the mirrors are down, please <a href="mailto:speedie@speedie.site">send me an email</a> so I can fix them.</p>
                <p>If you'd like to host your own to support the project, also contact me.</p>
	</div>
</body>
<footer>
	<?php include '../php/footer.php';?>
</footer>
</html>
