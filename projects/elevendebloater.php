<!DOCTYPE html>
<html>
<head>
<?php include '../php/projects_header.php';?>
<title>Elevendebloater </title>
<meta charset="UTF-8">
<meta name="description" content="Elevendebloater is a free software, bloat-free winget based debloater script for Microsoft Windows 11 which aims to debloat and de-spook Windows 11 as much as possible.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
        <h2>ElevenDebloater</h2>
            <p><img src="../img/elevendebloater.png" alt="images" width="70%"></p>
            <p>Microsoft Windows debloater specifically for Windows 11 (now with 10 support) that relies on the App Installer.</p>
            <p>Made this just because I had some free time and was messing around with 11 build 21996.1</p>
        <h3>Before running</h3>
            <p>Please run <code>winget list</code> in cmd.exe and accept the terms. Otherwise nothing will be deleted. 0.95 may have fixed this problem by doing it automatically. Still try this if you can't get it to work!</p>
        <h3>Mirrors:</h3>
            <p><a href="https://raw.githubusercontent.com/speediegq/elevendebloater/main/ElevenDebloater-1.0.bat">ElevenDebloater-1.0.bat</a>, <a href="https://github.com/speediegq/elevendebloater/archive/refs/tags/1.0.zip">1.0.zip</a></p>
            <p><a href="https://www.softpedia.com/get/Tweak/System-Tweak/ElevenDebloater.shtml#download">Unofficial SoftPedia mirror</a>.</p>
        <h3>How to use</h3>
            <p>Download the latest script from one of the mirrors above and run the .bat as Administrator. Then simply follow the instructions on-screen.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
