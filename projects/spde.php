<!DOCTYPE html>
<html>
<head>
<?php include '../php/projects_header.php';?>
<title>spDE</title>
<meta charset="UTF-8">
<meta name="description" content="spDE is a Gentoo and Arch metapackage which installs all software I use on my computers. It comes with USE flags so you can disable what you don't need/use. After installing this package you should end up with a system fairly close to mine. It can be compared to a post install script like LARBS but cleaner because it uses your existing package manager to install the system.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h1>spDE</h1>
                <p>spDE is an Arch and Gentoo metapackage which installs all software I use on my computers. If you're using Gentoo, it comes with USE flags so you can disable what you don't need/use. After installing this package you should end up with a system near identical to mine. It can be compared to a post install script like LARBS but cleaner because it uses your existing package manager to install the system (except dotfiles).</p>
                <p>It is a "sequel" to the now dead original spDE project, which similar to LARBS was a post-install script which installs my configuration files and packages. Problem with that approach is that it is a lot of work to maintain and there are way too many points of failure. Not to mention it is hard to reverse the process.</p>
                <p>I started creating this ebuild after all my builds of software got ebuilds to save time setting up a Gentoo system. It also takes less work to maintain an ebuild because all you need to do is add the programs to the DEPEND variable. I later expanded it to also run on Arch GNU/Linux.</p>
            <h2>Installation: Gentoo</h2>
                <p>First off, <a href="overlay.php">add my overlay</a>. Then simply run <code>emerge --keep-going --autounmask=y --autounmask-write --ask --quiet x11-misc/spde</code> as root. Apply required USE changes using <code>dispatch-conf</code> and then <code>U (use)</code>.</p>
                <p>Then, follow 'Using spDE'.</p>
            <h2>Installation: Arch</h2>
                <p>To install spDE on Arch, add my <a href="repository.php">repository</a>. Then run <code>pacman -Syyu; pacman -S spde</code>. Now that you have spDE installed, follow 'Using spDE'.</p>
            <h2>Using spDE</h2>
                <p>After installing spDE, you may find that software does not have any configuration. You can install my dotfiles and later have your own very easily. First of, run <code>spde -i</code> to create the /usr/share/spde directory which will contain all configuration files. Now, install the dotfiles to <em>your</em> user by using the <code>spde -a <your user></code> command. Now, follow 'Starting spDE' if you don't know what to do next.</p>
            <h2>Starting spDE</h2>
                <p>Alright, so you have spDE installed, now what? First, you need to start it. You can skip this if you use a display manager. In that case simply set the environment to speedwm and it will work as expected. To start it with xinit, run <code>startx /usr/bin/spde -r</code>. For sx, run <code>sx sh /usr/bin/spde -r</code>. After that, you should reach a desktop provided Xorg is configured properly.</p>
            <h2>Help, spDE deleted my configuration files by accident</h2>
                <p>Don't worry, don't panic, I thought of this. If spDE finds configuration files that it will rewrite, it is going to back them up first. All your configuration files will be in ~/.config/spde/.old_conf. If you added your user twice, sadly there's nothing you can do. Sorry.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
