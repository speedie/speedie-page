<!DOCTYPE html>
<html>
<head>
<?php include '../php/projects_header.php';?>
<title>Project 081 </title>
<meta charset="UTF-8">
<meta name="description" content="Project 081 is a modified Mac OS X 10.4.10 Tiger image that allows you to install Tiger on your unsupported Late 2007 and Early 2008 Apple Macs.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
	 <div class="content">
            <img src="../img/project081.png" width="300" alt="image">
            <h1>Project 081</h1>
            <p>NOTE: It has come to my attention that the MacBookPro4,1 <strong>does not boot</strong> Project 081 successfully and when it does, it has several issues. This project will be updated again to hopefully fix this issue.</p>
            <p>Project 081 is a modified Mac OS X 10.4.10 Tiger image that allows you to install Tiger on your <i>officially</i> unsupported Late 2007 and Early 2008 Apple Macs.</p>
            <p>It comes with many drivers and tweaks to improve the experience on unsupported hardware.</p>
            <h2>Why does this need to exist?</h2>
            <p>Apple Macs released Late 2007 or later officially only support Mac OS X 10.5 Leopard because Apple doesn't care about backwards compatibility. This doesn't matter for most people because Leopard is a fancy new release that brought many features.</p>
            <p>But Leopard also dropped 'Classic' support. Classic allowed you to run <i>classic</i> Mac OS 9 applications on Mac OS X. Many users had these fancy new Macs but couldn't run their old Mac OS 9 applications. And Apple refused to allow users to officially install Tiger.</p>
            <p>Project 081 aims to solve this by providing a modified Mac OS X image which has customized drivers and settings that work better with the newer hardware of Late 2007/Early 2008 Apple Macs.</p>
            <p>This works similar to a Hackintosh. Difference is this doesn't require and use a custom bootloader such as OpenCore, Clover, Chameleon or one of many other bootloaders out there. Project 081 is similar to GNU/Linux distributions in that they're a bundle of software, settings, configurations and drivers.</p>
            <h2>How does this add better hardware support?</h2>
            <p>Essentially, the installer has been modified to run a custom 'Project 081' package or .pkg. This package contains modified system files which replace the originals. You can learn more through the <a href="https://github.com/p081/wiki/wiki">Project 081 Wiki</a>. You can also view the customized packages since I tried my best to support freedom.</p>
            <h2>Download Project 081</h2>
            <p>You can download Project 081 for free. (as in price)</p>
            <p>If you need to, you can follow the <a href="p081-install-guide.php">official Project 081 installation guide</a>. <a href="p081-download.php">Download Project 081</a></p>
            <p>Keep in mind, <strong>Project 081 is based on Apple's proprietary drivers and code. My additional code is fully <i>free software</i> but may be based on non-free software.</strong></p>
            <h2>Where do I go?</h2>
            <ul>
		            <li><a href="https://github.com/p081/wiki/wiki">Wiki</a></li>
		            <li><a href="p081-download.php">Downloads</a></li>
		            <li><a href="p081-install-guide.php">Installation Guide</a></li>
            </ul>
            <h2>Thank you</h2>
            <p>Project 081 was made possible by:</p>
            <ul>
                    <li><h5>speedie</h5></li>
		            <li><h5>Lilium_Snow</h5></li>
		            <li><h5>Nobel Tech</h5></li>
		            <li><h5>dotexe1337</h5></li>
		            <li><h5>You</h5></li>
            </ul>
            <p>Thanks everyone on this list and other people who have helped me out for making this project possible.</p>
            <h2>Support the project through a donation</h2>
            <p>You can donate Monero crypto to me if you want. Monero is an <strong>anonymous</strong> cryptocurrency.</p>
            <p><img src="../img/monero-qr.png" alt="image"></p>
            <p><code>888ncoFDtpQecZvRgZf5ZCNXSmLVS3st1Yjf4k8SD4Jt4pPUpHkqzKE8UWiKFw9V5W6P946PUpmnS4YGuPkyq997LKQ3HzU</code></p>
	</div>
</body>
<footer>
	<?php include '../php/footer.php';?>
</footer>
</html>
