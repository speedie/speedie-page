<!DOCTYPE html>
<html>
<head>
<?php include '../php/projects_header.php';?>
<title>fontctrl </title>
<meta charset="UTF-8">
<meta name="description" content="fontctrl is a minimal, symlink based font manager for GNU/Linux. It handles installation, removal, and management of fonts, keeping your custom fonts easy to install, remove and manage.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h1>fontctrl</h1>
                <p>fontctrl is a minimal, symlink based font manager for GNU/Linux. It handles installation, removal, and management of fonts, keeping your custom fonts easy to install, remove and manage. Because it is symlink based, fonts can be toggled on/off without removing them. This is useful if you only want to temporarily remove a font from your system.</p>
                <p>This also makes it safer and easier to manage fonts because fontctrl will only touch fonts that it has installed. Any fonts your package manager installed will be left alone as-is. Because of this, it is less likely to break when removing fonts and other than creating symlinks in the font directory, it keeps your system cleaner than installing fonts manually.</p>
            <h2>Why would you use a font manager? I'm not new to GNU/Linux, I know how to install fonts.</h2>
                <p>fontctrl was not created to assist new people in installing fonts. Those people probably already use some desktop environment like GNOME, KDE or XFCE which comes with a graphical font manager built in. fontctrl is more for those people who have a lot of fonts and find it difficult to keep track of them. Instead of managing the fonts your package manager installs (your package manager manages those so there's no point), fontctrl tries to manage user-installed fonts.</p>
            <h2>What goals does fontctrl have?</h2>
            <ul>
		            <li><p>fontctrl tries to be minimal in design. The entire tool is written in less than 300 lines of POSIX compliant shell script.</p></li>
		            <li><p>fontctrl uses symlinks to keep track of installed fonts. This allows fonts to be enabled/disabled temporarily while fontctrl stores them elsewhere. This keeps your system clean as fontctrl only touches ~/.config/fontctrl and the font directories on your system (just for placing symlinks when enabled). You can think of this as similar to Gentoo's <code>eselect fontconfig</code>.</p></li>
		            <li><p>fontctrl tries to be hackable in design, meaning the user can easily add in any features he wants fontctrl to have. I accept pull requests that improve the project in some way or add new features if I see them as worth adding.</p></li>
		            <li><p>fontctrl is for fast installing and managing of fonts. As of 1.2 it also supports installing and managing of fonts in batch.</p></li>
            </ul>
            <h2>How do I install fontctrl?</h2>
                <p>fontctrl can be installed using the <code>git</code> command. Simply clone <a href="https://codeberg.org/speedie/fontctrl">the repository</a>, cd into it and <code>make install</code></p>
                <p>There will also be an ebuild on my Gentoo overlay for fontctrl but it is not complete yet.</p>
            <h2>How do I use fontctrl?</h2>
                <p>Every argument fontctrl has to offer can be seen by running <code>fontctrl</code> or <code>fontctrl help</code>.</p>
                <p>fontctrl has two ways to install fonts, globally and for the current user. Installing fonts globally requires superuser priviliges while installing for the current user does not. If you install fonts globally, the path for the actual font files are going to be in /etc/fontctrl/fonts and if you install for the current user, they are going to be placed in ~/.config/fontctrl/fonts.</p>
                <p>Start by downloading a font. After installing fontctrl, you can run <code>fontctrl install font</code> followed by --global if you want to install it globally. You can also add --user if you want but if not 4th argument is specified, the font will be installed for the current user only. Note that font means the font you just downloaded.</p>
                <p>The <code>install</code> command just installed the actual font file. Before we can use the font, we need to enable it. This can be done using <code>fontctrl list</code> followed by either --global or --user. Then you can <code>fontctrl enable font</code> followed by either --global or --user. Note that font here needs to be the font you want to enable. It should be listed when you ran fontctrl list. You only need to enter part of the filename, not the entire font.</p>
                <p>After the font is enabled, your applications should be able to use it. If you want to use it with something like dwm or st, run <code>fc-list | grep fontname</code>.</p>
                <p>For removal of fonts, the steps are the exact same except replace 'install' with 'remove' and 'enable' with 'disable'.</p>
                <p>For more information, run fontctrl help.</p>
            <h2>Need help?</h2>
                <p>If you are not able to figure out fontctrl, note that most issues are mentioned in the fontctrl help command. If you find a bug, consider reporting it using codeberg's 'Issues' feature. You can also <a href="mailto:speedie@speedie.site">send me an email</a> if you want.</p>
            <h3>Support this project</h3>
                <p>You can send Monero to this address:</p>
                <p><code>888ncoFDtpQecZvRgZf5ZCNXSmLVS3st1Yjf4k8SD4Jt4pPUpHkqzKE8UWiKFw9V5W6P946PUpmnS4YGuPkyq997LKQ3HzU</code></p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
