<!DOCTYPE html>
<html>
<head>
<?php include '../php/projects_header.php';?>
<title>rchat </title>
<meta charset="UTF-8">
<meta name="description" content="rchat or rautafarmi chat is a command-line rautafarmi instance client with Vim keybinds written in Bash. rchat has color support, $EDITOR support, xshbar integration, and message logging support as well as .Xresources (xrdb) and notification support through patching.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
           <h1>rchat</h1>
            <p>rchat is a command-line rautafarmi instance client with Vim keybinds written in Bash. (also works in zsh)</p>
            <img src="../img/rchat.png" alt="image">
            <p>It is very customizable and is customized through editing with an editor.</p>
            <h1>Features</h1>
            <ul>
            <li>Vim keybinds</li>
            <p>rchat was designed with Vim keybinds in mind. To enter a message, you use Insert mode and Vim keybinds to navigate.</p>
            <li>Color support</li>
            <p>rchat has support for fancy colors. If you wanna change the colors it's really easy to do.</p>
            <li>Write text in $EDITOR</li>
            <p>"In addition to simply entering text, you can do :e and write text in your defined $EDITOR.</p>
            <li>xshbar integration</li>
            <p>rchat has integration with xshbar plugins. This means you can get messages on your status bar.</p>
            <li>Multiple-instance support</li>
            <p>rchat supports multiple instances including custom ones. Simply add one.</p>
            <li>Message logging</li>
            <p>rchat logs all messages in /tmp/rchat-history. This combines messages across multiple instances.</p>
            <li>Only messaging, not bloat</li>
            <p>rchat is written in Bash, making it easy to configure with any editor. If the default features aren't enough, you can download and patch in patches that you can find below..</p>
            </ul>
            <h1>Installation</h1>
            <p>To install rchat, If you run Arch Linux or any distributions based on it, you can simply get it from the AUR (although it may be slightly outdated). Package is maintained by nezbednik and as of 0.4, is fully up-to-date. If you use 'yay', run 'yay -S rchat'. Then you can just begin using it.</p>
            <p>If you run Gentoo Linux, you can add <a href="overlay.php">my overlay</a> using layman and then just emerge it. The ebuild is maintained by me so it should be up-to-date.</p>
            <p>I recommend installing it by cloning the repository.</p>
            <p><code>cd ~</code></p>
            <p><code>git clone https://codeberg.org/speedie/rchat</code></p>
            <p><code>cd rchat</code></p>
            <p><code>make install</code></p>
            <h1>Usage</h1>
            <p>See Installation, then simply run it in a terminal. To join the default instance, simply type ':j'.</p>
            <p>To set a different instance, type ':set instance' and then enter your instance. If the instance is invalid or you wanna set it back, type ':set instance default'.</p>
            <p>From here, you can read the documentation by typing ':help'.</p>
            <h1>Patches</h1>
            <p>rchat doesn't come with features I believe a lot of people won't use. If you need more, you can use the GNU 'patch' command with one of these .diff files below.</p>
            <h3>Xresources</h3>
            <p>This patch enables .Xresources support for rchat. It allows setting the following values:
            <ul>
            <li>rchat.color1</li>
            <li>rchat.color2</li>
            <li>rchat.color3</li>
            <li>rchat.color4</li>
            <li>rchat.color5</li>
            <li>rchat.color6</li>
            <li>rchat.instance</li>
            <li>rchat.refresh</li>
            <li>rchat.linecount</li>
            <li>rchat.sep (1.0 only)</li>
            </ul>
            <p>If you use rchat 0.5, use the 0.5 patch.</p>
            <p>If you use rchat 1.0 or 1.1, use the 1.0 patch.</p>
            <p>Simply apply as usual.</p>
            <h4>Author: speedie</h4>
            <h4><a href="https://codeberg.org/speedie/rchat/raw/branch/patches/rchat-xresources-0.5.diff">rchat-xresources-0.5.diff</a></h4>
            <h4><a href="https://codeberg.org/speedie/rchat/raw/branch/patches/rchat-xresources-1.0.diff">rchat-xresources-1.0.diff</a></h4>
            <h4><a href="https://codeberg.org/speedie/rchat/raw/branch/patches/rchat-xresources-1.2.diff">rchat-xresources-1.2.diff</a></h4>
            <h4><a href="https://codeberg.org/speedie/rchat/raw/branch/patches/rchat-xresources-1.3-r2.diff">rchat-xresources-1.3-r2.diff</a></h4>
            <h3>Notifications</h3>
            <img src="../img/notif.png" alt="image">
            <p>This patch enables support for notifications in rchat using 'notify-send'. Make sure to install 'libnotify-send' before using.</p>
            <h4>Author: speedie</h4>
            <h4><a href="https://codeberg.org/speedie/rchat/raw/branch/patches/rchat-notification-0.5.diff">rchat-notification-0.5.diff</a></h4>
            <h4><a href="https://codeberg.org/speedie/rchat/raw/branch/patches/rchat-notification-1.0.diff">rchat-notification-1.0.diff</a></h4>
            <h4><a href="https://codeberg.org/speedie/rchat/raw/branch/patches/rchat-notification-1.3-r2.diff">rchat-notification-1.3-r2.diff</a></h4>
            <h3>Open URL</h3>
            <p>This patch adds a ':open' feature which opens the last URL displayed in your terminal.</p>
            <p>Really simple, but also extremely useful if you want to view links quickly.</p>
            <p>1.1 version likely compatible with 1.0!</p>
            <h4>Author: speedie</h4>
            <h4><a href="https://codeberg.org/speedie/rchat/raw/branch/patches/rchat-openurl-1.1.diff">rchat-openurl-1.1.diff</a></h4>
            <h4><a href="https://codeberg.org/speedie/rchat/raw/branch/patches/rchat-openurl-1.2.diff">rchat-openurl-1.2.diff</a></h4>
            <h3>Art</h3>
            <img src="../img/rfarmi.png" alt="image">
            <p>This patch adds support for art in rchat that runs before you join an instance. The art is placed in $HOME/.config/rchat/rchat_art and will run if present. The file can contain ANSI escape codes if you want colors.</p>
            <h4>Author: speedie</h4>
            <h4><a href="https://codeberg.org/speedie/rchat/raw/branch/patches/rchat-art-1.2.diff">rchat-art-1.2.diff</a></h4>
            <h3>Tput colors</h3>
            <img src="../img/rchat-tput.png" alt="image">
            <p>This patch replaces the ANSI escape codes with tput commands. This allows for more colors and also allows you to separate your rchat color scheme from your terminal color scheme (Useful if you use Pywal).</p>
            <h4>Author: speedie</h4>
            <h4><a href="https://codeberg.org/speedie/rchat/raw/branch/patches/rchat-tputcolors-1.3-r2.diff">rchat-tputcolors-1.3-r2.diff</a></h4>
            <h3>Hide status</h3>
            <img src="../img/rchatdebloater.png" alt="image">
            <p>This patch removes the rchat (vernum), separator and instance status from the chat area. This means messages are going to fill up the entire terminal window minus one line (used for the Insert mode).</p>
            <h4>Author: speedie</h4>
            <h4><a href="https://codeberg.org/speedie/rchat/raw/branch/patches/rchat-hidestatus-1.41.diff">rchat-hidestatus-1.41.diff</a></h4>
            <h3>Insert mode is editor</h3>
            <p>This patch replaces insert mode (i) with :e (Editing the message in $EDITOR). This may be useful if you prefer to edit messages in an editor like Vim or Emacs or need multiple lines very often. You can still access the regular Insert mode by pressing e though.</p>
            <h4>Author: speedie</h4>
            <h4><a href="https://codeberg.org/speedie/rchat/raw/branch/patches/rchat-itoedit-1.41.diff">rchat-itoedit-1.41.diff</a></h4>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
