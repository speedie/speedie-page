<!DOCTYPE html>
<html>
<head>
<?php include '../php/projects_header.php';?>
<title>Patches</title>
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
        <h2 id="patches">Patches</h2>
            <p>This is a list of (almost) all patches I’ve written.</p>
            <h3 id="dmenu">dmenu</h3>
            <ul>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/commit/c07fde5df8340a4b6f2452a5a261e339ecf743d7/dmenu-borderwhencentered-5.1.diff">borderwhencentered</a>
            <ul>
            <li>This patch adds the ability to draw the <a
            href="https://tools.suckless.org/border">border</a> but only when
            centered.</li>
            </ul></li>
            </ul>
            <h3 id="dwm">dwm</h3>
            <ul>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/commit/c07fde5df8340a4b6f2452a5a261e339ecf743d7/dwm-alttagcolors-6.4.diff">alttagcolors</a>
            <ul>
            <li>This patch allows you to define colors for each tag, similar to the
            old <a
            href="https://dwm.suckless.org/patches/rainbowtags">rainbowtags</a>
            patch.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/commit/c07fde5df8340a4b6f2452a5a261e339ecf743d7/dwm-awesomebar-6.4.diff">awesomebar
            (6.4)</a>
            <ul>
            <li>Updated version of <a
            href="https://dwm.suckless.org/patches/awesomebar">awesomebar</a> which
            prevents a crash when clicking on the title if there’s no clients. It
            also adds a <code>hideall</code> function in addition to the existing
            showall function.</li>
            </ul></li>
            <li><a href="https://dwm.suckless.org/patches/bartoggle">bartoggle</a>
            <ul>
            <li>Bartoggle allows you to toggle (enable/disable) each part of the bar
            individually using keybinds.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/commit/c07fde5df8340a4b6f2452a5a261e339ecf743d7/dwm-centerwindow-6.4.diff">centerwindow</a>
            <ul>
            <li>Allows you to center a client on the fly instead of every time
            there’s a single client.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/commit/c07fde5df8340a4b6f2452a5a261e339ecf743d7/dwm-current-desktop-6.3.diff">updatecurrentdesktop
            (6.4)</a>
            <ul>
            <li>6.4 update of the very old updatecurrentdesktop patch on the
            suckless website. Self explanatory, but you might want to use ewmhtags
            instead.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/commit/c07fde5df8340a4b6f2452a5a261e339ecf743d7/dwm-defaultstatus-6.4.diff">defaultstatus</a>
            <ul>
            <li>Very simple patch which allows you to override the default
            ‘dwm-VERSION’ status.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/branch/master/dwm-gestures-6.4.diff">gestures
            (6.4)</a>
            <ul>
            <li>Updated patch of a non-functional old patch from around 2008 which
            allows you to run functions by moving your mouse in one or more
            directions.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/branch/master/dwm-inset-6.4.diff">inset</a>
            <ul>
            <li>Alternative version of the insets patch from the suckless website.
            This one is simpler and can be configured using .Xresources.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/branch/master/dwm-keypressrelease-6.4.diff">keypressrelease
            (6.4)</a>
            <ul>
            <li>Updated version of the keypressrelease patch. Not much to be said
            here as the old one was functional, just didn’t apply cleanly.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/branch/master/dwm-layout-title-colorschemes-6.4.diff">layout-title-colorschemes</a>
            <ul>
            <li>Patch which adds colorschemes for the title and the layout
            indicator. There is the colorbar patch but it doesn’t seem to be as
            useful.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/branch/master/dwm-nolayout-6.4.diff">nolayout</a>
            <ul>
            <li>Patch which (permanently) removes the layout indicator from the bar.
            Smaller version of the bartoggle patch.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/branch/master/dwm-nomouse-6.4.diff">nomouse</a>
            <ul>
            <li>Patch which removes all mouse support from dwm saving code. May be
            useful if you don’t need or want mouse support at all.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/branch/master/dwm-query-6.4.diff">query</a>
            <ul>
            <li>Patch which allows you to “query” dwm for information about it, such
            as your number of tags, if your bar is hidden, your bar position and so
            on.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/branch/master/dwm-scrolltags-6.4.diff">scrolltags</a>
            <ul>
            <li>Patch which overcomplicates scrolling through tags. I don’t
            recommend using this patch.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/branch/master/dwm-setbarheight-6.4.diff">setbarheight</a>
            <ul>
            <li>Patch which allows you to set the bar height using keybinds. Does
            not conflict with the barheight patch.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/branch/master/dwm-setbarpadding-6.4.diff">setbarpadding</a>
            <ul>
            <li>Patch which allows you to set the bar padding using keybinds.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/branch/master/dwm-status2d-statuscmd-6.4.diff">status2d-statuscmd</a>
            <ul>
            <li>Combo patch of status2d and statuscmd. It also writes $BUTTON to a
            file in addition to setenv() making it less buggy. There are more
            variants of the same patch, see the repository for that.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/branch/master/dwm-tag-preview-barpadding-6.4.diff">tag-preview-barpadding</a>
            <ul>
            <li>Version of the Tag preview patch which supports bar padding.</li>
            </ul></li>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/branch/master/dwm-tag-preview-vanitygaps-6.4.diff">tag-preview-vanitygaps</a>
            <ul>
            <li>Version of the Tag preview patch which supports vanitygaps. There is
            also a combo patch of both tag previews, barpadding and vanitygaps in my
            repo.</li>
            </ul></li>
            <li><a
            href="https://dwm.suckless.org/patches/toggletopbar">toggletopbar</a>
            <ul>
            <li>Allows you to toggle the top bar. Conflicts with barpadding so
            there’s a version for that, see the repo.</li>
            </ul></li>
            </ul>
            <h3 id="st">st</h3>
            <ul>
            <li><a
            href="https://codeberg.org/speedie/patches/raw/branch/master/st-sixel-0.9.diff">st-sixel</a>
            <ul>
            <li>Sixel support for st. This is a cleaned up version of someone else’s
            work, see note at the top.</li>
            </ul></li>
            </ul>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
