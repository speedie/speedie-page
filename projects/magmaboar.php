<!DOCTYPE html>
<html>
<head>
<?php include '../php/projects_header.php';?>
<title>MagmaBoar </title>
<meta charset="UTF-8">
<meta name="description" content="MagmaBoar is a Firefox profile designed to give you privacy & freedom while using regular up-to-date Firefox (unlike GNU IceCat and similar forks). It enables many privacy options and tweaks which helps improve your privacy. In addition to this, it comes with many extensions like uBlock Origin, LibreJS, Decentraleyes and more.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>🔥🐗MagmaBoar</h2>
            <p>MagmaBoar is a Firefox profile optimized to provide privacy and freedom while browsing the web. Unlike other options such as GNU IceCat, Because MagmaBoar is just a Firefox profile, it can easily be modified and used with more modern versions of the web browser.</p> 
            <img src="../img/magmaboar-img.png" alt="image">
            <p>The MagmaBoar Firefox profile comes with many tweaks (see prefs.js) and extensions which prevents spyware from running in your browser.</p>
            <p>Here's a list of extensions:</p>
            <ul>
                <li><h5> HTTPS Everywhere</h5></li>
                <li><h5> Decentraleyes</h5></li>
                <li><h5> CanvasBlocker</h5></li>
                <li><h5> Privacy Badger</h5></li>
                <li><h5> ClearURLs</h5></li>
                <li><h5> uBlock Origin</h5></li>
                <li><h5> LibreJS</h5></li>
                <li><h5> Chameleon</h5></li>
                <li><h5> ff2mpv (Workaround for people who want to watch non-free video websites)</h5></li>
                <li><h5> Violentmonkey</h5></li>
            </ul>
            <h4>Something to note is that the LibreJS extension can cause some issues if you visit websites which have non-free JavaScript such as YouTube. To solve the YouTube issue, I included ff2mpv which can send videos to mpv and play them. Otherwise, you can blacklist the websites you need to view but this is going to severely decrease your privacy.</h4>
            <h2>How can I know if this works well or not?</h2>
            <p>You can check websites like <a href="https://deviceinfo.me">deviceinfo.me</a> which are designed to test your browser's privacy settings.</p>
            <h2>💾Installation</h2>
            <h4>NOTE: These instructions ONLY apply to GNU/Linux.</h4>
            <p>First, make sure to install Firefox using your favorite package manager on GNU/Linux.</p>
            <p>Then <code>cd ~/.mozilla/firefox/*.default-release</code>. Delete everything in this directory.</p>
            <p>Download the tar.gz and unpack it using <code>tar xpf /path/to/tar.gz ; cd magmaboar</code>.</p>
            <p>Finally, copy the contents to <code>~/.mozilla/firefox/*.default-release</code>. If it isn't enabled, type in <code>about:profile</code> and set it as default.</p>
            <h2>⬇ Download</h2>
            <p>You can download the latest tar.gz below!</p>
            <p><a href="https://raw.githubusercontent.com/speediegq/magmaboar/main/magmaboar-0.1.tar.gz">magmaboar-0.1.tar.gz</a></p>
            <h2>❗Reporting issues</h2>
            <p>You can create issues on the GitHub repository <a href="https://github.com/speediegq/magmaboar">here.</a></p>
            <h2>💰Support me</h2>
            <p>If this Firefox profile was useful to you, consider sending a small <a href="/donate.php">donation</a>.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
