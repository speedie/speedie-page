<!DOCTYPE html>
<html>
<head>
<?php include '../php/projects_header.php';?>
<title>speedie's Gentoo overlay</title>
<meta charset="UTF-8">
<meta name="description" content="Add my Gentoo overlay.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>speedie's Gentoo overlay</h2>
                <p>Since I maintain forks of software as well as some of my own, I decided to create a Gentoo overlay so that you can install my dotfiles and software using <code>portage</code>.</p>
                <p>As of now, this requires <code>layman</code>. This can be installed using <code>portage</code> as well.</p>
            <h3>What is an overlay?</h3>
                <p>An overlay is pretty much a third party Gentoo repository with other ebuilds not normally found in the regular Gentoo repositories. Often it's the only way to install software other than compiling from source yourself.</p>
            <h3>What packages does this overlay offer?</h3>
                <p>See 'Ebuild list' further down this page.</p>
            <h3>What distributions can I install this on?</h3>
                <p>Gentoo is the only distro I've tested this on. It might work on other Gentoo based distributions that use the <code>portage</code> package manager but it will NOT work on Arch, Debian or other distros that are not Gentoo based.</p>
            <h3>How can I add this overlay?</h3>
                <p>Using either eselect-repository or layman. Layman is no longer maintained so I do not recommend you use it.</p>
                <p>For layman: <code>layman -o https://speedie.site/overlay.xml -f -a speedie-overlay</code></p>
                <p>For eselect-repository: <code>eselect repository --add speedie-overlay git https://git.speedie.site/speedie-overlay</code></p>
                <p>To sync with layman: <code>layman -s speedie-overlay</code></p>
                <p>To sync with eselect-repository: <code>emaint -r speedie-overlay sync</code></p>
            <h3>Ebuild list</h3>
                <p>See the repository for a full list.</p>
            <h3>Where are the ebuilds hosted?</h3>
                <p>The ebuilds are all hosted on <a href="https://git.speedie.site/speedie-overlay">my cgit instance</a>.</p>
            <h3>Support me</h3>
                <p>If this overlay was useful, consider donating.</p>
                <p>XMR: <code>888ncoFDtpQecZvRgZf5ZCNXSmLVS3st1Yjf4k8SD4Jt4pPUpHkqzKE8UWiKFw9V5W6P946PUpmnS4YGuPkyq997LKQ3HzU</code></p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
