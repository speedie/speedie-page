<!DOCTYPE html>
<html>
<head>
<?php include '../php/projects_header.php';?>
<title>speedie's Arch repository</title>
<meta charset="UTF-8">
<meta name="description" content="Add my Arch repository.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>speedie's Arch repository</h2>
                <p>Since I maintain forks of software as well as some of my own, I decided to create an Arch repository so that you can install my dotfiles and software using <code>pacman</code>.</p>
            <h3>What packages does this repository offer?</h3>
                <p>See the repository for a full list.</p>
            <h3>What distributions can I install this on?</h3>
                <p>The repository should be functional on most if not all Arch based distributions. I have only tested it on Arch however.</p>
            <h3>How can I add this overlay?</h3>
                <p>First of all, this is a signed repository. This means you need to add my PGP keys before you can use the repository. To do this, run the following commands:</p>
                    <code>pacman-key --recv-keys CEB863B830D1318A</code>
                    <code>pacman-key --lsign-key CEB863B830D1318A</code>
                <p>Now, edit <code>/etc/pacman.conf</code> with your favorite editor and add the following lines to the file:</p>
                    <p><code>[speedie-aur]</code></p>
                    <p><code>SigLevel = Required DatabaseNever</code></p>
                    <p><code>Server = https://git.speedie.site/speedie-aur/plain/$arch</code></p>
                <p>Write it, and run <code>pacman -Sy</code> to sync the repositories. If the sync was successful, you can now start installing my packages.</p>
            <h3>Where are the packages hosted?</h3>
                <p>The packages are all hosted on <a href="https://git.speedie.site/speedie-aur">my cgit instance</a>.</p>
            <h3>Support me</h3>
                <p>If this overlay was useful, consider donating.</p>
                <p>XMR: <code>888ncoFDtpQecZvRgZf5ZCNXSmLVS3st1Yjf4k8SD4Jt4pPUpHkqzKE8UWiKFw9V5W6P946PUpmnS4YGuPkyq997LKQ3HzU</code></p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
