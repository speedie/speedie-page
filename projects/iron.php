<!DOCTYPE html>
<html>
<head>
<?php include '../php/projects_header.php';?>
<title>iron</title>
<meta charset="UTF-8">
<meta name="description" content="iron is a minimal rautafarmi client written in Bash for GNU/Linux. It allows the user to post/recieve messages using the rautafarmi API and it displays the information to the user in a nice format. iron is based on the older rchat rautafarmi client that used messages.txt to get messages.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h1>iron</h1>
            <img src="../img/iron.png" width="700" alt="image">
            <h2 id="what-is-iron">What is iron?</h2>
            <p>iron is a minimal rautafarmi client written in Bash for GNU/Linux. It allows the user to post/recieve messages using the <a href="https://donut.gq/rautafarmi/api.php">rautafarmi API</a> and it displays the information to the user in a nice format. iron is based on the older <a href="https://speedie.site/rchat">rchat</a> rautafarmi client that used messages.txt to get messages.</p>
            <h2 id="why-should-i-use-iron-over-rchat">Why should I use iron over rchat?</h2>
            <p>Unlike rchat, iron is minimal in design and only does what you expect a basic client to do. It does not have auto updating, no history, no donate command, no news, no changelog, and no :open. It also brings a much cleaner and improved codebase. Because of this, bugs are less likely and the client is much faster. The user doesn&#39;t have to run lines of code he doesn&#39;t need.</p>
            <p>Most importantly though, iron does not use the old messages.txt method used by older instances. This also means iron is the first rautafarmi client to use and support the new JSON based rautafarmi API. This API is much faster and will allow compatible instances to push updates without breaking client support.</p>
            <h2 id="features">Features</h2>
            <ul>
            <li>Vim-like<br>iron much like rchat is Vim like. Just like Vim, iron has different modes which are used in Vim like ways. You can even use Vim to compose your message if you want (using the &#39;:e&#39; command).</li>
            <li>16 color support<br>iron supports 16 colors out of the box. There will also be a 256-color patch for terminals that support it as soon as the rchat patch has been ported over.</li>
            <li>4chan-like green text<br>iron has 4chan-like green text for messages that start with &#39;&gt;&#39;.</li>
            <li>Support for the new, fast <a href="https://donut.gq/rautafarmi/api.php">rautafarmi API</a><br>iron is the first unofficial Rautafarmi client with support for the new Rautafarmi API. This API is much faster and less likely to break.</li>
            <li>No pointless features instead.<br>iron does not have pointless features slowing it down. If the user wants a feature that does not exist, he can easily implement it. Patches can be submitted and the user can apply it to his iron copy using the patch coreutil.</li>
            <li>Hackable<br>Because it is written in Bash, it is highly flexible and hackable allowing it to do many things with minimal effort by the user.</li>
            </ul>
            <h2 id="modes">Modes</h2>
            <p>iron has three modes - Normal mode, Command mode and Insert mode. When you first load messages, you will be in Normal mode. This mode is simply used for reading and entering another mode. If you choose to enter Command mode, you&#39;re able to perform actions to iron such as exiting or composing a message in an editor.</p>
            <p>If you press &#39;i&#39;, you&#39;ll be in Insert mode. This mode is used to compose messages. This will bring up a little area where you can type your message. If you press enter, your message will be sent. If you want to send messages with multiple lines, press e or :e to open up a file in an editor ($EDITOR).</p>
            <h2 id="usage">Usage</h2>
            <p>Simply run iron inside a terminal emulator or something like tmux. For iron to work though, it needs to connect to an instance. As of writing this document, there is only one. You can find it <a href="https://donut.gq/rautafarmi">here</a>. Run &#39;:set instance&#39; and then type in the URL of the instance. If you press enter, the instance will be set. This is saved until settings are reset (or ~/.local/share/iron is deleted). To join the instance, type &#39;:j&#39; and press Enter. This will (assuming dependencies are installed and you are connected to the internet) download the API and then print messages to the screen. Here, you are in Normal mode.</p>
            <p>From here, you may run commands, or enter Insert mode to compose a message. See &#39;Commands&#39; or &#39;Keybinds&#39; for more information.</p>
            <h2 id="installation">Installation</h2>
            <p>Download it (see &#39;Download&#39;) and run &#39;make install&#39; as root.</p>
            <h2 id="download">Download</h2>
            <p>iron can be downloaded using dev-vcs/git. To do this:</p>
            <pre><code>- cd
            - git clone https://codeberg.org/speedie/iron
            - cd iron
            </code></pre><h2 id="commands">Commands</h2>
            <ul>
            <li>&#39;:q&#39; - Quit iron</li>
            <li>&#39;:l&#39; - Leave the instance</li>
            <li>&#39;:r&#39; - Refresh</li>
            <li>&#39;:e&#39; - Compose a message in $EDITOR (or Vi if none is specified)</li>
            <li>&#39;:reset&#39; - Reset all settings (including instance) to default.</li>
            </ul>
            <h2 id="keybinds">Keybinds</h2>
            <ul>
            <li>i - Enter insert mode</li>
            <li>r - Refresh messages</li>
            <li>: - Enter command mode</li>
            <li>e - Compose a message in $EDITOR</li>
            <li>q - Leave the instance</li>
            <li>l - Leave the instance</li>
            <li>ZQ - Quit iron</li>
            <li>ZZ - Leave the instance</li>
            </ul>
            <h2 id="patches">Patches</h2>
            <p>To submit patches, Create a pull request with your patch in the <a href="https://codeberg.org/speedie/iron/src/branch/patches">patches branch</a> or <a href="mailto:speedie@speedie.site">send me an email</a>. Patches can be generated using the &#39;diff -up&#39; command on a GNU/Linux system.</p>
            
            <h4 id="tput-colors">tput colors</h4>
            <p>This patch replaces the ANSI escape codes with tput commands. This allows for more colors and also allows you to separate your iron color scheme from your terminal color scheme (Useful if you use Pywal).</p>
            <p>NOTE: The 1.1 version is compatible with 1.2 too.</p>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-tputcols-1.0.diff">iron-tputcols-1.0.diff</a>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-tputcols-1.1.diff">iron-tputcols-1.1.diff</a>
            
            <h4 id="embed-image">Embed Image</h4>
            <p>This patch adds new image functionality to iron. Specifically it allows you to post images (using the imgurl field). To post an image, simply add the link somewhere in your message. It will be moved to the Image URL field before the post is sent to the instance.</p>
            <p>NOTE: The 1.1 version is compatible with 1.2 too.</p>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-embed-image-1.0.diff">iron-embed-image-1.0.diff</a>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-embed-image-1.1.diff">iron-embed-image-1.1.diff</a>
            
            <h4 id="xresources">.Xresources</h4>
            <p>This patch adds xrdb or .Xresources support to iron. Note that values must be defined with the 'iron' prefix to be used. You may modify the patch to use any values if you want which would enable Pywal support.</p>
            <p>NOTE: The 1.0 version is compatible with 1.1 and 1.2 too.</p>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-xresources-1.0.diff">iron-xresources-1.0.diff</a>
            
            <h4 id="history">History</h4>
            <p>This patch adds keybinds for viewing older/newer messages. If you press 'u', you will see the newest messages. If you press 'd', you will go down the history.</p>
            <p>Do note that this requires parsing ALL messages, not just the terminal size meaning loading messages takes longer. Because of this, there are two variants. The regular variant will download and parse every single message on load and then will not parse it again. The ondemand variant will only parse the old messages when navigating through the history. I personally prefer this variant!</p>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-viewhist-1.1.diff">iron-viewhist-1.1.diff</a>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-viewhist-ondemand-1.1.diff">iron-viewhist-ondemand-1.1.diff</a>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-viewhist-1.2.diff">iron-viewhist-1.2.diff</a>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-viewhist-ondemand-1.2.diff">iron-viewhist-ondemand-1.2.diff</a>
            
            <h4 id="nickname">Nickname</h4>
            <p>This patch allows the user of iron to set nicknames for themselves. It will be saved until the ':reset' command is used (data is cleared) or the nickname is changed.</p>
            <p>Once a nickname is set, messages will be sent with it. To set a nickname, simply use the ':set nickname' command.</p>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-nickname-1.1.diff">iron-nickname-1.1.diff</a>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-nickname-1.2.diff">iron-nickname-1.2.diff</a>
            
            <h4 id="hidemessages">Hide messages</h4>
            <p>This patch adds two commands, ':hide' and ':show'. The hide command will open all messages sent inside $EDITOR (or Vim if $EDITOR is not defined) and allow the user to make changes. A patch will then be created that will be applied on startup. The show command does the complete opposite, it reverts the patch.</p>
            <p>This means the patch is useful for other things because it allows you to modify the messages in any way you want (or even create new ones client side).</p>
            <p>NOTE: The 1.1 version is compatible with 1.2 too.</p>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-hidemessages-1.1.diff">iron-hidemessages-1.1.diff</a>
            
            <h4 id="notification">Notification</h4>
            <p>This patch implements notification support. With this patch, the latest message along with the username will be sent as a notification. It requires a notification daemon and libnotify-send to work. If it is not available, no notification will be sent.</p>
            <p>NOTE: The 1.0 version is compatible with 1.1 and 1.2 too.</p>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-notification-1.0.diff">iron-notification-1.0.diff</a>
            
            <h4 id="openurl">Open URL</h4>
            <p>This patch adds a keybind to open a URL. To open the latest image sent (using the image field), you use the ':i' command. To open the latest URL sent (using the message field), you use the ':o' command. By default this patch uses xdg-open but you can change the browser to anything you want.</p>
            <p>NOTE: The 1.0 version is compatible with 1.1 and 1.2 too.</p>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-openurl-1.0.diff">iron-openurl-1.0.diff</a>
            
            <h4 id="itoedit">Insert mode editor</h4>
            <p>This patch replaces the 'i' bind with the action of the ':e' command. You can still access the old insert mode by pressing 'e' though.</p>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-itoedit-1.0.diff">iron-itoedit-1.0.diff</a>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-itoedit-1.1.diff">iron-itoedit-1.1.diff</a>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-itoedit-1.2.diff">iron-itoedit-1.2.diff</a>
            
            <h4 id="art">Art</h4>
            <p>This patch allows you to add art or general text to display in the iron starter menu.</p>
            <p>NOTE: The 1.0 version is compatible with 1.1 and 1.2 too.</p>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-art-1.0.diff">iron-art-1.0.diff</a>
            
            <h4 id="refreshzoom">Refresh Zoom</h4>
            <p>This patch allows iron to refresh its borders and messages based on terminal size without any reloading. This is done inside the IRON_HEADER function so any time the header is printed, the size is updated first.</p>
            <p>Was considering merging this into main but decided not to as not everyone would want this!</p>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-refreshzoom-1.2.diff">iron-refreshzoom-1.2.diff</a>
            
            <h4 id="hidestatus">Hide status</h4>
            <p>This patch removes the iron version number, all separators and the instance status from the chat area so that all space (minus two lines) will be used for messages.</p>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-hidestatus-1.0.diff">iron-hidestatus-1.0.diff</a>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-hidestatus-1.1.diff">iron-hidestatus-1.1.diff</a>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-hidestatus-1.2.diff">iron-hidestatus-1.2.diff</a>
            
            <h4 id="speedie">speedie's combo patch</h4>
            <p>This patch is a combo patch. It adds embed image, hide messages, i to edit, nickname, tput colors and view history patches into one patch that can be applied to a clean iron build.</p>
            <p>This diff was created for use with the Gentoo ebuild (because a few of the patches I use conflict with each other). If you want these patches, simply apply this patch.</p>
            <p>NOTE: The 1.2-r2 patch adds the refreshzoom patch as well.</p>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-speedie-1.1.diff">iron-speedie-1.1.diff</a>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-speedie-1.2.diff">iron-speedie-1.2.diff</a>
            <a href="https://codeberg.org/speedie/iron/raw/branch/patches/iron-speedie-1.2-r2.diff">iron-speedie-1.2-r2.diff</a>
            
            <h2 id="license">License</h2>
            <p>iron, <strong>along with any contributions made to it (such as all user-submitted patches)</strong> are licensed under the GNU GPLv3 free software license.</p>
            <h2 id="issues">Issues</h2>
            <p>Submit them <a href="https://codeberg.org/speedie/iron/issues">in the Codeberg repository</a> or let me know elsewhere such as <a href="mailto:speedie@speedie.site">email</a>.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
