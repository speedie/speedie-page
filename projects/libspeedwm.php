<!DOCTYPE html>
<html>
<head>
<?php include '../php/projects_header.php';?>
<title>libspeedwm</title>
<meta charset="UTF-8">
<meta name="description" content="libspeedwm is a minimal program for interacting with speedwm through the shell.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
           <h1>libspeedwm</h1>
             <p>libspeedwm is a minimal program for interacting with speedwm through the shell. It allows you to run signals but also perform actions. It is written in C because speedwm itself is written in C. It only has one dependency; libX11 used to set the root window name.</p>
           <h2>Why should I use libspeedwm?</h2>
             <p>While yes, you <em>can</em> run signals manually (speedwm -s "#cmd:10"), this is slightly risky. If speedwm gets updated and its signals get changed, your script will be using the old signals potentially breaking your script completely. libspeedwm solves this because libspeedwm can and will be updated alongside speedwm which means all of the actions are going to work on the latest version.</p>
           <h2>What are the downsides of libspeedwm?</h2>
             <p>This does come with a few downsides though. First is that libspeedwm will be a dependency for your script/project. It is an external dependency and is not bundled with speedwm itself.</p>
             <p>Secondly, if the user is on an old speedwm version, the libspeedwm version must also be old or scripts will break just the same as using signals normally. This comes with a huge problem: there are bugs in the old libspeedwm version.</p>
           <h2>Usage</h2>
             <p>See <code>libspeedwm --help</code>
           <h2>Download</h2>    
             <a href="https://codeberg.org/attachments/d23d09d7-b4dc-4d8a-a433-1dfde2eb07cd">libspeedwm-1.5.tar.gz</a>
             <p>The latest and previous releases of libspeedwm can be found <a href="https://codeberg.org/speedie/libspeedwm">here</a></p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
