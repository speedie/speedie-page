<!DOCTYPE html>
<html>
<head>
<?php include 'php/header.php';?>
<meta charset="UTF-8">
<meta name="description" content="Welcome to my personal website and blog about free software projects and minimalism.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>speedie's site</title>
</head>
<body>
		<div class="content">
				<h2>Hello there!</h2>
                        <p>Hello, I'm speedie and this is my personal website and blog. I'm a free software enthusiast, tech minimalist and creator of projects like <a href="https://speedwm.speedie.site">speedwm</a>, <a href="https://spmenu.speedie.site">spmenu</a>, <a href="https://github.com/ForwarderFactory">Forwarder Factory</a> and <a href="/projects/project081.php">Project 081</a>. Take a look around!</p>
						<ul>
							<li><a href="blog.php">Blog</a></li>
                                <p>My blog, usually about free software or personal interests.</p>
							<li><a href="project-list.php">Projects</a></li>
                                <p>A list of my programming projects.</p>
							<li><a href="faq.php">Frequently asked questions</a></li>
                                <p>Frequently asked questions are answered here.</p>
							<li><a href="rss.xml">RSS feed</a></li>
                                <p>RSS feed, all blog posts are posted in full.</p>
							<li><a href="updates.xml">RSS project updates</a></li>
                                <p>RSS feed for project updates. Follow this feed if you are a user of my software.</p>
							<li><a href="projects/overlay.php">Gentoo overlay</a></li>
                                <p>I maintain a Gentoo overlay, that is an unofficial Gentoo repository with custom packages written by myself which extend the packages you can install on a Gentoo system.</p>
							<li><a href="projects/repository.php">Arch repository</a></li>
                                <p>I also maintain an Arch user repository containing almost the same packages. This is al</p>
						</ul>
                            <p>Other services/websites:</p>
                        <ul>
                            <li><a href="https://matrix.to/#/#speediegq:matrix.org">Matrix space</a></li>
                                <p>I've been getting into Matrix recently, so feel free to join my space. There's also an end-to-end encrypted space for those with clients that support it.</p>
                            <li><a href="https://github.com/speediegq">GitHub</a></li>
                                <p>My GitHub account. It is more or less used as a backup, and most of my projects are not primarily hosted there.</p>
                            <li><a href="https://codeberg.org/speedie">Codeberg</a></li>
                                <p>My Codeberg account. It is more or less used as a backup, and most of my projects are not primarily hosted there. This site however <em>is</em> hosted there.</p>
                            <li><a href="https://www.youtube.com/@speediesite">YouTube</a></li>
                                <p>My YouTube channel. Most of what I post is technology related, usually related to my personal projects.</p>
                        </ul>
                <h3>Email me</h3>
                    <p>Feel free to <a href="mailto:speedie@speedie.site">send me encrypted or unencrypted email.</a> My public GPG key is available <a href="https://ls.speedie.site/pubkey.asc">here</a>, and my email address is <code>speedie@speedie.site</code>. The email is stored on my VPS.</p>
                    <code>gpg --recv-keys CEB863B830D1318A</code>
		</div>
</body>
<footer>
		<?php include 'php/footer.php';?>
</footer>
</html>
