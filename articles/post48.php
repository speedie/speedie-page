<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Software update</title>
<meta charset="UTF-8">
<meta name="description" content="Today I want to just quickly talk about some important (and some lesser important) stuff regarding my software projects.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Software update</h2>
                <h4>2023-02-25</h4>
                    <p>Today I want to just quickly talk about some important (and some lesser important) stuff regarding my software projects. First of all, let's talk about spmenu. In addition to the color support I added a month or two ago, spmenu now has image support. I initially couldn't think of many uses for this, however it's actually really useful. Let's say I want to list out all my wallpapers and set one as my wallpaper. My current wallpaper script actually uses a separate program for that, sxiv. But now that spmenu has image support I can actually just preview the wallpapers in spmenu right away, without an external program. This actually makes it more powerful than a lot of terminal emulators. Pretty cool if you ask me.</p>
                    <p>I have also made significant changes to another one of my projects, that being spDE. It now includes a helpful script which handles starting speedwm but more importantly handles installing configuration files. This means to install spDE now you only need to emerge it and use the spde commmand to set it up with configuration files. Saves even more time for me, and if you need an easy to use but efficient desktop environment which allows you to easily get into efficient software it's also good for that.</p>
                    <p>I also have a new project now called ivtools. ivtools is mostly a collection of ffmpeg and imagemagick snippets, whenever I need to do something with a video file I will put the ffmpeg command in a script. The goal is to eventually have scripts for most simple things a video editor can do, so that you don't need to start bloated editors like Blender or kdenlive when you just want to do basic video editing like adding music or cutting video. You can get ivtools <a href="https://git.speedie.site/ivtools">here</a>.</p>
                    <p>I have recently started using a program called dnote. dnote is a dmenu fork, but the only similarity it retains is libdrw and the general structure of dmenu because dnote is a notification daemon. This similarity to dmenu makes it really easy to add new things into it. I have myself added .Xresources and alpha into dnote. You can get my build <a href="https://git.speedie.site/dnote">here</a> if you want to try it out. You'll definitely see me using this in shell scripts soon, in fact I plan on migrating speedwm modules and speedwm-extras scripts to using it. It's great, pretty much notifications but better in every way.</p>
                    <p>As for speedwm, I'm pretty happy with it and while I have plans on doing things with it, they'll probably be in the form of a new project. For one, I want a rule and keybind configuration file which will require major changes to be made. I also want to rewrite the status, so that each status module is a different bar module.</p>
                    <p>Anyways, that was just an update on the projects I've been working on. No need to drag this blog post out longer so thank you for reading, have a good day!</p>

</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>

