<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>speedwm 1.9 release</title>
<meta charset="UTF-8">
<meta name="description" content="speedwm 1.9 was released so I thought I'd write this short blog post. Yes, there is a real blog post in the works as well, in fact it's basically complete, but I think writing this is necessary.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>speedwm 1.9 release</h2>
                <h4>2023-02-05</h4>
                <p>speedwm 1.9 was released so I thought I’d write this short blog post. Yes, there is a <em>real</em> blog post in the works as well, in fact it’s basically complete, but I think writing this is necessary.</p>
                <p>Either way, speedwm 1.9 is out now, and I want to talk about some of the changes I’ve made to it, since there are some big changes that will definitely be noticed if you just update from 1.8 to 1.9. First of all, if you just updated, you might notice that most of the keybindings have been removed. Many of the chained keybindings have been removed, because I simply wasn’t ever using them. But that’s not what you noticed, is it?</p>
                <p>I removed most of the keybindings that call <code>spawn()</code> (the function that runs a program) because if I leave them in, people need to have all that software installed to make the keybindings functional. That’s not great, so instead of dealing with that I simply made a copy of the old <code>mouse.h</code> and <code>keybinds.h</code> and uploaded them to my <code>dotfiles</code> repository on Codeberg.</p>
<p>Now, of course I still use the old list of keybinds myself, so how am I going to update keybinds efficiently? Well, that’s why I added a few lines to the Makefile. If there is a <code>keybinds.rl.h</code> for example, it will be temporarily copied to <code>keybinds.h</code> and the old file will be restored after the compilation, so the original file is intact but the binary contains the keybindings from <code>keybinds.rl.h</code>. This behavior is not exclusive to this header but also works on <code>bar.h</code>, <code>options.h</code>, <code>mouse.h</code>, and <code>status.h</code>.</p>
<p>In other changes, <code>dmenu</code> is no longer a dependency for speedwm, and is no longer used. Instead, speedwm now uses <code>spmenu</code>, my fork of dmenu which adds some useful things like color support, a proper keybind array, many more arguments and more. This fork was created to solve compatibility issues, as spmenu has a lot of arguments that dmenu simply does not have, and dmenu does not (by default) ignore arguments it does not recognize, something it should probably do because of all the patches available for it.</p>
                <p>As for bug fixes, there are a lot of them. First of all, status modules now support pango markup, which was disabled by default previously. The window swallowing functionality now updates the icon, not doing so is a dumb oversight on my end, but 1.9 fixes it. I’ve also fixed the custom layout crashing. Previously it would crash when you try to enter an S-expression due to the (bad) handling of history. Instead of using the old terrible approach we’re just running a separate shell script and parsing <code>stdout</code>. Much better, and because it doesn’t read a file using <code>fgets()</code> the possibility for crashing is much smaller. <strong>Finally, I have fixed multi-monitor crashing. It’s a shame it took this long to be noticed, but I will make sure it works properly for every release from now on. On 1.8, you <em>can</em> mitigate this by disabling tag previews.</strong></p>
                <p>I’ve also added a lot more documentation, although existing documentation has been shortened down. Documentation will now be found primarily on the wiki, which anyone has both read and write access to. Yes, I know this is risky but I’m using Git to back stuff up, so it should be fine even if someone is dumb enough to ruin it for everyone.</p>
                <p>The update also includes a few (not a lot, but a few) new features like:</p>
                <ul>
                    <li>Added support for extra status, which is by default separated by “;”</li>
                    <li>Added multi-monitor keybindings</li>
                    <li>Query support, allowing you to run <code>speedwm -q &lt;query&gt;</code> which returns information about speedwm.</li>
                    <li>Autostart doesn’t run on restart.</li>
                </ul>
                <p>Importantly though, speedwm-extras is no longer a dependency. You can still install it of course, and I still use it, but speedwm does not depend on it anymore. Either way, that’s it for this post, just a little update on speedwm since it has been over 2 months since the last release of speedwm was released. Expect 2.0 to take a long time, as I do not feel like pushing another update adding minimal changes. Thank you for reading, check out speedwm if you want an easy way into dwm and like efficient software, and have a good day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
