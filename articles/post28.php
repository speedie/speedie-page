<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>SHILL POSTS ARE BACK! fontctrl (Fonts on GNU/Linux-Improved)</title>
<meta charset="UTF-8">
<meta name="description" content="THE SHILL POSTS ARE BACK! Fonts on GNU/Linux absolutely suck so I wrote a script in suckless fashion to fix them.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>SHILL POSTS ARE BACK! fontctrl (Fonts on GNU/Linux-Improved)</h2>
            <h4>2022-09-16</h4>
            <p>THE SHILL POSTS ARE BACK! Fonts on GNU/Linux absolutely suck so I wrote a script in suckless fashion to fix them. So, what is wrong with GNU/Linux fonts and how does title related fix the problem?</p>
            <p>First problem is manually installing them. This in itself would not be a problem but the issue here is that there are about 3 million different paths where a font should go because of conflicting standards.</p>
            <p>fontctrl addresses this issue because it introduces two modes. Global and user. When you install a font globally using fontctrl, it will be located in /etc/fontctrl/fonts/. This of course requires root so the solution was to add a '--user' flag which will install it for the current user only. If this mode is used, the fonts can be found in ~/.config/fontctrl/fonts/.</p>
            <p>Secondly, if a font is on the system, it can be really really difficult to keep track of it. After all, you probably have many many fonts, so good luck finding and removing the fonts you don't use. fontctrl fonts are not truly installed unless they're enabled similar to something like eselect.</p>
            <p>After a fontctrl font is installed, it will be enabled. Enabled really just means symlinked from the */fontctrl/fonts/ directory to the actual font directory. This means the font is just stored locally in the fontctrl/fonts folder without being available to the system unless enabled. This allows fonts to easily be managed.</p>
            <p>What makes fontctrl special is that it leaves the manually installed fonts and fonts installed using a package manager separate and does not even touch them. fontctrl <i>only</i> manages its own fonts. This keeps your system clean, as fontctrl will not conflict with another font manager.</p>
            <p>Finally I should add that fontctrl is written in less than 200 lines of code, which makes it stable and less likely to fail. No features other than the base features are planned in order to keep the tool as stable as possible and it already does more than most people will need anyway from a minimal font manager.</p>
            <p>If you want to try fontctrl for yourself, you can find more information about it <a href="fontctrl.html">here.</a> Gentoo ebuild should be complete soon! Expect a mpm package in the near future as well. Thanks for reading, have a good day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
