<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Open source: Fake freedom. </title>
<meta charset="UTF-8">
<meta name="description" content="Stop using the term 'Open Source'. By supporting 'open source' you're not supporting software that respects the user's freedom. Instead you're supporting fake freedom. Open source is just as evil as proprietary software, the ONLY difference is that the source code for the application is public. You don't wanna support proprietary software with a nice mask on, do you?">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Open source: Fake freedom.<h2>
            <h4>2022-06-05</h4>
            <p>Yep, I'm back with yet another rant because we totally don't have enough of those yet. I hope the next few blog posts are going to be.. more positive but I feel like this must be said.</p>
            <p>This one is about free software and open source. This is probably my first blog post that has made be.. extremely angry while writing it. In fact I had to edit the finished post 5 times just because I kept insulting the people who support 'open source'. So please..</p>
            <p>Stop using the term 'Open Source'.</p>
            <p>By supporting 'open source' you're not supporting software that respects the user's freedom. Instead you're supporting 'fake' freedom. Open source is just as evil as proprietary software. The only difference is that the source code for the application is public. You don't wanna support proprietary software with a nice mask on, do you?</p>
            <p>If free software is what you <i>actually</i>support then please use the term 'Free software' or 'Libre software' instead. If you support 'open source' then you're supporting companies like Microsoft, Google, Apple and many more who don't care about your (free)dom but only the 'collaborate' aspect of open source.</p>
            <p>They like open source because they get YOUR (as in people who write code for the project) work and code for nothing in return. Open source is important but it's just part of freedom.</p>
            <p>In addition to this, many people who are unaware of the free software movement/project think these big tech companies are nice people who care about their users when this is far from the truth.</p>
            <p>Open source does not guarantee the user's freedom. This is why tech companies support 'Open source' but not free software. Free software is evil to them because they want control, something free software doesn't and cannot allow.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
