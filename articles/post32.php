<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Forwarder Factory is over. (Please read the blog post)</title>
<meta charset="UTF-8">
<meta name="description" content="So, very important blog post today. In short, Forwarder Factory is done, it's over. Please consider actually reading this blog post!">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Forwarder Factory is over. (Please read the blog post)</h2>
            <h4>2022-10-26</h4>
            <p>So let's stop wasting time, very important (but not good) blog post today. In short, Forwarder Factory is done, it's over. Now, so I can get this blog post out quickly, I am simply going to repost the huge and last Discord message I sent in the #announcements channel in its entirety. I know I said I wasn't going to make another blog post before the redesign but I <strong>need</strong> to get this out.</p>
            <p>Original post: "So.. I've known this day would come for a very long time and unfortunately, today is that day! In the end, Forwarder Factory is (finally) over. I want to take some time into writing an announcement for you so that you're not lost. If you even remotely support me, please read the entire thing!</p>
            <p>Before you start screaming "But what about preservation", note that the GitHub repositories will stay up, along with the Forwarder Factory organization. The YouTube channel will also stay up, however I will be removing the invite for the Discord server and replacing it with a link to a direct download. All the GitHub content is going to be mirrored to my Codeberg (speedie), and will get its own repository. I suggest you fork either the Codeberg or GitHub repositories to make sure they stay up if I decide to one day delete them, or if GitHub thinks I am breaking the terms of service (it is a Microsoft product we're talking about).</p>
            <p>Myself and my staff have been thinking about and debating this for a very long time, but I can no longer take the daily conflicts and other maintainence issues that come with owning a server/IRC channel like this. Yes, I could in theory transfer ownership but the FF name has already been through way too much so I don't feel like giving someone else responsibility for a mess I created is fair. <strong>So the goal here is to eventually archive this channel and Discord server.</strong></p>
            <p>Recently (about an hour ago as of posting this), the server suffered yet another conflict, specifically about moderation. While this is not the direct cause, I would consider it the final straw as I have been thinking about leaving the internet/ending the server and IRC channel for a very long time now, months actually.</p>
            <p>I want you to know that this is <strong>not</strong> the fault of any members here. And if it must be then it is every staff members' fault. I should take most of the blame but it is a waste to dedicate my last announcement here to that. To be honest though, I've been expecting this to happen for a very long time as I said earlier and the reason is fairly obvious looking back.</p>
            <p>This server has been through a lot of drama, has had staff joining and leaving, raids and more over the last 1.5 years or so that it has existed for. For example:</p>
            <ul>
            <li>The raid early on, back when I only had one staff member and got up at 3:00 PM.</li>
            <li>Domzuq mod abuse.</li>
            <li>Myself calling OBS Studio soyware.</li>
            <li>#based-general, the now long gone Discord channel which was supposed to be a place for special people to talk, but ended up a wasteland disaster.</li>
            </ul>
            <p>The last one, #based-general is in my eyes the root cause of this. What I now call "the #based-general mentality" which is a term I just invented while writing this basically spread to the rest of the server. The biggest issue however, is how I am not a good leader. It is this fact and lack of moderation by myself that caused this.</p>
            <p>First of all, I am very sorry for potentially taking away your (definitely not) favorite place to talk. However I no longer think I should be the one leading the Forwarder Factory project and as such, I would like you to go to a better community that is focused on what it should be; the Nintendo Wii console and modding it.</p>
            <ul>
            <li>If you are here for the Wii specific things such as my forwarders, consider joining the r/WiiHacks Discord server. They are essentially us done right, and stay on topic well.</li>
            </ul>
            <p>Again, <strong>all Wii repositories will stay up.</strong> Any Discord specific content is also going to be uploaded to these repositories to make sure nothing gets lost. That's what we tried to prevent in the beginning, right?</p>
            <p>Either way, with that being said, if you'd like to keep up with me and/or my projects, I have a website at speedie.site where I post the current information. In fact I may post this on my website too! As for the staff members here, simply ask them!</p>
            <p>I would like to give a big thank you to:</p>
            <ul>
            <li>Gabubu for being an awesome friend, moderator, forwarder creator and of course helping preserve rare forwarders.</li>
            <li>Damaj301damaj for being an awesome friend (and very funny guy), moderating and finding awesome rare WADs for you people of this server/channel.</li>
            <li>emilyd for being an awesome friend and teaching me about free software such as dwm, as well as writing contributions such as sblorgo, helping me with HTML and more.</li>
            <li>nezbednik for being an awesome friend, helping complete the Homebrew Channel collection and making the IRC channels we created possible.</li>
            <li>Lilium_Snow, who is no longer here for being an awesome friend for years, creating graphics and thumbnails for me, including all the Forwarder Factory icon/banner stuff, testing projects, and much more.</li>
            <li>Domzuq and DomzuqTR, who is no longer a part of FF for their contributions of rare Wii stuff.</li>
            </ul>
            <p>..and everyone who watched one of my videos, tried one of my projects, installed one of my forwarders or joined the Forwarder Factory Discord server/IRC channel. You guys rock and we will miss you a lot.</p>
            <p>I would also like to apologize to Domzuq for joking around with his IP and I would also like to apologize to bloodythorn and the r/WiiHacks community for letting them down (the server invite is/was in #affiliates).</p>
            <p>I personally have very mixed feelings about this, however I feel like it is the right thing to do. Remember; all good things eventually end. The end of Forwarder Factory is today! If you have any questions about what is going to happen, you can DM me on Discord or /query me on IRC. I am going to be somewhat active on Discord until the end from now on to make sure everyone gets a chance to say something to me!</p>
            <p>Thank you for everything, everyone!"</p>
            <p>Yes, thank you everyone for the support, I appreciate it.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
