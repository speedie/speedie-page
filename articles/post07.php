<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>rchat 0.4 is here (Now available on Arch and Gentoo) </title>
<meta charset="UTF-8">
<meta name="description" content="rchat 0.4 is here. It doesn't bring much of note except a few commands but you can now install it easily if you're on Gentoo or Arch through the AUR or my ebuild overlay respectively.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>rchat 0.4 is here (Now available on Arch and Gentoo)</h2>
            <h4>2022-04-24</h4>
            <p>rchat 0.4 has finally been released. This release doesn't bring much of note except a few commands.</p>
            <p>However, you can now install rchat easily on Arch based distributions through the AUR and Gentoo using my ebuild repository.</p>
            <p>To install rchat 0.4 on Gentoo, add my overlay, you can find out how to do so by following this link: https://codeberg.org/speedie/speedie-overlay.</p>
            <p>To install rchat 0.4 on Arch, simply install the AUR package. You can use an AUR helper like 'yay' if you want. Usage with 'yay' is 'yay -S rchat'.</p>
            <p>For other GNU/Linux distributions, you can head over to my GitHub and download the latest version. Or clone it using Git.</p>
            <p>The latest release can be found here. Simply download, save to /usr/bin/rchat and chmod +x /usr/bin/rchat.</p>
            <p>https://raw.githubusercontent.com/speediegq/rchat/main/rchat</p> 
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
