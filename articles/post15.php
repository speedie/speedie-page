<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Are GNU/Linux users elitist or are normies too stupid to learn? </title>
<meta charset="UTF-8">
<meta name="description" content="Windows/macOS users often call me an elitist but is that really true? In my eyes, not really. Normies however are elitist about being too stupid to learn. They take pride in being stupid and refuse to learn even when it takes no time whatsoever.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
        <h2>Are GNU/Linux users elitist or are normies too stupid to learn?<h2>
        <h4>2022-06-06</h4>
            <p>Another day another blog post!</p>
            <p>Windows/macOS users often call me an elitist GNU/Linux user but is that really deserved? Well, I personally don't think or care too much about people's opinions on me. After all, I have better things to do and since it's so common to call GNU/Linux users 'elitist', it doesn't really mean anything to me.</p>
            <p>However, since this is so common I wanted to find this out for myself. So let's think for a moment here? Why do I write blog posts just like the one you're reading?</p>
            <p>Well, for me it's because it allows me to say exactly what I think whenever I want. It's a nice way to (sometimes) express your anger at something. However I don't really view myself as an elitist. After all, I write blog posts like this one and write guides like <a href="https://speedie.site/guide02.html">this one</a>. I went out of my way to help <i>new</i> GNU/Linux users in this case install Arch.</p>
            <p>If I was an elitist I'd tell you to 'RTFM' like every Arch user ever. Instead I went out of my way to help you get started with Arch.</p>
            <p>What I'm not about to do however is read the 'FM' for you. If you aren't capable of reading plain English then I will call you exactly what I view you as, <i>an idiot</i> or <i>a normie</i>.</p>
            <p>In addition to this, it ALWAYS seems like the people calling <i>me</i> an elitist are part of this group. People who will complain about something being difficult without having the patience to read and perform. They will call anyone who uses software that they view difficult to use (for example Gentoo, Arch, or suckless software) an elitist. They can't seem to find one reason why anyone would actually want to use said software except to look cool on the internet.</p>
            <p>Now I'm not gonna deny the fact that there do exist <i>some</i> elitist GNU/Linux users but they're the exception rather than the rule. Most people who use this (in the normie's eye difficult) software do it because it's more efficient once you know how it works. A good example of this would be Vim.</p>
            <p>The 'real' elitists (in my opinion) are the normies since they consider anyone who uses X software an elitist even if only a few (but loud) people have an elitist attitude about the software they use.</p>
            <p>The normies are proud to be normies. Just like we are proud to use this 'hard to use' software.</p>
            <p>The difference is, we try to teach the normies this 'hard to use' software but instead of actually learning anything, they simply call us elitists. Therefore, I personally consider the normies the actual elitists.</p>
            <p>To be clear, if you don't understand how to use X software then that's perfectly fine. What I do have a problem with is proud stupidity. Normies are always proud to be normies instead of learning. They take pride in being stupid.</p>
            <p>That's all for this blog post. Have a good day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
