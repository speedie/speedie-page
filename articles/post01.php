<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Stop making Linux user friendly.. sort of</title>
<meta charset="UTF-8">
<meta name="description" content="A problem that's getting bigger with each day is that we're making technology more and more convenient and overall easier to use however in the process soydevs make soyware which not only harms the normie its intended to help but also power users. Stop making Linux user friendly.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Stop making GNU/Linux user friendly.. ..sort of</h2>
            <h4>2022-03-09</h4>
                <p>A problem that&#39;s getting bigger with each day is that we&#39;re making technology easier but it&#39;s also getting more and more &quot;evil&quot; with big tech companies collecting and selling our data for monetary gain. </p>
                <p>So then we turn to alternatives (Usually free software and GNU/Linux) which is a good thing, more people caring about their online privacy. The problem is how we approach these new users. We want it to be easy for them but in the process of making it easy for new users we&#39;re also making it a nightmare for experienced users and slowly making GNU/Linux just as bloated as those proprietary operating systems. </p>
                <p>What do I mean with &quot;bloat&quot; since that term is kind of a meme by now?</p>
                <p>Nowadays most of the software we use is bloated. The websites you visit likely have a lot of JavaScript in them (Facebook, Google, Discord, Instagram, Twitter, etc.) which forces us to have bloated web browsers for viewing those pages slowing down older computers. Additionally this also requires a GUI program which some users might not want.</p>
                <p>But even if you avoid the web there&#39;s still bloat in a lot of software. A lot of software on GNU/Linux now is packaged using Universal packaging like Snap packages (Canonical), Flatpaks (Red Hat), and AppImages. </p>
                <p>Now since soydevs decide to package using these and since these are universal, every single package has to have support for every distro which obviously is very bloated and probably moreso than .dmgs containing a binary on macOS or a .exe since there are more GNU/Linux distros than macOS versions/Windows versions.</p>
                <p>Thing is, when you&#39;re trying to keep your system minimal for multiple reasons (Could be old hardware or you simply like minimal software) these packages are just not an option. You might say, &quot;Just compile from source&quot; (as if I&#39;m not already using Portage)</p>
                <p>And yes, I can definitely see your point but the problem comes when software is <i>absolutely proprietary</i>, because you can&#39;t exactly compile software without the source code in your hands.</p>
                <p>Now proprietary software is NOT minimal, obviously however whenever you need to install a piece of proprietary software you may in some cases be forced to use one of these bloated packages since the proprietary software developer is too lazy to actually use the distro specific package managers or at least provide a tarball for the user and leave it up to the distro maintainers.</p>
                <p>So what are these universal package formats good for? Well, they&#39;re good for Windows/macOS normies who are used to things <i>just-working</i> without any tinkering. But problem comes when everyone starts adopting this new fancy packaging format. Because once these become standard (I definitely predict they will), you will be forced to use them (unless the software is free and open source).</p>
                <p>TLDR; By using bloated universal packages and making it easy for the normies, you&#39;re making it harder for minimal GNU/Linux users/experienced users who REALLY hate universal packaging.</p>
                <p>So please, if you&#39;re going to distribute software, please provide packaging for the distro&#39;s native package manager.</p>
                <p>Have a good day</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
