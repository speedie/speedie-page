<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<meta charset="UTF-8">
<title>Happy 20th Birthday Arch Linux </title>
<meta name="description" content="Today, 2022-03-11 marks 20 years since Arch Linux was released. I would like to show some respect by creating this post. Arch Linux is the distribution that got me into minimal Linux distributions and even though it has its issues (drop systemd), it's really great.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Happy 20th Birthday Arch Linux!</h2>
            <h4>2022-03-11</h4>
            <p>So today, 2022-03-11 depending on your time zone marks 20 years since Arch Linux was released to the public. I would like to show some respect by
            creating this very post. Arch Linux is still one of the best Linux distributions and I use it on a few machines.</p>
            <p>Even though I personally use Gentoo, Arch Linux was what got me into minimal Linux distributions and bloat-free software so I believe
            we should give it the respect it deserves.</p>
            <p>So thank you Arch Linux for being "different" and being minimal. Although I don't agree with some of your decisions such as switching from OpenRC to Systemd, you're one of the most popular distros and managed to get many people away from the bloated
            Linux distributions to something minimal and DIY. You even basically created Linux elitism by making an OS
            that's difficult to install.</p>
            <p>Happy 20th Birthday Arch Linux and thank you for reading this.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
