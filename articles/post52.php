<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Friendship ended with Gentoo, now Arch is my best friend!</title>
<meta charset="UTF-8">
<meta name="description" content="Alright so I have a brief announcement or something today and that is, I have officially stopped using Gentoo. Yes that's right, the Gentoo elitist is now an Arch cuck. But why, why would you commit such a crime you might say? Well, Gentoo has actually been giving me more and more problems for months now, and it doesn't seem to get any better.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Friendship ended with Gentoo, now Arch is my best friend!</h2>
                <h4>2023-03-26</h4>
                    <p>Alright so I have a brief announcement or something today and that is, I have officially stopped using Gentoo. Yes that's right, the Gentoo elitist is now an Arch cuck. But why, why would you commit such a crime you might say? Well, Gentoo has actually been giving me more and more problems for months now, and it doesn't seem to get any better.</p>
                    <p>I've had so many dumb issues with Gentoo recently, such as Xft fonts being broken, packages failing to emerge, <code>--depclean</code> removing my entire system, and the final straw, gnome-keyring issues that just do not occur on other GNU/Linux distributions. In case you're not aware, I have been using Arch on my laptop for months now, and while Arch has some annoying issues such as GPG keys constantly breaking pacman when updating, I find that it works much better now.</p>
                    <p>To make matters worse for Gentoo, syncing the repositories takes a very long time, and it's valuable time that I do not want to spend just because a program is slow and written in Python. Moving over to Arch was not difficult though. I said 'fuck it' yesterday at around 04:00 in the morning, and started installing Arch over Gentoo. Thankfully, as you guys know I have an arch repository containing nearly the same programs as my gentoo repository (overlay), and as such I was able to install my config files and all my programs using one command. It's super nice, otherwise I would've probably spent much more time on this.</p>
                    <p>Anyways, as for my overlay, I will probably update it every once in a while using maybe a docker container, but I'm going to be focusing on the arch repository because it's what I'm using. For those of you that actually use Arch, this might be good news for you because it means you will always be able to install my software using pacman. I know that some of you will probably be disappointed about this, because I'm kind of known as a Gentoo user at this point, but I just can't take Portage's stupidity anymore. If you need to however, feel free to remove my feed!</p>
                    <p>I also took the time to move /home to a separate partition, which is really nice if you want to reinstall quickly. Whatever, that's all I needed to say with this blog post. Have a good day!</p>
</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>

