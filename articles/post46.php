<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>libvirt is the worst program ever made..</title>
<meta charset="UTF-8">
<meta name="description" content="I really, really REALLY hate libvirt. It may be the worst program I've ever used, in fact it even outclasses a lot of nonfree software, it is so bad it doesn't want to work and it doesn't do anything I want it to do.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>libvirt is the worst program ever made..</h2>
                <h4>2023-02-06</h4>
                        <h3>Another rant.</h3>
                            <p><strong>Warning: This blog post, unlike most of my others features strong language, because I hate libvirt. Please leave if you're sensitive to this.</strong></p>
                            <p>I really, really REALLY hate libvirt. In fact it may be the worst program I have ever used, in fact it even manages to somehow outclass a lot of nonfree software, it is that bad. It's so bad I'm going to say fuck for the first time on this blog. And yes, I will take advantage of that. But first, what is libvirt and why am I complaining about it?</p>
                            <p>First, for this to make sense I'm going to talk a bit about my computing setup. I've got a fairly normal computer with an Intel Core i9 (backdoors included), and importantly two graphics cards (AMD RX 570+580), allowing me to pass one through to the virtual machine. As my host operating system, You guys know I use Gentoo, the best GNU/Linux distribution with a minimal kernel configuration and on top of that is speedwm with all the toggles set to zero. This Gentoo host is also LUKS encrypted. This is because the host is only used for running my virtual machines, which act as my actual "host". You can compare this to something like Qubes OS, where each operating system is its own container, only more based and more difficult to set up.</p>
                            <p>I do this because it allows me to easily monitor and backup each "machine". It's also pretty nice for security since I can just destroy the VM whenever I want, and I can choose to only pass through my mouse and keyboard and nothing else. Also, if I find a new cool GNU/Linux distribution I want to try, just clone the existing VM, rename it and install and when I'm done I can destroy that VM.</p>
                            <p>Now, to do this most people including myself use QEMU with KVM. QEMU is pretty nice and doesn't suck as much as libvirt does (we'll get into this shortly). QEMU doesn't do virtualization by itself, it's actually an emulator but combined with KVM (built into the Linux kernel) you've basically got a virtual machine. Only problem is that QEMU with KVM is very complicated and requires a fuck ton of different arguments to even work. So people write shell scripts to do this, which is fine, I'm all for scripting your way to the result you want but most people don't want to bother with this. So then, someone had enough with QEMU being difficult to use and writes a program called libvirt and it is the ungodly garbage responsible for all pain and suffering in the world. Seriously. libvirt basically has its own configuration file, which it then translates into QEMU argument on the fly and runs using your default shell. Fairly simple.. or that's what it should be if libvirt wasn't a garbage program written by absolute retards who have no business doing anything computer related.</p>
                        <h3>Why does it suck so much?</h3>
                            <p>Now that you hopefully understand what it is, why do I hate it so much? First of all, it creates more problems than it solves. What's the solution to having a complicated way to configure a program? Just add an even more complicated way to configure it, that will be perfect. QEMU is configured by using arguments like other programs, this gets complicated with a lot of arguments but it can be managed with a shell script. libvirt has an even longer configuration file, which is configured in HTML-like syntax that has been adapted to libvirt, so not only does it fail to solve a problem, it just forces the user to learn a new config and shove more shitty software up the already complicated and fucked up software stack on GNU/Linux.</p>
                        <h3>How can libvirt suck more?</h3>
                            <p>To make matters even worse, once the libvirt service has been started, it straight up refuses to fucking stop again. I run <code>rc-service libvirtd stop</code> and what does it do? Oh yeah that's right absolutely fucking nothing. Except it refuses to give me my USB and PCIe devices that was given to the guest so that means no ethernet anymore. That means I'm going to have to reboot my computer entirely and decrypt my hard drive before I can even try again. Fucking thief.</p>
                            <p>As if that wasn't enough, it doesn't even tell me when there's a configuration error. It just tries to start, silently fucks itself, steals my PCIe and USB devices, refuses to stop, forces me to reboot until I manage to find and fix the problem. You might be screaming right now, saying READ THE LOG FILE READ THE LOG FILE, but the log file contains nothing but shit, nothing useful, just obvious things like 'qemu started' or something along those lines.</p>
                            <p>That's where it ends right? Right? Well, no. Recently it has started failing to start for absolutely no reason other than just not wanting to start. It ends with me just rebooting over and over hoping it will FINALLY pass my mouse through and let me start using the virtual machine. Fuck libvirt.</p>
                        <h3>End.</h3>
                            <p>Well, that's the end of this one. I will probably go more into detail on how this setup works and why it is still superior to using an operating system on bare metal like most people do at some point. Have a good day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
