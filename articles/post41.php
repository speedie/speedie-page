<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Why YouTube©️™️ doesn't recommend your videos.</title>
<meta charset="UTF-8">
<meta name="description" content="Unless you're some big channel that uploads every single day or something, you've no doubt experienced the absolute pain that is YouTube and their algorithm refusing to promote your videos for one reason; your upload 'schedule'.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Why YouTube©️™️ doesn't recommend your videos.</h2>
                <h4>2023-01-15</h4>
                    <p>Now, unless you're some big channel that uploads every day or something like that, you've no doubt experienced the absolute pain that is YouTube refusing to promote your videos.</p>
                    <p>Even if you're someone like me who doesn't really care that much about getting views or something and just casually uses the platform once a month or something to post stuff occasionally, you've no doubt noticed that if you upload videos in a short timeframe, no matter what it's about, it's almost guaranteed to get more views than anything you just post and forget about.</p>
                    <p>This is just a theory but I suspect Google and YouTube wants you to constantly push out new videos every day, even if it's total crap not worth 30 seconds of your time. As you know, I tend to only upload videos once a month or less, and that's for multiple reasons.</p>
                    <p>Firstly, I don't have time to push out content daily about the new cool dwm patch or whatever. Not only is it not very useful for the viewer and basically acts as filler, but it's a waste of time for me as well. Secondly, YOU don't have time to watch this filler garbage content that only exists to make YouTube push your content to people's feed.</p>
                    <p>I think a good example of this is if you look at my Forwarder Factory channel, which is now completely dead because I have zero plans to upload anythng to it, but anyways all of the old videos were successful and got plenty of views, despite the content of the videos being mostly crap.</p>
                    <p>I then of course stopped uploading to it for reasons I don't need to specify and 4 months later I broke the silence a nd posted something to it. Got 487 views which is absolutely nothing in comparison to what the earlier videos got. YouTube likely refused to promote this video because I hadn't been posting consistently up until that point.</p>
                    <p>Of course, this isn't always the case and we don't exactly know how the YouTube algorithm works because of course it is not free as in freedom software. Some topics combined with good timing are basically view magnets. My Windows 11 sounds video got over 100,000 views just because at that time Windows 11 had just leaked and almost no one had covered the topic yet.</p>
                    <p>Either way this YouTube thing is really frustrating and it's one of many reasons why I think being a 'YouTube Certified™️ Content Creator™️©️' is a waste of time for people like me. Even if you get less traffic to your website or whatever, I highly recommend that you stick to free/libre ways of spreading your word whenever possible such as using the LBRY network or even better, just having your own blog and website that you have control over, or at the very least, you don't trust YouTube.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
