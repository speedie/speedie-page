<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Dear bloggers: Your RSS feeds suck.</title>
<meta charset="UTF-8">
<meta name="description" content="Alright, so today I want to talk about one really annoying thing people still do in the year 2023 with their blogs. What people will often do is write blog posts, and then they will have an RSS feed. Great! Except the blog post only contains a breif description, and to read the full blog post you need to visit the website using your bloated web browser.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Dear bloggers: Your RSS feeds suck.</h2>
                <h4>2023-04-19</h4>
                    <p>Now that most of my issues regarding this site are resolved, I want to start writing about something. I have a lot of topics I want to talk about, however for many of these topics there's just not much content to them, so I apologize for the length of some of these.</p>
                    <p>Anyways what better topic to start with than this one. My blog isn't perfect, I post a lot of garbage here quite often, but what pisses me off is when people will write blog posts, have an RSS feed and then ruin it with one thing. They will put about 1/10 of the blog post in the <code>description</code> tag, and then they will have the blog post in full on their website.</p>

                    <p>This is extremely annoying, because it means I have to open up my bloated web browser just to view your blog post which could normally be read using my RSS reader, which is designed for reading blog posts. What if I want to read your blog post on the command line? Or what if I want to read your blog post when I don't have internet?</p>
                    <p>Good RSS readers like Newsboat and sfeed store the full feed locally, meaning you can actually read the articles even when you don't have any internet connection. But when you force me to go to your website, I can't just save it when I do have internet and read the blog post whenever I want to read it.</p>
                    <p>Now, I know why you would do this. If you have a site, chances are you want people to visit it. RSS is convenient, very convenient and I'm going to admit I don't actually visit the sites for blogs I follow very often, usually I read the feeds every day and then very occasionally visit the websites. But I still think this is annoying.</p>
                    <p>So, if you're going to have a blog and you plan on using RSS, please provide the full blog post in the description tag. I know this can cause issues with paragraphs, but you can steal my feed as a base if you want. Thanks for reading, and have a good day.</p>
</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>

