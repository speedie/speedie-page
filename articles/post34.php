<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>We're back up better than ever! (feat. OpenBaSeD)</title>
<meta charset="UTF-8">
<meta name="description" content="We're back up better than ever! Right when the canonicucks thought I was gone for good, I rise from my grave and bring you a new website and blog post. Okay, okay on a serious note I have some very important news to share with the people who follow my blog.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>We're back up better than ever! (feat. OpenBaSeD)</h2>
                <h4>2022-11-25</h4>
                    <h3>We're back!</h3>
                        <p>We're back up better than ever! Right when the canonicucks thought I was gone for good, I rise from my grave and bring you a new website and blog post. Okay, okay on a serious note I have some very important news to share with the people who (still) follow my blog.</p>
                    <h3>Solving the existing problem with speedie.site</h3>
                        <p>Maintaining the speedie.site website has been a struggle and it only gets worse with every blog post and page I create.</p>
                        <p>I thought about my alternatives for solving this or at least improving on it, things like using Markdown, writing a simple shell script to append text or even stupid static site generators, but in the end I decided to go with PHP. And it makes sense, PHP is a solid option and has been around for years.
                        <p>It has a major flaw in what I am trying to accomplish with it and that is server-side code that the user can't see. Of course the source code for the PHP is public and available for everyone.</p>
                        <p>I have and will continue to publish all changes made to the PHP to <a href="https://codeberg.org/speedie/speediegq">the Codeberg repository</a> but you have no way to verify that the code is identical to what is running on the server. I am not sure if there is any way to prove this to the users of my website but if there is, let me know and I'll gladly provide.</p>
                        <p>This is much more convenient than manually editing <strong>every single HTML document</strong> on my website any time I want to change a single character.</p>
                    <h3>Hosting</h3>
                        <p>GitHub Pages which I used to use up until yesterday has one flaw which would stop this plan right away; it does not support running PHP in any way. I presume this is for ""Security"" reasons, like what if some script kiddy runs <em>dangerous</em> code on our servers? Either way, I had to stop using Pages for this to work out.</p>
                        <p>My friend offered to host my website on her VPS and for a while I accepted but because I felt bad for wasting her bandwidth and space and because it's not as safe to rely on others, I decided to get my own VPS instead.. It's really cheap and while I *do* plan on self-hosting eventually (need to build a computer to host on), I felt this was a good solution.</p>
                        <p>I installed OpenBSD on it because it is a great system for building secure servers but importantly it uses LibreSSL which was something I really wanted for this website.</p>
                    <h3>Issues</h3>
                        <p>OpenBSD works a little differently than the GNU/Linux I am used to so this took a bit of work to figure out. Most of my issues were PHP related because it's much harder to set up on BSD.</p>
                        <p>While the website still has a few flaws that I plan on fixing very soon (https://speedie.site/projects doesn't lead to project-list.php and instead results in an 'Access Denied.' from Apache), the website seems to be work fine.</p>
                    <h3>Website rewrite</h3>
                        <p>So, let's talk about the website itself.</p>
                        <p>First of all, just take a look around. The website has been rewritten from scratch, this time using PHP for the header and footer. This allows changes to be easily made to the header for every single document (there were a lot of them) saving time.</p>
                        <p>As for other changes, blog posts and text guides are now in the articles/ directory. This was done to keep the root less cluttered which is important when you are going to be hacking on the website for a while. Images are still in img/, CSS is still in css/ and the header/footer is in the php/ directory and projects have been moved into the projects/ directory. I may do more with PHP in the future but right now it is only being used to include the header and footer.</p>
                        <p>I also decided to archive blog post 1 through 10 due to them being either irrelevant, misleading or just bad to read. You <em>can</em> still read them but they have a little warning attached.</p>
                    <h3>RSS that sucks less</h3>
                        <p>I finally spent the 10 minutes necessary to make my RSS feed valid. Readers like Newsboat now display the right time and date which was not the case previously. It's a small thing, but equally small is the time it takes to fix the problem I had created.</p>
                    <h3>Wen replace domain name?</h3>
                        <p>Not sure yet, if Unfreenom decides to take away my .gq domain name I am likely going to buy a proper domain that doesn't do stupid stuff like this. I won't do it otherwise due to the pain of switching domains (and changing GitHub name once again).</p>
                    <h3>Questions?</h3>
                        <p>If you have any questions about what has been going on, please consider <a href="mailto:speedie@speedie.site">emailing me</a>. I don't bite but respect should be earned.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
