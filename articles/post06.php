<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>It's time to stop using Adblock Plus (seriously stop)  </title>
<meta charset="UTF-8">
<meta name="description" content="It's time to stop using Adblock Plus. It's 2022 and people are still using Adblock Plus despite their shady choices and misleading claims.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>It&#39;s time to stop using Adblock Plus (seriously stop)</h2>
            <h4>2022-03-12</h4>
            <p>One of the most popular types of browser extensions have been ad blockers. Now there are so many ad blockers, most of them being absolutely terrible that it can be hard to pick one.
            But fear not, just look for the highest rated one right? Well.. No. Because on most browsers Adblock Plus is the highest rated extension and therefore it&#39;s also the most popular.</p>
            <p>But little do most users know that ABP isn&#39;t what it seems. You see, big tech companies pay the creator(s) of ABP to not block their ads.
            You might say, these are what ABP calls &quot;Acceptable Ads&quot; but NO, these are not just whitelisted until you manually disable them, these are ALWAYS whitelisted.</p>
            <p>Now, let&#39;s talk about what &quot;Acceptable Ads&quot; are. These are ads that ABP deems &quot;Acceptable&quot; and they likely get paid
            to make these &quot;Acceptable&quot;.
            The reason this feature exists is obvious. Most normies don&#39;t spent time &quot;tinkering&quot; with their software and therefore probably use the default settings.
            And &quot;Acceptable Ads&quot; are also enabled by default. So the user installs ABP thinking they&#39;re getting privacy and an ad free experience when that&#39;s not really the case.</p>
            <p>Guess what makes it worse? ABP doesn&#39;t tell its users that they&#39;re being paid money by big tech companies. And because ABP is one of the most popular if not THE most popular
            ad blocker its users are going to trust it no matter what.</p>
            <p>So what ad blocker should you use? Well, you could go with the least bloated option which is to edit your /etc/hosts file but manually adding entries is extremely time consuming
            and ultimately not worth your time so instead you can simply use the &quot;uBlock Origin&quot; extension which is available for Chromium, WebKit and Firefox based browsers.</p>
            <p>uBlock Origin despite having a bad name in my opinion is definitely the best ad blocker. It&#39;s 100% open source and free as in freedom. Its filter is also public and if you really
            want you can add it to your /etc/hosts manually. It has no &quot;Acceptable Ads&quot; trash which means money doesn&#39;t decide what&#39;s going to be blocked.</p>
            <p>It also has a bunch of other nice features that other ad blockers lack such as preventing WebRTC leaks and disabling JavaScript, cookies, trackers and other stuff that you
            might not want. It&#39;s definitely one of the best browser extensions and I highly recommend it.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
