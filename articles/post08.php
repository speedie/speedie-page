<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Half an rchat (rchat 0.5 is out) </title>
<meta charset="UTF-8">
<meta name="description" content="rchat 0.5 is here. It brings a few more commands such as setting how quickly messages are loaded. rchat now has its own page on my website, you can check it out yourself. There are also patches available for rchat now which is the main method you'll be able to add patches.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Half an rchat (rchat 0.5 is out)</h2>
            <h4>2022-04-26</h4>
            <p>rchat 0.5 is here. It brings a few more commands such as setting how quickly messages are loaded.</p>
            <p>It is likely one of the last feature updates, as new features will come in the form of patches.</p>
            <p>rchat now has it's own page on my website, you can check it out here: https://speedie.site/rchat.html</p>
            <p>In addition to this, patches are now available. At the time of making this post, there's a .Xresources patch available for 0.5</p>
            <p>Please be respectful and don't abuse the new feature.</p>
            <p>- To install rchat 0.5 on Gentoo, add my overlay, you can find out how to do so by following this link: https://github.com/spoverlay/splay.</p>
            <p>- To install rchat 0.5 on Arch, simply install the AUR package. You can use an AUR helper like 'yay' if you want. Usage with 'yay' is 'yay -S rchat'.</p>
            <p>- For other GNU/Linux distributions, you can head over to my GitHub and download the latest version. Or clone it using Git.</p>
            <p>The latest release can be found <a href="https://codeberg.org/speedie/rchat">here</a>. Simply download, save to /usr/bin/rchat and chmod +x /usr/bin/rchat.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
