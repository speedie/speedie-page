<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>I HATE APPLE!!! </title>
<meta charset="UTF-8">
<meta name="description" content="I believe Apple is an evil business, moreso than many other tech companies for one reason. No it's not their high prices. Actually it's worse than that. We can laugh all we want about the $1000 Apple Studio Display stand but really, that's far from the worst. The real reason I hate Apple is because they manipulate the consumer.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>I HATE APPLE!!!<h2>
            <h4>2022-05-23</h4>
            <p>Today I'm starting a new 'series' of blog posts essentially about how certain companies are evil. This one will be about Apple as I'm pretty sure you can tell if you even read the title of the post.</p>
            <p>Before you call me a biased RMS fan who runs a FSF approved distribution, never goes outside, has Stallman posters and hates everything that isn't free software:</p>
            <p>I am far from biased, in fact I used to run macOS as my main desktop operating system, used an iPad years ago and since 2020, have been maintaining a Mac OS X 10.4 retro project.</p>
            <p>Even then, I believe Apple is an evil business, moreso than many other tech companies for one reason. No it's not their high prices. Actually it's worse than that. We can laugh all we want about the $1000 Studio Display stand but really, that's far from their worst. The real reason I hate Apple is because they manipulate the consumer.</p>
            <p>If your first reaction to this is "HOW" then Apple managed to do it on you too.</p>
            <p>So why is Apple evil? Apple is evil because they manipulate the consumer into thinking their products provide privacy for the normie (Which most Apple users are).</p>
            <p>Need a good example? Simply watch their newest (at the time of writing) video called Privacy on iPhone. In the video they in sort of a weird way claim Apple and iPhone devices specifically allow you to disable app tracking. So why is this evil? Isn't this great for privacy?</p>
            <p>If the normie consoomer who doesn't know any better tries to disable app tracking using whatever features Apple the evil company provides, Apple gets all that data for themselves. Now of course they claim they don't sell your data but how are you gonna know? Almost every piece of software written by Apple is ABSOLUTELY PROPRIETARY which means you have absolutely no idea what it does with all your data.</p>
            <p>And while they have access to your data, the normie consoomer thinks Apple cares about your privacy and only wants the best for you. This means people who are now starting to care about privacy go out and buy iPhone devices thinking they're good for privacy when they're really not.</p>
            <p>Sure, iPhones may be better than Samsung, Huawei, Xiaomi and the endless other companies when it comes to privacy but to think they don't track you is really stupid. A general rule is, if something's not "free and open source software" it tracks you. Apple stops other companies from tracking you but they track you themselves instead. At the end of the day, the data is still collected and sent to Apple so it really makes no difference in the end.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
