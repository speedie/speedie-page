<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>..and a git repository (CGIT REVIEW!!!)</title>
<meta charset="UTF-8">
<meta name="description" content="I don't think it needs to be said but I've been doing some changes to this website recently. If you saw yesterday's blog post, I talked about some changes I had made, such as introducing a speedwm and general wiki to the site.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>..and a git repository (CGIT REVIEW!!!)</h2>
                <h4>2023-01-28</h4>
                    <p>I don't think it needs to be said but I've been doing some changes to this website recently. If you saw yesterday's blog post, I talked about some changes I had made, such as introducing a speedwm and general wiki to the site. I made some changes to the wiki shortly after publishing that blog post, and today I made even more changes so I think it justifies another blog post. Either way, this blog post is practically just an extension to the last one.</p>
                    <p>Now, why did I make these changes, and why the title which (seemingly) has nothing to do with the point of this blog post anyway? Well, you see I decided to try to set up a Git server by myself for a few reasons and that's exactly what I really want to talk about, I just think I should put the wiki changes in this post as well. First of all, I should mention that the speedwm wiki has moved to <a href="https://speedwm.speedie.site">speedwm.speedie.site</a> and the more.. general wiki has moved to <a href="https://wiki.speedie.site">wiki.speedie.site</a>. I'm doing this because it's good practice, really. Besides, I want to be able to use projects/speedwm again if necessary. I have updated the blog post to reflect these changes of course, but just keep that in mind.</p>
                    <p>Alright, let's talk about <code>cgit</code>, which is likely why you clicked on this blog post in the first place. cgit as the name implies is a git frontend which allows you to go through commits and other nice things using your browser.. alright alright I think we all know what it is now but cgit is written in C, no not C++, Rust or any other garbage, just plain perfect C. It's also very minimal but still offers basic functionality and even some nice things like RSS feeds for commits, which I talked about in my blog post about <a href="https://codeberg.org">Codeberg</a>, which is really nice for people like myself who primarily use the internet through these feeds anyway.</p>
                    <p>I looked at other solutions as well, such as stagit but they all had annoying flaws which cgit didn't seem to have so I decided to attempt to host an instance of cgit. I would say it went relatively well, and it was definitely a learning experience. I don't think I've learned this much about Git in 30 minutes ever before, so I'm definitely glad I did it for that reason as well.</p>
                    <p>By now, you may be wondering if I'm going to leave Codeberg. I have no plans to leave Codeberg, but some of my projects are going to be moved to <a href="https://git.speedie.site">git.speedie.site</a> because internet independence is always a good thing, and I like the control this gives me. As of writing this blog post, speedwm, speedwm-extras, speedwm-wiki, libspeedwm, spmenu, st and speediegq all have repositories on git.speedie.site, some more are going to be moved and the Codeberg repositories for these are going to stay as backups that will be updated around each version bump/release. Some repositories will stay on Codeberg as it's just unnecessary to move them to git.speedie.site for various reasons such as lack of quality.</p>
                    <p>Either way, I think that's all I have to say, next blog post is probably going to be about my computer setup in general (it's VERY different from the norm, beyond just the operating system and window manager). So yeah, check out <a href="https://git.speedie.site">git.speedie.site</a>, and the wiki if you haven't already, and have a good day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
