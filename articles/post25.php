<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>I left GitHub and you should too!</title>
<meta charset="UTF-8">
<meta name="description" content="As we all know, especially the developers who read my blog, GitHub is by far the most popular Git service out there. It is also 100% free as in beer and offers a lot of features, even free hosting. However this comes at a price - Privacy. GitHub is not as nice as it used to be.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>I left GitHub and you should too!<h2>
            <h4>2022-08-23</h4>
            <p>As we all (hopefully) know, especially the developers who read my blog, GitHub is by far the most popular Git service out there. It is also 100% free as in beer and offers a lot of features, even free hosting (although it does not allow the usage of PHP).</p>
            <p>However, this comes at a price - Privacy. GitHub is not as nice as it used to be. GitHub used to <i>only</i> be a Git service but once it became popular, big tech does what big tech does best and decided to put their disgusting, money hungry hands on yet another product and this time it was GitHub because in 2019, Microsoft bought GitHub meaning everyone was right in expecting a huge downfall.</p>
            <p>GitHub was definitely not perfect before it was purchased by Microsoft however if you are familiar with Microsoft and their background, you know their intentions were to dominate the programming and developer space. And why wouldn't they? They have created the C# programming language (and others), have a very popular code editor called Visual Studio and more recently, Visual Studio Code which is built using Electron. Guess who designed Electron? GitHub.</p>
            <p>In addition, GitHub has their own IDE/text editor called Atom which just like Visual Studio Code, is also an Electron application. These two text editors, Atom and Visual Studio Code would kind of compete, and by buying GitHub, Microsoft would have complete control over both of these projects. After users of Atom got worried after Microsoft purchased GitHub, they said that they would keep both editors alive.</p>
            <p>They did keep their promise for a few years but a few months ago, Atom was put to rest in a blog post named 'Sunsetting Atom.' which now makes Visual Studio Code even more popular. Microsoft does have the right to do this, and there will likely be forks of Atom, but it is clear what they want to do. They want the Atom users to move to Visual Studio Code.</p>
            <p>So why did I mention this? Well, it's because you can no longer trust GitHub. Because it's now a Microsoft product, they want full control as they always do. Users have been worried for years and so have I but because I have so many projects, moving has been quite difficult and time consuming. And this is someone who is not a developer. Now imagine an actual developer behind many popular projects. It is a hassle and Microsoft knows this. Therefore most users are going to keep using GitHub as the time is just not worth it to them. And after going through this, I can kind of see why.</p>
            <p>So, let's say you've had enough of Microsoft and their stupid proprietary services. Let's say you want to completely de-Microsoft your life. Where and how are you going to host your code?</p>
            <p>Even though it may seem like everyone uses GitHub, it is not the only option out there. Another popular option is GitLab but just like GitHub, it is also spyware and offers even LESS features. It may not be owned by Microsoft but it is just as evil. If you are making the move, avoid both GitHub as well as GitLab.</p>
            <p>If possible, avoid any services that are not usable without JavaScript. If you do this, it is possible to use the Git service with LibreJS which means you don't have to run any proprietary code just to view and use the website. For this reason, I chose Codeberg.</p>
            <p>All of its JavaScript is free software and although LibreJS does block a few scripts, they are free software and can be verified as such. In addition, if you still want to block those scripts, the website will remain functional as those are only used for the text editor.</p>
            <p>Codeberg also does not force you to use some obscure authentication making Git (the command line utility) difficult to configure and use. Codeberg "just werks" with minimal configuration. I prefer to use an SSH key to authenticate but there are also other methods available. Codeberg has a very similar layout to GitHub so there is basically no learning curve. It also offers some really useful features that GitHub does not such as being able to download patches/diffs for commits. This is really useful for me personally when comparing older commits to new ones of my software or reverting changes.</p>
            <p>Moving repositories over can take some time and it doesn't allow you to automatically transfer over repositories like GitLab does however once you go through with it, it's really quick. You just create the repository, clone the old repo, clone the new repo, copy everything, commit and push. It can all be done with the command line Git so if you want, you can maybe even write a script to do it automatically.</p>
            <p>Either way, I cannot recommend Codeberg enough and I am going to move all my repositories over. Many of my repositories have already moved and you can now find all my suckless builds, rchat and more there. I hope to be able to fully move soon, and although it's a lot of effort, it is going to be worth it.</p>
            <p>That's it for this blog post, have a great day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
