<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>speedie.site now has a wiki!</title>
<meta charset="UTF-8">
<meta name="description" content="This is just a brief blog post regarding some new somewhat important news for this website! speedie.site has received a wiki, which anyone may edit. No, this is not some bloated wiki solution, it's done using purely PHP and CSS, and articles are written in Markdown!">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>speedie.site now has a wiki!</h2>
                <h4>2023-01-26</h4>
                    <p>This is just a brief blog post regarding some new somewhat important news for this website! speedie.site has received a wiki, which anyone may edit. No, this is not some bloated wiki solution, it's done using purely PHP and CSS, and articles are written in Markdown! This is great, because speedwm documentation is already written in Markdown and has been for a while. Not only that, but speedwm has it's own separate wiki, because a lot of information is speedwm related.</p>
                    <p>Now, I want to get speedwm 1.9 out soon, especially since it has been two full months since the last release and over 100 commits since then, but I want to move some documentation over to the speedwm specific wiki first, as I think the man page is growing very, <strong>very</strong> big. That is not great, not to mention any documentation in a tarball cannot be updated as the release has already happened. Not ideal, is it?</p>
                    <p>Also, a speedwm wiki was pretty much necessary at this point. While you <em>can</em> pull request to the speediegq repository on Codeberg, there's a lot of delay and work involved in that, when all you want to do is say, fix a simple typo or something like that. However not only can you edit existing articles written by me, but you can also create new articles if you feel like something is missing. While this is speedwm documentation, a lot of it also applies to dwm, so if you're into hacking on dwm you might want to check out the wiki!</p>
                    <p>As for the more general wiki, it's far, FAR from complete so far. In fact there is only one article. But I plan on slowly expanding it as time goes on. With that said, you may be wondering what wiki solution I went with. Did I write my own? What wiki solutions are there that aren't absolutely massive and terrible?</p>
                    <p>Of course, <a href="https://github.com/panicsteve/w2wiki">w2wiki</a>. w2wiki is written purely in PHP and CSS, and articles are written in Markdown as stated previously. I have to say I really like w2wiki and I have no plans to use something different. I will say, I don't really like the default CSS, however we're not soydevs here, so I changed that to fit the theme and minimalism of the regular speedie.site website.</p>
                    <p>Anyways, that's just a small post for today, I have two more suckless related posts coming up relatively soon, but after that I hope to write some more classic blog posts. I have been relatively busy with real life of course, so I haven't had time to write anything proper, although I have a relatively good amount of ideas.</p>
                    <p>That's it for today, thank you for reading, check out the <a href="https://wiki.speedie.site">general wiki</a>, as well as the <a href="https://speedwm.speedie.site">speedwm wiki</a> and have a nice day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
