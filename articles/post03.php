<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Dear soydevs; Stop making desktop applications bloated.</title>
<meta charset="UTF-8">
<meta name="description" content="Modern software is suffering from a huge issue. One that must be stopped before every application is like this. Bloat. Specifically web bloat which is currently spreading to desktop software. This means not only is the modern web unusable but so will the desktop. Soydevs must be stopped.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Dear soydevs; Stop making desktop applications bloated.</h3>
            <h4>2022-03-10</h4>
                <p>Today I wanted to talk about something else. I guess it&#39;s kinda related to my previous post titled &quot;Stop making Linux user friendly.. sort of&quot;.
                Modern software has this problem, a very huge problem. &quot;Bloat&quot;. Now bloat can be caused by many different aspects. Programming language, poorly written code, unnecessary features, etc. but the main one I wanna talk about is called &quot;Electron&quot;.</p>
                <p>Now if you&#39;re not familiar with Electron, it&#39;s basically a minimal version of Chromium designed to create individual desktop applications from HTML, CSS and JavaScript. This makes it easy to create desktop applications as long as you know a lot about web development.</p>
                <p>In fact many desktop applications you might be using (Almost all of them are proprietary) use Electron.
                Examples of these are: GitHub Desktop, balenaEtcher, Discord, Spotify, and yes, even a text editor, Visual Studio Code or as I like to call it, &quot;SoyDev Code&quot;.
                
                If you&#39;ve viewed my page, <a href="hate.html">hate</a>, you&#39;ll know that both Electron and the JavaScript programming language is there on my list.</p>
                <p>Either way, JavaScript is a VERY bloated language and is often obfuscated making it harder to see spyware, even if it&#39;s TECHNICALLY open source.
                Now JavaScript is becoming more popular in web development and this fact is pissing people who want a minimal system off.
                So to make it worse, since Electron allows you to turn HTML+CSS+JS into a desktop application, this means JS will be used for software as well.</p>
                <p>You might say, &quot;I have a fast computer so I don&#39;t care if the software is bloated or not&quot; and while that&#39;s a fair point, what if someone doesn&#39;t have a fast computer or just simply likes minimal software. Then they&#39;re essentially forced to use this bloated slow application just because a developer was too lazy to actually learn another language.</p>
                <p>TLDR; The web is extremely bloated and since Electron turns &quot;the web&quot; into a desktop application, software on the desktop is getting bloated too.</p>
                <p>So what do I want? I want developers to STOP using this Electron trash.</p>
                <p>Thank you and have a good day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
