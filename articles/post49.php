<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>spDE - Now also on Arch based distros</title>
<meta charset="UTF-8">
<meta name="description" content="Yesterday I decided to get some work done and ported most of my Gentoo packages for my software, such as speedwm and spmenu but much more to Arch. Of course, first I had to learn how to actually do this, but as it turns out, it’s actually even easier than on Gentoo. I was able to create all of these packages, including testing in less than an hour. Because of that I decided to port spDE to Arch.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>spDE - Now also on Arch based distros</h2>
                <h4>2023-02-27</h4>
                    <p>Yesterday I decided to get some work done and ported most of my Gentoo packages for my software, such as speedwm and spmenu but much more to Arch. Of course, first I had to learn how to actually do this, but as it turns out, it’s actually even easier than on Gentoo. I was able to create all of these packages, including testing in less than an hour.</p>
                    <p>I had to make a few minor changes to spDE for it to work on Arch, such as changing the owner of the home folder to the user rather than the user’s group but for the most part it works perfectly. No, the package will not be on the AUR as I do not want the user to have to deal with compiling the stuff locally. I also don’t want to deal with the AUR moderators potentially deleting packages.</p>
                    <p>With that said, if you want to install spDE on your Arch box, add my <a href="https://git.speedie.site/speedie-aur">arch repository</a> (instructions in README) and <code>pacman -Syyu</code>. Then simply <code>pacman -S spde</code> provided the sync completed successfully! The rest of the steps are the same as on Gentoo, run <code>spde -i</code>, <code>spde -a &lt;your user&gt;</code> and finally <code>startx /usr/bin/spde -r</code> to start spDE.</p>
                    <p>spDE on Arch is <em>nearly</em> identical to spDE on Gentoo. There are a few minor differences but they shouldn’t be that noticeable. If you don’t want the entire spDE package, you can choose to install the stuff individually, too. There are packages like <code>speedwm</code>, <code>speedwm-spde</code>, <code>spmenu</code>, <code>libspeedwm</code>, <code>speedwm-extras</code>, <code>fontctrl</code> and so on which can be installed without installing the entire thing.</p>
                    <p>That was just an update for those of you that are interested in running spDE on Arch, I have been putting this off for a long time so I’m glad I finally did it. Thank you for reading, install spDE if you want, and have a good day!</p>

</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>

