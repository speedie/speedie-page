<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>I WILL make music great again. (My next project)</title>
<meta charset="UTF-8">
<meta name="description" content="Time for something a little different than usual. Today I'm going to talk about my next project that I am working on as we speak. It usually starts with 'boo this program sucks, let's write something that does what I need it to do better than the current options and this time, it's no exception.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>I WILL make music great again. (My next project)</h2>
                <h4>2022-12-20</h4>
                    <h3>Finally something worth your time?</h3>
                        <p>Time for something a little different than usual. Today I'm going to talk about my next project that I am working on as we speak. It usually starts with 'boo this program sucks, let's write something that does what I need it to do better than the current options and this time, it's no exception.</p>
                        <p>As you know, I've been working rather heavily on my dwm build; speedwm. I've been trying to <em>iron</em> out bugs with it and generally make it the window manager (or computer setup in general) of my dreams. After I reached 800 commits just a few days on the new speedwm repository alone, not to mention the many more on the old repositories, which adds up to over a thousand commits, I realized that I am trying to improve something that doesn't really desperately need improvement.</p>
                        <p>Now that I'm fairly happy with it, I am going to start a new project. I might work slowly on speedwm, and hope to have a 1.9 release out soon but I don't plan on making any more massive changes to it. So as usual when it's new project time, what happens is I think of a piece of software that sucks and then I try to make something that fixes the flaws. What is it this time?</p>
                    <h3>Make music great again</h3>
                        <p>Yes, you heard me right. I am going to write a music player. Or rather a mpd frontend, because it would be stupid to reimplement something that already works well, right?</p>
                        <p>Now, I am listening to music through cmus as I am writing this blog post for you, and I have to say it's generally a great music player. It's light, fast, Vim-like and fairly polished and clean. Unlike the mocp I was using earlier, it has a UI more like.. you know, a music player than a file manager which is one of the reasons why I much, <em>much</em> prefer cmus. So why does this need change then?</p>
                    <h3>cmus is not built for window managers</h3>
                        <p>Yeah, I said it. One of the problems with cmus is how difficult it is to manage it through the command-line. There is cmus-remote, however even with this you often have to use lots of slow awk commands to get the format you want.</p>
                        <p>I plan on solving this by adding window manager/status friendly syntax which allows the user to specify a format, no slow awks or seds in your status script.</p>
                    <h3>Lack of extensibility</h3>
                        <p>None of the music players/mpd frontends I've looked at are extensible enough for my taste. I want to be able to make it extremely minimal or command-line Spotify without any delay or external programs.</p>
                        <p>It doesn't help that cmus and a lot of other music players don't seem to be maintained very much. Most of them seem to be written for TTY users, and don't have a lot of fancy features. This is not great when you want a light (but feature rich) music experience.</p>
                        <p>I plan to solve this by keeping the base small and clean, as well as a very well documented codebase unlike suckless software. While I for the most part like their philosophy, I am not a fan of how they refuse to add any meaningful comments to their code.</p>
                    <h3>Cool planned features</h3>
                        <p>None of these have been implemented yet but I might end up implementing them. Some as modules, some built into it.</p>
                            <ul>
                                <li>Vim-like default keybinds but of course ones that can be changed. Extensibility is one of my goals for this project and is one problem I have with cmus and the countless other players.</li>
                                <li>FLAC support. This is an absolute must and I will not release it before FLACs play perfectly. I am a sucker for lossless music and those mp3s can go back to history.</li>
                                <li>Cover art previews. This is not an absolute must, but it would be really nice to have sixel/ueberzug/w3m previews for this player.</li>
                                <li>Different approach. Instead of using patches like I do for most of my software, I plan on providing a simple base with a plugin system. If I end up writing it in POSIX shell, I could simply <code>.</code> plugins in the config file. This would be easy to configure and very customizable.</li>
                                <li>macOS and BSD compatibility. I want support for many operating systems, and a mpd frontend has no reason not to work on macOS and BSD. That said, I may need some help implementing macOS stuff, please contact me if you're willing to help and test it for me.</li>
                                <li>Usage of ncurses?</li>
                                <li>Maybe this could've been mentioned earlier, but I want wide image support. I won't implement stuff like Kitty image previews, but feel free to PR for it :)</li>
                                <li>External piping. This could for example allow anything to be piped into programs like fzf, dmenu or rofi.</li>
                                <li>Playlists. Simple as, I suppose.</li>
                                <li>Auto downloading metadata using something like Musicbrainz.</li>
                                <li>Few dependencies. This is not a requirement because the target userbase is window manager users like myself, not TTY minimalist LFS users, but it would be nice to save Gentoo users like myself some compile times.</li>
                                <li>Perhaps ability to download a music video or something similar. This is really just for show and probably wouldn't be very useful in practice so I don't think this will happen.</li>
                            </ul>
                    <h3>Ideas</h3>
                        <p>If you have any ideas for this project, please consider emailing me it. I want to make this project as good as it can be.</p>
                    <h3>Naming?</h3>
                        <p>I have no idea what to name this mpd frontend, if you have any suggestions feel free to send me a suggestion.</p>
                    <h3>End.</h3>
                        <p>Going to end the blog post here as I've already rambled on and on about how music players suck and what I want it to do. Thank you for reading and have a great rest of your day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
