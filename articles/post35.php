<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>speedwm 1.5: It's still speedwm.</title>
<meta charset="UTF-8">
<meta name="description" content="Eh, this happens weekly might as well get it over with.. Decided to release speedwm 1.5. Just like the previous version, it's mostly bug fixes.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>speedwm 1.5: It's still speedwm.</h2>
                <h4>2022-11-28</h4>
                    <h3>Not this again..</h3>
                        <img src="../projects/preview.png" alt="preview image" width="75%">
                        <p>I know what you might be thinking but eh, this happens weekly, might as well get it over with.</p>
                        <p>Decided to release speedwm 1.5 today and in my opinion, it makes all previous versions irrelevant, not because this one is so great in terms of features but because it's so much faster.</p>
                    <h3>Master, what changed then?</h3>
                        <p>Tired me at 10 PM joke out of the way, what changed, what made 1.5 worth the release?</p>
                        <p>In my spare time I had waiting for the bus, among other things I decided to rewrite all of my status modules. I also wanted to see how many <code>if</code> statements I could get rid of. Answer? All of them. None of my modules have a single if statement in them now. I also cut the line count for most of them down by at least half, which is a good thing for readability, because fewer lines means fewer possible mistakes and fewer possible bugs to destroy the software.</p>
                        <p>I would also look like a hypocrite if I was promoting well written software while having terrible code myself. Pretty much all commits after the 1.4 release have been bug fixes. ..Except a few.</p>
                        <p>One important thing to note is that speedwm_run is no longer used, and speedwmrc is not used either. If you want a speedwm config file, copy <code>docs/example.Xresources</code> to wherever you want it and add <code>xrdb /path/to/that/file</code> to ~/.config/speedwm/autostart.sh. To start speedwm, you simply run <code>startx /usr/bin/speedwm</code> or add speedwm to your .xinitrc. Those of you using a display manager already know what to do.</p>
                    <h3>Get this slow trash off my system.</h3>
                        <p>From that h3 I'm sure you know where this is going. speedwm 1.5 removes the old stellar xsetroot (well, now speedwm -s <text>) status script and replaces it with a partially integrated fork of dwmblocks. dwmblocks is a lot faster than the status script I was using previously. Because it still uses status modules though, you can still configure it in shell so no extra skill is necessary.</p>
                        <p>The old script also didn't allow updating each module individually meaning you're going to constantly waste your resources on reloading status modules that don't need to be reloaded all that often. The new status (written in C) solves this because it allows you to specify how often each module gets updated, and it can also be updated manually by sending a signal.</p>
                    <h3>That's all, glowies.</h3>
                        <p>That's all I have to provide for today. I am tired and don't feel like writing a proper blog post, but I will hopefully have something worth reading out tomorrow.</p>
                        <p>Have a good day, or night.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
