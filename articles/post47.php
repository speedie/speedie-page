<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Ethical software is not ethical and should be abolished.</title>
<meta charset="UTF-8">
<meta name="description" content="So I think most of you that follow my blog are free software and privacy supporters. Even if you support the 'open source' movement more than the based free software movement, I think we can both agree on the fact that ethical software is not ethical and might actually be less ethical than nonfree software. I want to talk about this because while it is nothing new, I got reminded of it and thought it was an excellent topic for today.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Ethical software is not ethical and should be abolished.</h2>
                <h4>2023-02-07</h4>
                    <p>Good afternoon good sirs! So I think most of you that follow my blog are free software and privacy supporters. Even if you support the 'open source' movement more than the highly based free software movement, I think we can both agree on the fact that ethical software is not ethical and might actually be less ethical than nonfree software. I want to talk about this because while it is nothing new, I got reminded of it and thought it was an excellent topic for today.</p>
                    <p>If you're out of the loop, or maybe haven't fallen for the free software ideology as much as the rest of us, what is "ethical" software and why should it be abolished? Ethical software is a software movement similar to the free software and open source movements, which strive for freedom in technology. However, the difference is ethical software ultimately is not about user freedom but rather a hate license that allows users of the many licenses to allow and disallow usage of the software and source code based on the political views of the individual using the software. Don't worry, it doesn't end there because the licenses aren't just about politics but also other world "issues" (depending on your views).</p>
                    <p>Let's take the NoHarm license for example. It is recommended by the <a href="https://ethicalsource.dev/">ethical source</a> website and is one of the many licenses they promote. Most of the license looks similar to the GPL, that is until you get to section 5 where we can see some radical differences. This is also where most of the stupid terms can be read. But first, let's read the preamble where it says "As software developers, we engineer the infrastucture of the 21st century" and "We envisage a world free from injustice, inequality and the reckless destruciton of lives and our planet". Now we can see their priorities, it's no longer about user freedom but instead about social justice and promoting political ideas. Free software is for everyone, absolutely everyone, even the people who don't support it. The 'Do No Harm' license however is for everyone, but only if you believe in the same ideas as the author of the software.</p>
                    <p>Let's read through the first section, 'abuses of human rights'.</p>
                    <ul>
                        <li>Human trafficking</li>
                        <li>Sex trafficking</li>
                        <li>Slavery or indentured servitude</li>
                        <li>Discrimination based on age, gender, gender identity, race, sexuality, religion, nationality</li>
                        <li>Hate speech</li>
                    </ul>
                    <p>Most of this seems somewhat fair, as it's already illegal in most countries (hence abuses of human rights). But this is where the first problem comes in, why does a free software license need to mention any of this stuff when it's illegal anyway? And if it's illegal, chances are you don't care about following a software license anyway. Even if it's not illegal in your country, most of this is not even related to software or freedom. A license should not punish you for commiting any of these acts, that's why we have law enforcement. You might say, I don't do any of this so I don't care but that's just stupid. Aren't we fighting for user freedom? Why settle down just because <em>you're</em> following the license?</p>
                    <p>Next we have 'environmental destruction'.</p>
                    <ul>
                        <li>The extraction or sale of fossil fuels</li>
                        <li>The destruction of habitats for threatened or endangered species, including through deforestation or burning of forests</li>
                        <li>The abuse, inhumane killing or neglect of animals under human control</li>
                        <li>Industrial processes that generate waste products that threaten life</li>
                    </ul>
                    <p>Here we can see the license devolve further into the insanity that is politics. Even though the average person is not going to commit most or even any of the acts mentioned here anyway, we're fighting for user freedom, aren't we? 'The extraction or sale of fossil fuels' is stupid, because it does not even do any harm to <strong>living</strong> creatures on Earth, but rather benefits humanity in one way or another. I feel like this has slowly devolved into an anti-capitalism kind of license, which is very different from what it says in the preamble. Then we have 'Industrial processes that generate waste products that threaten life'. Nuclear generates (potentially) deadly waste products, yet you depend on it.</p>
                    <p>Alright, next is 'conflict and war' which is more stupidity for various reasons I'll go into in a moment.</p>
                    <ul>
                        <li>Warfare</li>
                        <li>War crimes</li>
                        <li>Weapons manufacturing</li>
                        <li>Violence (except when required to protect public safety)</li>
                    </ul>
                    <p>Okay, why is this stupid? First of all, if you're alright with commiting WAR CRIMES, do you really care about following a software license? No, and for the average person, good luck even starting a war. If you somehow manage to start a war, and want to use the software, and still care about following the license, it's still really stupid, a software license should not to any normal human being decide whether or not you can use it for war. I think we all can agree on this, no matter your opinion on war.</p>
                    <p>'Weapons manufacturing' is stupid too. There are valid, legal uses of weapons, such as hunting. This means, if I want to hunt I cannot use any ethical software. Really, really stupid. And violence is up to law, so it is pointless to put in a software license. Despite all of this, so far it's been at least somewhat reasonable. All that changes now, as it mentions 'addictive or destructive products and services'.</p>
                    <ul>
                        <li>Gambling</li>
                        <li>Tobacco</li>
                        <li>Products that encourage adversely addictive behaviours</li>
                    </ul>
                    <p>Now, what's wrong here? A lot, there are so many flaws here and I'll get into some of them. First off, it disallows gambling, which might be the first actual software related clause in this entire license. Yes, that's right. We're this far in and finally a real software license clause can be found. Anyways, this is stupid because there are a lot of ways to get around this, gambling is actually kind of subjective. I do think this is a bit too restrictive for a free software/open source license, and it would be considered nonfree due to section 5.</p>
                    <p>As for Tobacco, tobacco is definitely addictive, but it's up to each individual and no one is forcing you to use it so I think this point is unnecessary. This is also where the cracks in the license start to show, because how do you even integrate software or even electricity into tobacco? Anyways, for addictive behaviours, there are even more flaws with the license, because there are a lot of things that can be addictive. Does this mean you can't use the program with video games? What about using it at all, because the software <em>might</em> be addictive. Yeah, I don't know what they were thinking when they added this to the license.</p>
                    <p>The next section links to some other licenses, such as the 'Universal Declaration of Human Rights' and 'Convention on the Rights of the Child', none of which belongs in a free software license. I won't go into too much detail about this but you can read these licenses yourself if you want. The rest of the license continues like a standard copyleft license, which means all the cuck licensing tricks (such as relicensing) are not possible to pull off.</p>
                    <p>Now, finally, what do I think about 'Ethical' software? I think ethical software is unethical, more so than nonfree software. Despite some of the acts here being morally questionable, free software builds on fundemental ideas such as making sure every user of the software is guaranteed the four freedoms. This means, even if you're a war criminal who violates human rights, or something like that, you can still use and benefit from free software. You know, this is what I hate with people who constantly use social media, they are exactly the kind of people who would use and write a license like this. They care more about politics and social justice than writing a quality piece of software, or even making sure users are free to do whatever they want with the software. SJWs don't make the world a better place, they make it a significantly worse place, where freedom is ignored in favor of political opinions.</p>
                    <p>That's it for today, let me know what you think about ethical software, and have a good day!</p>
</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>

