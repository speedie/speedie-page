<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Please give me suggestions...</title>
<meta charset="UTF-8">
<meta name="description" content="This might actually be my last blog post.. sort of. Now that I have your valuable attention, please read the rest of the blog post! So, a few days ago I started working on a website redesign. However I quickly realized that it was more work than it was worth.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Please give me suggestions...</h2>
            <h4>2022-10-10</h4>
            <p>This might actually be my last blog post.. sort of. Now that I have your valuable attention, please read the rest of the blog post!</p>
            <p>So, a few days ago I started working on a website redesign. However I quickly realized that it was more work than it was worth. So instead, I decided that it would be easier to straight up start from scratch. No, not completely from scratch, that would be ridiculously time consuming.</p>
            <p>However I decided that it would be easier to create the HTML manually, from scratch and then copy over the text (such as blog posts) from the older page.</p>
            <p>Anyways, let's get to the point of this blog post which is, I need suggestions regarding the redesign. Anything. What should I implement, what kind of design should I go for? This time, I am able to use PHP for the header, which I will likely end up doing however as always, speedie.site will stay as an anti-JavaScript website and as such, I am not going to add a single line of it even if it makes my website look nicer.</p>
            <p>Here are some ideas I've got myself:</p>
            <ul>
		            <li>About me page</li>
		            <p>About me page, which will contain information about me, and the software I use.</p>
		            <li>Separate 'Projects' category (for example, https://speedie.site/projects/speedwm)</li>
		            <p>Right now, project pages are all over the place and difficult to find. Therefore I think it would be much easier to have a projects category.</p>
		            <li>Separate 'Blog' category (for example, https://speedie.site/blog/post01.html)</li>
		            <p>There are now many blog posts, and the one you're reading only makes the problem worse. Because of this, I need a way to make them easier to sort and keep track of.</p>
		            <li>Header and Footer done using PHP</li>
		            <p>In my opinion, this is the most important part. Otherwise, If I want to change one thing in the header, I have to manually edit <strong>every single HTML file in the website.</strong> Doing so can take a very very long time.</p>
		            <li>Atom feed</li>
		            <p>I might start offering Atom feeds alongside RSS feeds if the benefit makes it worth it.</p>
		            <li>Separate feeds for Projects, Wii and philosophy/politics/opinions on the state of the world/technology</li>
		            <p>This blog currently has way too many blog posts that don't really matter a few months after the event. Removing all the 'rchat <version>' posts from the main blog and uploading them to a separate 'Projects' feed is going to make it much cleaner and easier for the user.</p>
            </ul>
            <p>As for web design, I don't really know what I want to do yet. Will likely go for a minimal design though, as minimalism tends to hide the flaws of not using JavaScript fairly well!</p>
            <p>If you want to give me suggestions, you can join my IRC channel (Libera.chat network, #ff or #ststid'kwtn), or you can <a href="mailto:speedie@speedie.site">>send me an email</a>. That's it for this blog post, your suggestions are appreciated, thank you for reading and have a good day people!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
