<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Let's talk about Project 081 0.6</title>
<meta charset="UTF-8">
<meta name="description" content="So.. because I keep getting questions about Project 081, even though I don't really work on it much anymore, I thought I'd make this blog post as an update on it as well as why it's taking so damn long.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Let's talk about Project 081 0.6</h2>
                <h4>2023-01-03</h4>
                        <p>So.. because I keep getting questions about Project 081, even though I don't really work on it much anymore, I thought I'd make this blog post as an update on it as well as why it's taking so damn long. Also if this blog post comes across as negative, I'm sorry, it's 5 AM and I'm tired. Suck it up.</p>
                        <p>I will be honest here, I hate working on Project 081, and this is not because I think the project is bad, or even because its ABSOLUTELY PROPRIETARY (hopefully one day we get the source code for Tiger). It's because it's a massive pain to test it or even to create a basic image. As I mentioned in the issue on the GitHub repository, testing the project has been a challenge for multiple reasons. Here are just a few:</p>
                        <ul>
                            <li>Different hardware.</li>
                                <p>In order to even test this project, you <strong>need</strong> a real Mac compatible with it. Now, we know what machines are compatible to some extent and we know what hardware those machines have but attempting to emulate that hardware is a massive pain and basically impossible.</p>
                                <p>That leaves owning the actual hardware, of course. Now, while I'm sure others are willing to test for me, even having to fix a syntax error in a Bash postinstall script or something would need the image to be updated. This means the user testing needs to download it, write it to a USB, test it, tell me what happened, and very likely repeat it over and over again. This sucks!</p>
                            <li>Creating the image.</li>
                                <p>Confession time here, the image used to be created on Windows which had no way to automate it whatsoever. This meant making all of it from scratch which would take ages.</p>
                                <p>Even on GNU/Linux, mounting macOS disk images is a bit of a pain, but there it can at least be automated using a shell script. This would also be convenient because.. it's my operating system of choice.</p>
                                <p>Making the image on macOS would be ideal, because we can still write shell scripts there and the operating system supports mounting it natively. However this means macOS is <strong>required</strong> to even create a basic image, which sucks because I no longer actually use it as my operating system of choice. The best option, but still sucks.</p>
                            <li>Works on X, broken on Y.</li>
                                <p>This is the number one issue with Project 081 right now. If you've seen the <a href="https://github.com/p081/Project081">GitHub repository</a> for Project 081 and actually read the README or Issues, you'll without a doubt know that Project 081 doesn't even work on the MacBookPro4,1. This has been a known issue for a long time and a great example of this problem.</p>
                                <p>Now, Project 081 0.6 will partially fix this by only installing what your system needs based on the output of sysctl hw.model but still, this sucks too.</p>
                        </ul>
                        <p>That's not to say Project 081 0.6 is cancelled. I did say work on Project 081 ended on my page a while ago but that notice was removed after I realized that fixing this problem was fairly trivial. People have been asking me about the state of Project 081 though so clearly there is some demand for a new version.</p>
                        <p>For now, there's an open <a href="https://github.com/p081/Project081/issues/6">progress tracker for Project 081 0.6</a> which spawned out of <a href="https://github.com/p081/Project081/issues/4">this issue regarding blue screen.</a> The goal is to eventually fix this issue and get a proper version out but before that can happen, I need to get a script or something working so that this massive waste of time can be automated. That would only leave testing, which takes a bit part of the pain away.</p>
                        <p>Well then, to end this blog post off, I would like to mention how I'm very surprised that Project 081 is still (somewhat) in demand. Especially since it has not been actively worked on for over 2 years now. Yeah, insane I know. Time flies when you're.. ..not having fun.</p>
                        <p>If you have any more issues on any hardware, consider making an issue on the p081/Project081 repository as I do not want to have to make yet another release after this if I can avoid it. Thank you for reading this and have a good day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
