<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>How I got into GNU/Linux</title>
<meta charset="UTF-8">
<meta name="description" content="I am back. I finally got released from prison after Google put me there and forced me to write a blog post. Today I want to talk about getting into GNU/Linux and free software, how I got into it and how you can get into it yourself. But because my started using GNU/Linux story is so short, I'm gonna also talk about how I got into certain software like dwm and Gentoo, since normies tend to call this stuff difficult.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>How I got into GNU/Linux</h2>
                <h4>2022-12-13</h4>
                    <h3>I AM FINALLY FREE! Time to unfree myself.</h3>
                        <p>I am back. I finally got released from prison after Google put me there and <a href="post36.php">forced me to write a blog post.</a>.</p>
                        <p>Today I want to talk about getting into GNU/Linux and free software, how I got into it and how you can get into it yourself. But because my "started using GNU/Linux" story is so short, I'm gonna also talk about how I got into certain software like dwm and Gentoo, since normies tend to call this stuff difficult.</p>
                    <h3>My first encounter with GNU/Linux</h3>
                        <p>My first encounter with the GNU/Linux operating system was in 2011 when I found out about this new "fancy" operating system called Ubuntu GNU/Linux. People like myself reading this blog post right now might say "DOnT yOU hATe UbUnTU?!?!?" but back then, it was very different from what it is now.</p>
                        <p>Back then, it was a fairly nice system. I didn't end up sticking with it as you'll read later, because it wasn't able to run most of my software. For this reason, I went back to Windows 7 and forgot about Ubuntu and the wonders of GNU/Linux and free software.</p>
                        <p>Either way I kept using Ubuntu for a few months (not sure why I didn't stick with it after that) and used the absolute garbage that is Wine to attempt to emulate the Windows experience I was familiar with instead of attempting to learn new software. Probably the most common mistake any new user makes.</p>
                    <h3>Most easy distros are terrible</h3>
                        <p>In 2017 or so I went back to trying out various GNU/Linux distributions like Elementary OS and Manjaro, all of which are absolute garbage distros that barely work. While it may be different now, I can't help but stop and ask; Why are all "easy" distros so bad?</p>
                        <p>The answer is going to be different depending on who you ask but since you're reading my blog, I'll answer it. It's because they try to make the wonders of GNU/Linux something that it isn't. They try to turn it into a free version of Windows, rather than making 'the ultimate OS'. </p>
                        <p>This stupid direction made all easy distros end up using this stupid software that takes the bad from Windows and nothing good and what you get is an absolute mess which is going to break within a month.</p>
                        <p>Long story short, lasted a few weeks and then went back to Windows.</p>
                    <h3>Freedom is hard to resist</h3>
                        <p>However, freedom was hard to resist so I kept trying out various distributions in my free time but never ended up finding anything I actually liked. Eventually I got sick and tired of Windows doing stupid Windows-y things and installed Ubuntu.</p>
                        <p>After all, it had been years since I last used it (2011) but maybe software support has improved, I thought. Yes, software support had improved but everything else SUCKS. It was slow, had terrible default applications, grub broke all the time, kernel was missing, X doesn't even start properly, and many many more issues.</p>
                        <p>I then later reinstalled Ubuntu in the hopes that I had messed something up. User error is normal, I thought. After that, I didn't really encounter any more issues and I was able to do most things just fine on my computer. I still don't know why it just NOW works properly.</p>
                        <p>After a while I started learning to write basic shell scripts, because that's what you do when you have free time and want to learn how to use your computer properly. It's a great way to learn your system.</p>
                        <p>While watching some bad YouTube guides on the basics of shell scripting, I saw this one guy with a really cool desktop. "WHAT.. WhAT?!?!? IT TILES???? WAOW". Through a simple search and some reading on window managers, I installed i3-gaps on my then Ubuntu system and switched to using it. I really started to like customizing my system, and I loved efficient, quick keybinds for everything.</p>
                    <h3>Distro that sucks less</h3>
                        <p>Ubuntu was of course holding me back from the customization I wanted to do, and had thousands of packages even after removing all the GNOME garbage. I had already been experimenting with Arch for a while on my old iMac.</p>
                        <p>Out of boredom, one day I decided to install Gentoo in a virtual machine. It was more as a challenge more than anything, because as I understood it, this distribution was difficult to use and install. (Now that's false, but that's another issue)</p>
                        <p>Eventually, when Ubuntu yet again broke, I decided to install Gentoo on my system. It took a while due to the weak processor I used to have.</p>
                    <h3>suckless software</h3>
                        <p>Around this time, I was told about suckless software; specifically dwm. It basically looked like i3 but more convenient to use. It also seemed like a great window manager because it was configured through source code meaning it's very extensible and hackable.</p>
                        <p>So for my new Gentoo system, I installed dwm instead of i3 and started configuring it. You can actually still use my dwm build from then, it can be found <a href="https://github.com/speediegq/dwm">here</a>. This made my computer feel so much better and so much more usable and extensible. I cannot recommend learning software like this enough.</p>
                    <h3>Then to now.</h3>
                        <p>Not much has changed. Sure, I've switched to/from software but I'm still learning about the system and software I use. There really is no end to learning about technology and software. It keeps going as long as you're interested in learning further.</p>
                    <h3>How can I get into it too?</h3>
                        <p>My number one tip for people trying to get into GNU/Linux is this; don't give up too early. Expect to face challenges but be smart and attempt to work around them.</p>
                        <p>And as I mentioned above, instead of trying to run your spyware software, try to run free software compatible with GNU/Linux rather than the Windows-only software you're already running.</p>
                        <p>Finally, don't expect to learn everything instantly. You learn as you go, and you learn from your mistakes.</p>
                    <h3>The most generic conclusion in history</h3>
                        <p>I am now a Gentoo GNU/Linux user. Overall, it's been a great experience and I love the freedom and customization I can do to my desktop. I'm able to make it anything I can imagine. It truly is great and it's the peak of computing; well, other than writing your own operating system.</p>
                        <p>Thank you for reading the blog post, have a good day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
