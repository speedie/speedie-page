<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>I use Chromium based browsers again.. (How to lose your followers in less than 5 minutes)</title>
<meta charset="UTF-8">
<meta name="description" content="If you've been following my blog for a while, you might remember an old blog post of mine titled 'Everything I want to use is Chromium'. Like the title says, In the blog post I complain about every web browser using the same web engine; Chromium. In the end I said something that I would later regret though, because of course I did.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>I use Chromium based browsers again. (How to lose your followers in less than 5 minutes)</h2>
                <h4>2022-11-30</h4>
                    <h3>Think before you talk, or you end up looking like an idiot like me.</h3>
                        <p>If you've been following my blog for a while, you might remember an old blog post of mine titled 'Everything I want to use is Chromium'. Like the title says, In the blog post I complain about every web browser using the same web engine; Chromium. In the end I said something that I would later regret though, because of course I did.</p>
                        <p>"To contribute to this, I've decided to BAN myself from ever touching anything based on Chromium or Electron."</p>
                        <p>While I have partially held that promise (I haven't tried Electron and I have no plans to do so), I am now back on Chromium based web browsers. Feel free to unsubscribe from my feed now.</p>
                    <h3>With that said..</h3>
                        <p>Now that I successfully managed to lose my last readers, I am gonna continue talking to myself in this blog post. Why did I start using Chromium again?</p>
                        <p>It's because Firefox sucks. That's the answer. Very surprising isn't it? Okay, okay here's the thing. If the year was 2010, I would be sitting here shilling Mozilla and everything they have ever produced because they truly used to be great. I have been a fan of Firefox if you will for years. They're one of the only (sort of) mainstream free software web browsers.</p>
                        <p>But lately they've been going down a dark path. From the privacy issues, to the Facebook partnership, to Google being the default search engine, to their controversial blog posts (I know a thing or two about that) and many more.</p>
                    <h3>We get it, why is Firefox no longer usable?</h3>
                        <p>Long story short, Mozilla Firefox is riddled with bugs, making it about as stable as alpha tier software.. that has existed for almost 20 years. First annoying issue: Firefox being unable to open more than one new window at one time. As Firefox reached version 100, it stopped properly opening windows. I use a lot of windows and few tabs, so this really impacted my usage of it. As you know I use a tiling window manager and I liked being able to put one Firefox window on another tag and another on my current tag. This was useful for writing the website you're currently reading.</p>
                        <p>With my mouth filled with the bad taste of the bug I just mentioned, it doesn't help that the web is getting increasingly harder to use with the Gecko web engine. Piped and Invidious? Page crashes really often. searx? Loads much slower than on Chromium, and this is not even a JavaScript heavy website in the first place so there is no excuse for the slowness.</p>
                        <p>The final straw for me was Firefox taking what feels like years to start up. I would press my keybind to start up Firefox and sometimes wait up to 20 seconds for it to start. It was all up to luck, really. As if it makes it better, when it doesn't start, you'll assume you didn't press the key or something so you'll do it again, and again.. and again. When Firefox finally starts up, you'll have 20 different instances of Firefox and you'll be using up all your RAM and CPU. Not ideal.</p>
                        <p>Ever since I experienced these issues, I went looking for a better web browser and eventually said, 'You know what, I don't care anymore I'll try this out.'.</p>
                    <h3>Chromium.. doesn't suck? WHAT?</h3>
                        <p>I ended up trying Chromium (specifically ungoogled-chromium but it's nearly identical) and it only ended up being the fastest, most stable web browser I've ever used. It just works, without the slowness, without the horrible bugs, without the 20 seconds startup time, without searx and piped not loading pages properly. It all just worked, and it worked great.</p>
                        <p>Realizing I just couldn't pass up the opportunity to switch to a web browser that doesn't fail me on a daily basis, I cut my losses, realized I eventually had to write this blog post and removed Firefox from my system.</p>
                        <p>Is Chromium perfect? Far from it. For one, the Manifest v3 garbage is going to be a major blow to extensions in Chromium. Plus, Chromium is nearly impossible to customize in any meaningful way. Sure, you can change the colors and stuff like that but good luck doing more advanced things. To my knowledge it's not even possible to remove the account icon from the bar, which is why Chromium is the GNOME of web browsers. (Except, GNOME actually sucks)</p>
                    <h3>To be fair..</h3>
                        <p>I never said Chromium was a bad web browser. I didn't really dislike Chromium for that reason. The reason I refused to use Chromium was because it is slowly becoming the one and only web engine. Slowly, all other web engines are becoming more and more irrelevant. This is partially why Firefox sucks so bad. But I am done with Firefox constantly screwing me over.</p>
                    <h3>Qutebrowser</h3>
                        <p>A week or so after I started using Chromium, I realized I could now try Qutebrowser, which is Chromium based but similar to browsers like Vimb in that it uses Vim keybinds for navigation. I ended up enjoying this web browser a lot and while it is slower than Chromium, it is significantly faster than Firefox and is now my web browser of choice. It is written in Python which probably contributes to that extra slowness over Chromium, it's not really noticeable during usage but you will notice it when you start it up.</p> 
                        <p>It also has no extensions just like Vimb and suckless surf and all of these browsers, but it's really /comfy/ to use, especially for someone used to Vim.</p>
                    <h3>But.. but.. what about webkit?</h3>
                        <p>Not gonna go into the Webkit engine here too much but it is even less usable than Firefox in pretty much every way. Sure, web browsers like Surf that use it start up nearly instantly but it has so many flaws and tends to crash when even a slight bit of JavaScript is in the website. And as we all know, it's really difficult to avoid JavaScript in the year 2022.</p>
                        <p>Even loading basic websites, like searx, and even 4chan, a website which has barely been updated since 2003, both surf and vimb tend to crash almost instantly. I tried with a vanilla build of surf as well and still crashes. Because of this issue, and media playback barely working if at all, webkit is not the engine for me.</p>
                    <h3>So.. conclusion?</h3>
                        <p>I know you're probably expecting a 'I switched back to Windows' or 'I work at Apple' blog post next but.. yeah, no that's not gonna happen. I still use exclusively free software and that's not changing. Ultimately, Firefox has gotten a lot worse since I made that initial blog post, so I gave in and started using Chromium.</p>
                        <p>Thank you for reading, I will now await my check in the mail from Google Inc, see you next time where I install Windows on my ThinkPad and switch to VS Code and Microsoft Edge.</p>
                        
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
