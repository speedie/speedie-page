<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>News/important update regarding the site</title>
<meta charset="UTF-8">
<meta name="description" content="I will keep this one short so you can actually read through it. Yesterday (13/04/2023) I purchased a domain, because as we all know I do not trust Freenom to keep my site up. This domain is a lot more reliable, however it does mean you will have to swap out '.gq' for '.site'.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>News/important update regarding the site</h2>
                <h4>2023-04-14</h4>
                    <p>I will keep this one short so you can actually read through it. Yesterday (13/04/2023) I purchased a domain, because as we all know I do not trust Freenom to keep my site up. This domain is a lot more reliable, however it does mean you will have to swap out '.gq' for '.site'. I have redirected <em>some</em> parts of my site. I have redirected the main <code>speedie.gq</code> domain, and I have also redirected rss.xml so that RSS readers won't complain. You should still change the URL, however if you exclusively consume my website through RSS you will get the message anyway because of this. Finally I redirected the wiki.</p>
                    <p>Switching over is not hard. The page is identical, and although SSL was not functioning earlier today, I have resolved the issue. So to switch over, just replace 'speedie.gq' with 'speedie.site'. This is especially important if you use Arch and my repository. If you do, you must edit <code>/etc/pacman.conf</code> and replace the URL. There may be a few sharp edges as of now, as I simply ran a few <code>sed</code> commands on the old site without looking through it properly, if there are issues please <a href="mailto:speedie@speedie.site">email me</a> so it gets fixed. I should also add I moved from Nginx to Apache a few days ago, so <a href="https://wiki.speedie.site">the speedie.site wiki</a> and <a href="https://speedwm.speedie.site">the speedwm wiki</a> should be functional again.</p>
                    <p>Anyway, that was just a short blog post about something relatively important. I will <em>probably</em> keep the speedie.gq domain updated as well, but I cannot guarantee it will work properly. If you have any questions, feel free to email me, or simply join the <a href="https://matrix.to/#/#speedie:matrix.org">Matrix space</a>. Either way, that's it, have a good rest of your day!</p>
</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>

