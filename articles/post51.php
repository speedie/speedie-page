<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Important site update (and the Matrix)</title>
<meta charset="UTF-8">
<meta name="description" content="I'm going to keep this one short and to the point. As some of you may know, my domain is going to expire. It is going to expire on the 31st of March 2023, which is not far from today and that's what I'm going to talk about. Please read this.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Important site update (and the Matrix)</h2>
                <h4>2023-03-15</h4>
                    <p>I'm going to keep this one short and to the point. As some of you may know, my domain is going to expire. It is going to expire on the 31st of March 2023, which is not far from today and that's what I'm going to talk about.</p>
                    <p>For those of you that don't know, my website uses a "free" TLD (top level domain). This seemed like a good option last year, but as I want to continue this stuff, it presents a problem. Freenom is the company that provides the .gq TLD, along with a few more domains such as .tk. Freenom has shown themselves to be problematic, and they have done things like taking away domains from people after the websites have become too popular. Renewing their domains is also difficult and annoying, and even then doesn't work all the time for all people.</p>
                    <p>Because of this, I decided to write this blog post, and to make sure my readers have a place to keep up with me if my website does collapse, I've created a Matrix channel which I recommend you join. You can join it <a href="https://matrix.to/#/#speedie:matrix.org">here</a>. You can start with Element, it's all free software unlike the previous Discord server.</p>
                    <p>I don't plan on making this a big thing like Forwarder Factory was, and in fact I don't want that either. This is simply going to be a small place for me to talk to my readers, discuss the website and other things like that.</p>
                    <p>Either way, my domain expires March 31st if I'm unable to renew it. If I manage to renew it, you can continue using the site like normal for an additional year. Otherwise, I'm simply going to purchase a new domain. I do not yet have another domain, which is why I recommend you join the Matrix channel.</p>
                    <p>That's really all I wanted to say, as the writer here I think it is important that you are informed about everything. My website code is all available for free on Codeberg so you can still have that if you want. Thanks for reading, have a good day!</p>
</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>

