<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>What you can expect from Project 081 0.6 </title>
<meta charset="UTF-8">
<meta name="description" content="Project 081 0.5 is getting kind of old. It has quite a bit of known bugs so they need to be addressed. There are also other nice-to-have improvements and features coming with the 0.6 update. Here are a few of them!">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>What you can expect from Project 081 0.6</h2>
            <h4>2022-03-10</h4>
                <p>This is an update for Project 081 users (if there still are any). As this project has slowly died I would like to finally finish it as I have been working on other stuff like gentoo-install, spDE, my RSS feed, sfetch and various other projects.</p>
                <p>After I spent more time with GNU/Linux and stuff my older Mac OS X project kinda.. died. So therefore my goal is to finish Project 081 soon so that it&#39;s as good as it can possibly be. All the assets are (hopefully) complete however it has yet to be &quot;compiled&quot; together into an image for the user.</p>
                <p>So while you wait, here&#39;s a list of a bunch of changes i&#39;ve made to Project 081 as well as a few I&#39;m going to do.</p>
                <ul>
                <li>Project 081 0.6/1.0 now comes with sfetch</li>
                <li>Project 081 0.6/1.0 will come with an RSS reader as well as my feed.</li>
                <li>Project 081 0.6/1.0 will FINALLY fix AirPort for iMac8,1 users.</li>
                <li>Project 081 0.6/1.0 will hopefully boot on all systems.</li>
                <li>Project 081 0.6/1.0 will hopefully be a single image with a single package that determines what to install based on sysctl hw.model</li>
                <li>Project 081 0.6/1.0 will be an even smaller image</li>
                <li>Project 081 0.6/1.0 will have a preinstalled image available.</li>
                <li>Project 081 0.6/1.0 will FINALLY come with drivers for newer Apple keyboards</li>
                </ul>
                <p>That&#39;s basically what you can expect, if you have any more suggestions please create an Issue on GitHub.</p>
                <p>That&#39;s all, Have a good day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
