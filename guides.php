<!DOCTYPE html>
<html>
<head>
<?php include 'php/header.php';?>
<title>Guides </title>
<meta charset="UTF-8">
<meta name="description" content="This page has a list of all my text guides. Some may be out of date but they should be marked as such.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Guides</h2>
            <p>These are text guides I've written.</p>
                <h5><a href="articles/guide03.php">Guide: Upgrading Suckless software</a>, written 2022-05-09</h5>
                <h5><a href="articles/guide02.php">Guide: Installing Arch Linux</a>, written 2022-05-05</h5>
                <h5><a href="articles/guide01.php">Guide: Upgrading your Gentoo kernel to 5.17.0 (and later)</a>, written 2022-03-23</h5>
		</div>
</body>
<footer>
		<?php include 'php/footer.php';?>
</footer>
</html>
