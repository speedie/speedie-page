<!DOCTYPE html>
<html>
<head>
<?php include 'php/header.php';?>
<meta name="author" content="speedie">
<title>Blog posts</title>
<meta charset="UTF-8">
<meta name="description" content="Here's an archive of all my blog articles/posts. Almost if not all of these were initially posted on my RSS feed.">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
        <h2>Blog</h2>
            <p>I repost some of my RSS feed articles/posts here sometimes. This is a view of all of them.</p>
            <p>NOTE: You can see all of them using my <a href="rss.xml">RSS</a> feed.</p>
            <p><a href="articles/post58.php">Latest blog post</a></p>
        <h3>Feeds</h3>
            <p>This is a list of feeds containing the complete list of blog posts. You can add any of these to your RSS reader and read at any time, provided you have an internet connection.</p>
            <p>If you do not have a reader, install newsboat with your GNU/Linux distribution's package manager.</p>
            <ul>
                <li><a href="rss.xml">RSS feed</a></li>
				<li><a href="updates.xml">RSS project updates</a></li>
                <li><a href="rss-archived-01.xml">Blog post archive feed (1-36)</a></li>
            </ul>
        <h3>Articles</h3>
            <h4 id="50">Posts 50-59</h4>
                <h5><a href="articles/post58.php">Swedish man rants about licenses again</a>, written 2023-05-23
                <h5><a href="articles/post57.php">Ungoogled Chromium: The best browser for most people</a>, written 2023-05-01
                <h5><a href="articles/post56.php">Why I don't use Wayland (and how it can be improved)</a>, written 2023-04-28
                <h5><a href="articles/post55.php">Dear bloggers: Your RSS feeds suck.</a>, written 2023-04-19</h5>
                <h5><a href="articles/post54.php">News/important update regarding the site</a>, written 2023-04-14</h5>
                <h5><a href="articles/post53.php">I switched back to Microsoft Windows.. here's why!</a>, written 2023-04-01</h5>
                <h5><a href="articles/post52.php">Friendship ended with Gentoo, now Arch is my best friend!</a>, written 2023-03-26</h5>
                <h5><a href="articles/post51.php">Important site update (and the Matrix)</a>, written 2023-03-15</h5>
                <h5><a href="articles/post50.php">Normies are destroying GNU/Linux</a>, written 2023-03-09</h5>
            <h4 id="40">Posts 40-49</h4>
                <h5><a href="articles/post49.php">spDE - Now also on Arch based distros</a>, written 2023-02-27</h5>
                <h5><a href="articles/post48.php">Software update</a>, written 2023-02-25</h5>
                <h5><a href="articles/post47.php">Ethical software is not ethical and should be abolished.</a>, written 2023-02-07</h5>
                <h5><a href="articles/post46.php">libvirt is the worst program ever made..</a>, written 2023-02-06</h5>
                <h5><a href="articles/post45.php">speedwm 1.9 release</a>, written 2023-02-05</h5>
                <h5><a href="articles/post44.php">..and a git repository (CGIT REVIEW!!!)</a>, written 2023-01-28</h5>
                <h5><a href="articles/post43.php">speedie.site now has a wiki!</a>, written 2023-01-26</h5>
                <h5><a href="articles/post42.php">When will I use BSD?</a>, written 2023-01-17</h5>
                <h5><a href="articles/post41.php">Why YouTube©️™️ doesn't recommend your videos.</a>, written 2023-01-15</h5>
                <h5><a href="articles/post40.php">Let's talk about Project 081 0.6</a>, written 2023-01-03</h5>
            <h4 id="37">Posts 37-39</h4>
                <h5><a href="articles/post39.php">Goodbye 2022..</a>, written 2022-12-31</h5>
                <h5><a href="articles/post38.php">I WILL make music great again. (My next project)</a>, written 2022-12-20</h5>
                <h5><a href="articles/post37.php">How I got into GNU/Linux</a>, written 2022-12-13</h5>
        <h3 id="archived">Archived blog articles/posts</h3>
                <p>NOTE: Some of these have been archived due to lack of quality, others have been archived to keep the number of current posts low.</p>
            <h4 id="30">Posts 30-36</h4>
                <h5><a href="articles/post36.php">I use Chromium based browsers again. (How to lose your followers in less than 5 minutes)</a>, written 2022-11-30</h5>
                <h5><a href="articles/post35.php">speedwm 1.5: It's still speedwm.</a>, written 2022-11-28</h5>
                <h5><a href="articles/post34.php">We're back up better than ever! (feat. OpenBaSeD)</a>, written 2022-11-25</h5>
                <h5><a href="articles/post32.php">Forwarder Factory is over. (Please read the blog articles/post)</a>, written 2022-10-26</h5>
                <h5><a href="articles/post31.php">Please give me suggestions...</a>, written 2022-10-10</h5>
                <h5><a href="articles/post30.php">I HATE NONFREE SOFTWARE (install gnu icecat)</a>, written 2022-09-23</h5>
            <h4 id="20">Posts 20-29</h4>
                <h5><a href="articles/post29.php">RIP in peace rchat (and releasing its replacement)</a>, written 2022-09-21</h5>
                <h5><a href="articles/post28.php">SHILL POSTS ARE BACK! fontctrl (Fonts on GNU/Linux-Improved)</a>, written 2022-09-17</h5>
                <h5><a href="articles/post27.php">I AM TOO AWESOME FOR SMARTPHONES!</a>, written 2022-09-15</h5>
                <h5><a href="articles/post26.php">Vim: You're wasting your life away if you don't use it.</a>, written 2022-09-04</h5>
                <h5><a href="articles/post25.php">I left GitHub and you should too!</a>, written 2022-08-23</h5>
                <h5><a href="articles/post24.php">What happened to spDE? (And announcement)</a>, written 2022-07-23</h5>
                <h5><a href="articles/post23.php">Why I ban software.</a>, written 2022-07-20</h5>
                <h5><a href="articles/post22.php">OH NO IM BEING CANCELLED ON TWITTER WHAT WILL I DO???</a>, written 2022-07-14</h5>
                <h5><a href="articles/post21.php">PipeWire Review (RSS REVIEWS!!!)</a>, written 2022-07-04</h5>
                <h5><a href="articles/post20.php">rchat 1.3 is out! (I AM GREAT AT PUSHING UPDATES!)</a>, written 2022-06-29</h5>
            <h4 id="10">Posts 10-19</h4>
                <h5><a href="articles/post19.php">dwm: dynamic window greatness!</a>, written 2022-06-18</h5>
                <h5><a href="articles/post18.php">I HATE ATI!!!</a>, written 2022-06-18</h5>
                <h5><a href="articles/post17.php">Website update</a>, written 2022-06-10</h5>
                <h5><a href="articles/post16.php">Return of the rchat (rchat 1.0 is out)</a>, written 2022-06-07</h5>
                <h5><a href="articles/post15.php">Are GNU/Linux users elitist or are normies too stupid to learn?</a>, written 2022-06-06</h5>
                <h5><a href="articles/post14.php">Why I switched license from MIT to GPLv3</a>, written 2022-06-05</h5>
                <h5><a href="articles/post13.php">Open source: Fake freedom.</a>, written 2022-06-05</h5>
                <h5><a href="articles/post12.php">I HATE APPLE!!!</a>, written 2022-05-23</h5>
                <h5><a href="articles/post33.php">Why I don't support RiiConnect24.</a>, written 2022-05-20</h5>
                <h5><a href="articles/post11.php">Smartphones are only smart because you're dumb.</a>, written 2022-05-04</h5>
                <h5><a href="articles/post10.php">xinit is bloated</a>, written 2022-04-30</h5>
            <h4 id="9">Post 1-9</h4>
                <h5><a href="articles/post09.php">Everything I want to use is Chromium</a>, written 2022-04-26</h5>
                <h5><a href="articles/post08.php">Half an rchat (rchat 0.5 is out)</a>, written 2022-04-26</h5>
                <h5><a href="articles/post07.php">rchat 0.4 is here (Now available on Arch and Gentoo)</a>, written 2022-04-24</h5>
                <h5><a href="articles/post06.php">It's time to stop using Adblock Plus (seriously stop)</a>, written 2022-03-12</h5>
                <h5><a href="articles/post05.php">Happy 20th Birthday Arch!</a>, written 2022-03-11</h5>
                <h5><a href="articles/post04.php">What you can expect from Project 081 0.6</a>, written 2022-03-10</h5>
                <h5><a href="articles/post03.php">Dear soydevs: Stop making desktop applications bloated</a>, written 2022-03-10</h5>
                <h5><a href="articles/post02.php">Notice for spDE users</a>, written 2022-03-09</h5>
                <h5><a href="articles/post01.php">Stop making GNU/Linux user friendly.. sort of</a>, written 2022-03-09</h5>
		</div>
</body>
<footer>
		<?php include 'php/footer.php';?>
</footer>
</html>
