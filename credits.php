<!DOCTYPE html>
<html>
<head>
<?php include 'php/header.php';?>
<meta charset="UTF-8">
<meta name="description" content="Credit where credit is due.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Credits</title>
</head>
<body>
		<div class="content">
				<h2>Credits</h2>
                    <p>Credits for this website and all services I run.</p>
                    <ul>
                        <li>speedie.site</li>
                            <p>The initial concept for this site was designed by <a href="https://donut.eu.org">donut.eu.org</a>. I have expanded on it and rewritten a lot of the styling since. Thank you!</p>
                        <li>git.speedie.site</li>
                            <p>Favicon and logo was designed by <a href="https://donut.eu.org">donut.eu.org</a> in scalable vector format. git.speedie.site is a cgit instance.</p>
                        <li>ls.speedie.site</li>
                            <p>ls.speedie.site is a fork of <a href="https://github.com/lorenzos/Minixed">MINIXED</a> with mostly styling changes.</p>
                        <li>speedwm.speedie.site, spmenu.speedie.site, wiki.speedie.site</li>
                            <p>All my wikis are done through <a href="https://github.com/panicsteve/w2wiki/">w2wiki</a>, which I have modified a bit to fit my needs.</p>
                        <li>Web server</li>
                            <p>Everything is hosted on a Vultr VPS using Apache2.</p>
                    </ul>
		</div>
</body>
<footer>
		<?php include 'php/footer.php';?>
</footer>
</html>
